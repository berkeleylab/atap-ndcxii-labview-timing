﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="15008000">
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="bandwidth-selector.ctl" Type="VI" URL="../bandwidth-selector.ctl"/>
		<Item Name="CheckForAutomaticTrigger.vi" Type="VI" URL="../../Timing/CheckForAutomaticTrigger.vi"/>
		<Item Name="Main.vi" Type="VI" URL="../Main.vi"/>
		<Item Name="TDS654C-enum-string-to-number.vi" Type="VI" URL="../TDS654C-enum-string-to-number.vi"/>
		<Item Name="TDS654C-set-channel-scale.vi" Type="VI" URL="../TDS654C-set-channel-scale.vi"/>
		<Item Name="TDS654C-set-time-scale.vi" Type="VI" URL="../TDS654C-set-time-scale.vi"/>
		<Item Name="TDS654C-TimeSettings.ctl" Type="VI" URL="../TDS654C-TimeSettings.ctl"/>
		<Item Name="TDS654C-VoltSettings.ctl" Type="VI" URL="../TDS654C-VoltSettings.ctl"/>
		<Item Name="ZMQ-subscribe-to-shot-events.vi" Type="VI" URL="../../ZMQ-events/ZMQ-subscribe-to-shot-events.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="instr.lib" Type="Folder">
				<Item Name="dg535 delay.vi" Type="VI" URL="/&lt;instrlib&gt;/srdg535/SRDG535.LLB/dg535 delay.vi"/>
				<Item Name="dg535 error handler.vi" Type="VI" URL="/&lt;instrlib&gt;/srdg535/SRDG535.LLB/dg535 error handler.vi"/>
				<Item Name="DG535 Receive Message.vi" Type="VI" URL="/&lt;instrlib&gt;/srdg535/SRDG535.LLB/DG535 Receive Message.vi"/>
				<Item Name="DG535 Send Message.vi" Type="VI" URL="/&lt;instrlib&gt;/srdg535/SRDG535.LLB/DG535 Send Message.vi"/>
				<Item Name="dg535 set output.vi" Type="VI" URL="/&lt;instrlib&gt;/srdg535/SRDG535.LLB/dg535 set output.vi"/>
				<Item Name="dg535 trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/srdg535/SRDG535.LLB/dg535 trigger.vi"/>
				<Item Name="GFT9404_Auxiliary Delay Channels.ctl" Type="VI" URL="/&lt;instrlib&gt;/GreenfieldTechnology GFT9404/_Typedefs/GFT9404_Auxiliary Delay Channels.ctl"/>
				<Item Name="GFT9404_Precision Delay Channels.ctl" Type="VI" URL="/&lt;instrlib&gt;/GreenfieldTechnology GFT9404/_Typedefs/GFT9404_Precision Delay Channels.ctl"/>
				<Item Name="niScope trigger coupling.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl"/>
				<Item Name="niScope trigger slope.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl"/>
				<Item Name="niScope trigger window mode.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl"/>
				<Item Name="niScope vertical coupling.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl"/>
				<Item Name="Tektronix 7000 Series.lvlib" Type="Library" URL="/&lt;instrlib&gt;/Tektronix 7000 Series/Tektronix 7000 Series.lvlib"/>
				<Item Name="TKTDS 6XX Close.vi" Type="VI" URL="/&lt;instrlib&gt;/tktds6xx/tktds6xx.llb/TKTDS 6XX Close.vi"/>
				<Item Name="TKTDS 6XX Initialize.vi" Type="VI" URL="/&lt;instrlib&gt;/tktds6xx/tktds6xx.llb/TKTDS 6XX Initialize.vi"/>
				<Item Name="TKTDS Query SESR.vi" Type="VI" URL="/&lt;instrlib&gt;/tktds6xx/tktdsxxx.llb/TKTDS Query SESR.vi"/>
				<Item Name="TKTDS Read Event Queue.vi" Type="VI" URL="/&lt;instrlib&gt;/tktds6xx/tktdsxxx.llb/TKTDS Read Event Queue.vi"/>
				<Item Name="TKTDS Read Waveform.vi" Type="VI" URL="/&lt;instrlib&gt;/tktds6xx/tktdsxxx.llb/TKTDS Read Waveform.vi"/>
				<Item Name="TKTDS Reset.vi" Type="VI" URL="/&lt;instrlib&gt;/tktds6xx/tktdsxxx.llb/TKTDS Reset.vi"/>
			</Item>
			<Item Name="user.lib" Type="Folder">
				<Item Name="MD5 F function__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 F function__ogtk.vi"/>
				<Item Name="MD5 FGHI functions__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 FGHI functions__ogtk.vi"/>
				<Item Name="MD5 G function__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 G function__ogtk.vi"/>
				<Item Name="MD5 H function__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 H function__ogtk.vi"/>
				<Item Name="MD5 I function__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 I function__ogtk.vi"/>
				<Item Name="MD5 Message Digest (Binary String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 Message Digest (Binary String)__ogtk.vi"/>
				<Item Name="MD5 Message Digest (Hexadecimal String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 Message Digest (Hexadecimal String)__ogtk.vi"/>
				<Item Name="MD5 Message Digest__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 Message Digest__ogtk.vi"/>
				<Item Name="MD5 Padding__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 Padding__ogtk.vi"/>
				<Item Name="MD5 ti__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 ti__ogtk.vi"/>
				<Item Name="MD5 Unrecoverable character padding__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 Unrecoverable character padding__ogtk.vi"/>
				<Item Name="Wait (ms)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/time/time.llb/Wait (ms)__ogtk.vi"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="compatCalcOffset.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatCalcOffset.vi"/>
				<Item Name="compatFileDialog.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatFileDialog.vi"/>
				<Item Name="compatOpenFileOperation.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOpenFileOperation.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="CQL-close-DB.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-close-DB.vi"/>
				<Item Name="CQL-consistencylevel.ctl" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-consistencylevel.ctl"/>
				<Item Name="CQL-conv-type-json.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-conv-type-json.vi"/>
				<Item Name="CQL-flags.ctl" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-flags.ctl"/>
				<Item Name="CQL-frame.ctl" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-frame.ctl"/>
				<Item Name="CQL-opcode.ctl" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-opcode.ctl"/>
				<Item Name="CQL-open-DB.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-open-DB.vi"/>
				<Item Name="CQL-option.ctl" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-option.ctl"/>
				<Item Name="CQL-query.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-query.vi"/>
				<Item Name="CQL-Read-basic-byte.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Read-basic-byte.vi"/>
				<Item Name="CQL-Read-basic-bytes.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Read-basic-bytes.vi"/>
				<Item Name="CQL-Read-basic-int.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Read-basic-int.vi"/>
				<Item Name="CQL-Read-basic-short.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Read-basic-short.vi"/>
				<Item Name="CQL-Read-basic-string-list.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Read-basic-string-list.vi"/>
				<Item Name="CQL-Read-basic-string-multimap.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Read-basic-string-multimap.vi"/>
				<Item Name="CQL-Read-basic-string.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Read-basic-string.vi"/>
				<Item Name="CQL-read-data-from-byte-array.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-read-data-from-byte-array.vi"/>
				<Item Name="CQL-read-header.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-read-header.vi"/>
				<Item Name="CQL-Read-query-result-metadata.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Read-query-result-metadata.vi"/>
				<Item Name="CQL-Read-result-column-type.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Read-result-column-type.vi"/>
				<Item Name="CQL-read-result.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-read-result.vi"/>
				<Item Name="CQL-request-response.ctl" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-request-response.ctl"/>
				<Item Name="CQL-use-keyspace.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-use-keyspace.vi"/>
				<Item Name="CQL-Write-basic-long-string.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Write-basic-long-string.vi"/>
				<Item Name="CQL-Write-basic-string-map.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Write-basic-string-map.vi"/>
				<Item Name="CQL-Write-basic-string.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Write-basic-string.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Open_Create_Replace File.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/Open_Create_Replace File.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="zeromq.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/zeromq/zeromq.lvlib"/>
			</Item>
			<Item Name="4CH-ScopeSetting-DBv3.ctl" Type="VI" URL="../../cassandra-NDCXII-interface/4CH-ScopeSetting-DBv3.ctl"/>
			<Item Name="4CH-ScopeSetting-TEKTRONIX-display-v2.ctl" Type="VI" URL="../../cassandra-NDCXII-interface/4CH-ScopeSetting-TEKTRONIX-display-v2.ctl"/>
			<Item Name="4CH-ScopeSetting-TEKTRONIX-display-v3.ctl" Type="VI" URL="../../cassandra-NDCXII-interface/4CH-ScopeSetting-TEKTRONIX-display-v3.ctl"/>
			<Item Name="Conv-4Ch-TEKTRONIX-to-JSON.vi" Type="VI" URL="../../cassandra-NDCXII-interface/Conv-4Ch-TEKTRONIX-to-JSON.vi"/>
			<Item Name="Conv-JSON-to-4Ch-TEKTRONIX.vi" Type="VI" URL="../../cassandra-NDCXII-interface/Conv-JSON-to-4Ch-TEKTRONIX.vi"/>
			<Item Name="Conv-TEKTRONIX_DPO7000-to-JSON.vi" Type="VI" URL="../../cassandra-NDCXII-interface/Conv-TEKTRONIX_DPO7000-to-JSON.vi"/>
			<Item Name="Devicelist.vi" Type="VI" URL="../../Top/Devicelist.vi"/>
			<Item Name="NDCXII-cassandra-library.lvlib" Type="Library" URL="../../NDCXII-cassandra-library.lvlib"/>
			<Item Name="timebase-convert-to-enum.vi" Type="VI" URL="../../cassandra-NDCXII-interface/timebase-convert-to-enum.vi"/>
			<Item Name="voltage-range-convert-to-enum.vi" Type="VI" URL="../../cassandra-NDCXII-interface/voltage-range-convert-to-enum.vi"/>
			<Item Name="ZMQ-REP-load-hash.vi" Type="VI" URL="../../ZMQ-events/ZMQ-REP-load-hash.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
