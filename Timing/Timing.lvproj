﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="15008000">
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Property Name="varPersistentID:{602A70E9-7D7B-45C8-8339-559FCB8C66A2}" Type="Ref">/My Computer/GTiming.lvlib/PXI1Tstop</Property>
	<Property Name="varPersistentID:{71DD80C2-D109-4086-89C9-A4DC9CFBD178}" Type="Ref">/My Computer/GTiming.lvlib/PXI0Timing</Property>
	<Property Name="varPersistentID:{9F5F5F5D-2266-41E0-826E-6B338EFFA569}" Type="Ref">/My Computer/GTiming.lvlib/TestStringArray</Property>
	<Property Name="varPersistentID:{A594894F-4877-4ECB-B01A-0BF2956C9C7F}" Type="Ref">/My Computer/GTiming.lvlib/GTime</Property>
	<Property Name="varPersistentID:{CEE69877-9DE4-4E03-BB96-4FBA75CA6C74}" Type="Ref">/My Computer/GTiming.lvlib/PXI1Timing</Property>
	<Property Name="varPersistentID:{DEAC677D-BC73-46AF-9233-54163AD4709B}" Type="Ref">/My Computer/GTiming.lvlib/PXI0Tstop</Property>
	<Property Name="varPersistentID:{E401C4ED-48EE-431B-92D7-27C00929DFFA}" Type="Ref">/My Computer/GTiming.lvlib/BackAnnotate</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="CrateTiming.ctl" Type="VI" URL="../CrateTiming.ctl"/>
		<Item Name="ExpandPXI0.vi" Type="VI" URL="../ExpandPXI0.vi"/>
		<Item Name="ExpandPXI1.vi" Type="VI" URL="../ExpandPXI1.vi"/>
		<Item Name="Flatten.vi" Type="VI" URL="../Flatten.vi"/>
		<Item Name="GTime.vi" Type="VI" URL="../GTime.vi"/>
		<Item Name="GTime1.vi" Type="VI" URL="../GTime1.vi"/>
		<Item Name="GTiming.lvlib" Type="Library" URL="../../Acquisition/GTiming.lvlib"/>
		<Item Name="PXI0Timing.vi" Type="VI" URL="../PXI0Timing.vi"/>
		<Item Name="PXI0Top.vi" Type="VI" URL="../PXI0Top.vi"/>
		<Item Name="PXI1Timing.vi" Type="VI" URL="../PXI1Timing.vi"/>
		<Item Name="PXI1Top.vi" Type="VI" URL="../PXI1Top.vi"/>
		<Item Name="Startup.vi" Type="VI" URL="../Startup.vi"/>
		<Item Name="test1.vi" Type="VI" URL="../test1.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="instr.lib" Type="Folder">
				<Item Name="gft14xx.dll" Type="Document" URL="/&lt;instrlib&gt;/GreenfieldTechnology GFT9404/gft14xx.dll"/>
				<Item Name="GFT9404_Channel Definition.ctl" Type="VI" URL="/&lt;instrlib&gt;/GreenfieldTechnology GFT9404/_Typedefs/GFT9404_Channel Definition.ctl"/>
				<Item Name="GFTy 9404.lvlib" Type="Library" URL="/&lt;instrlib&gt;/GreenfieldTechnology GFT9404/GFTy 9404.lvlib"/>
				<Item Name="niScope trigger coupling.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl"/>
				<Item Name="niScope trigger slope.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl"/>
				<Item Name="niScope trigger window mode.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl"/>
				<Item Name="niScope vertical coupling.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl"/>
			</Item>
			<Item Name="user.lib" Type="Folder">
				<Item Name="MD5 F function__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 F function__ogtk.vi"/>
				<Item Name="MD5 FGHI functions__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 FGHI functions__ogtk.vi"/>
				<Item Name="MD5 G function__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 G function__ogtk.vi"/>
				<Item Name="MD5 H function__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 H function__ogtk.vi"/>
				<Item Name="MD5 I function__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 I function__ogtk.vi"/>
				<Item Name="MD5 Message Digest (Binary String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 Message Digest (Binary String)__ogtk.vi"/>
				<Item Name="MD5 Message Digest (Hexadecimal String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 Message Digest (Hexadecimal String)__ogtk.vi"/>
				<Item Name="MD5 Message Digest__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 Message Digest__ogtk.vi"/>
				<Item Name="MD5 Padding__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 Padding__ogtk.vi"/>
				<Item Name="MD5 ti__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 ti__ogtk.vi"/>
				<Item Name="MD5 Unrecoverable character padding__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 Unrecoverable character padding__ogtk.vi"/>
				<Item Name="Wait (ms)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/time/time.llb/Wait (ms)__ogtk.vi"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="CQL-close-DB.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-close-DB.vi"/>
				<Item Name="CQL-consistencylevel.ctl" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-consistencylevel.ctl"/>
				<Item Name="CQL-conv-type-json.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-conv-type-json.vi"/>
				<Item Name="CQL-flags.ctl" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-flags.ctl"/>
				<Item Name="CQL-frame.ctl" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-frame.ctl"/>
				<Item Name="CQL-opcode.ctl" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-opcode.ctl"/>
				<Item Name="CQL-open-DB.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-open-DB.vi"/>
				<Item Name="CQL-option.ctl" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-option.ctl"/>
				<Item Name="CQL-query.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-query.vi"/>
				<Item Name="CQL-Read-basic-byte.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Read-basic-byte.vi"/>
				<Item Name="CQL-Read-basic-bytes.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Read-basic-bytes.vi"/>
				<Item Name="CQL-Read-basic-int.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Read-basic-int.vi"/>
				<Item Name="CQL-Read-basic-short.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Read-basic-short.vi"/>
				<Item Name="CQL-Read-basic-string-list.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Read-basic-string-list.vi"/>
				<Item Name="CQL-Read-basic-string-multimap.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Read-basic-string-multimap.vi"/>
				<Item Name="CQL-Read-basic-string.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Read-basic-string.vi"/>
				<Item Name="CQL-read-data-from-byte-array.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-read-data-from-byte-array.vi"/>
				<Item Name="CQL-read-header.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-read-header.vi"/>
				<Item Name="CQL-Read-query-result-metadata.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Read-query-result-metadata.vi"/>
				<Item Name="CQL-Read-result-column-type.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Read-result-column-type.vi"/>
				<Item Name="CQL-read-result.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-read-result.vi"/>
				<Item Name="CQL-request-response.ctl" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-request-response.ctl"/>
				<Item Name="CQL-use-keyspace.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-use-keyspace.vi"/>
				<Item Name="CQL-Write-basic-long-string.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Write-basic-long-string.vi"/>
				<Item Name="CQL-Write-basic-string-map.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Write-basic-string-map.vi"/>
				<Item Name="CQL-Write-basic-string.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Write-basic-string.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="zeromq.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/zeromq/zeromq.lvlib"/>
			</Item>
			<Item Name="All-PXI-Scopes.ctl" Type="VI" URL="../../PXIcommon/All-PXI-Scopes.ctl"/>
			<Item Name="ArchArray.vi" Type="VI" URL="../../Archiver-VIs-LV10/ArchArray.vi"/>
			<Item Name="bandwidth-selector.ctl" Type="VI" URL="../../BeamlineScope/bandwidth-selector.ctl"/>
			<Item Name="ChangeTimingMode.vi" Type="VI" URL="../../Top/ChangeTimingMode.vi"/>
			<Item Name="CheckForAutomaticTrigger.vi" Type="VI" URL="../CheckForAutomaticTrigger.vi"/>
			<Item Name="CloseMultiVIRef.vi" Type="VI" URL="../../Top/CloseMultiVIRef.vi"/>
			<Item Name="CloseSingleVIRef.vi" Type="VI" URL="../../Top/CloseSingleVIRef.vi"/>
			<Item Name="Conv-DG535Setting-To-JSON.vi" Type="VI" URL="../../cassandra-NDCXII-interface/Conv-DG535Setting-To-JSON.vi"/>
			<Item Name="CreateBPMWindow.vi" Type="VI" URL="../../Top/CreateBPMWindow.vi"/>
			<Item Name="CreateF-cup.vi" Type="VI" URL="../../Top/CreateF-cup.vi"/>
			<Item Name="Devicelist.vi" Type="VI" URL="../../Top/Devicelist.vi"/>
			<Item Name="DG535-Settings-DB-v1.ctl" Type="VI" URL="../../cassandra-NDCXII-interface/DG535-Settings-DB-v1.ctl"/>
			<Item Name="DG535-Settings.ctl" Type="VI" URL="../../cassandra-NDCXII-interface/DG535-Settings.ctl"/>
			<Item Name="FindDevice.vi" Type="VI" URL="../../Top/FindDevice.vi"/>
			<Item Name="GetTimingSetting.vi" Type="VI" URL="../GetTimingSetting.vi"/>
			<Item Name="InitTiming.vi" Type="VI" URL="../../Top/InitTiming.vi"/>
			<Item Name="LED-blink-for-n-seconds.vi" Type="VI" URL="../../Top/LED-blink-for-n-seconds.vi"/>
			<Item Name="NDCXII-cassandra-library.lvlib" Type="Library" URL="../../NDCXII-cassandra-library.lvlib"/>
			<Item Name="OpenMultiViRef.vi" Type="VI" URL="../../Top/OpenMultiViRef.vi"/>
			<Item Name="OpenSingleViRef.vi" Type="VI" URL="../../Top/OpenSingleViRef.vi"/>
			<Item Name="OpenTCPConn.vi" Type="VI" URL="../../Archiver-VIs-LV10/OpenTCPConn.vi"/>
			<Item Name="readSolenoidTiming.vi" Type="VI" URL="../../Top/readSolenoidTiming.vi"/>
			<Item Name="SendValue.vi" Type="VI" URL="../../Archiver-VIs-LV10/SendValue.vi"/>
			<Item Name="Set-Timing-Modules.vi" Type="VI" URL="../../Top/Set-Timing-Modules.vi"/>
			<Item Name="SolenoidTimeControl.ctl" Type="VI" URL="../../Top/SolenoidTimeControl.ctl"/>
			<Item Name="TimingControls.ctl" Type="VI" URL="../../Top/TimingControls.ctl"/>
			<Item Name="U64ArrToJSONArr.vi" Type="VI" URL="../../Archiver-VIs-LV10/U64ArrToJSONArr.vi"/>
			<Item Name="U64ToJSONNum.vi" Type="VI" URL="../../Archiver-VIs-LV10/U64ToJSONNum.vi"/>
			<Item Name="updateSolenoidTiming.vi" Type="VI" URL="../../Top/updateSolenoidTiming.vi"/>
			<Item Name="UpdateTimingDevice.vi" Type="VI" URL="../../Top/UpdateTimingDevice.vi"/>
			<Item Name="ZMQ-REP-load-hash.vi" Type="VI" URL="../../ZMQ-events/ZMQ-REP-load-hash.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
