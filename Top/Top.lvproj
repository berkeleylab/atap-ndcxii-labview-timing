﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="15008000">
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Displays" Type="Folder">
			<Item Name="OctalChannelTrace.vi" Type="VI" URL="../OctalChannelTrace.vi"/>
			<Item Name="QuadChannelTrace.vi" Type="VI" URL="../QuadChannelTrace.vi"/>
			<Item Name="QuadChannelTraceSmall.vi" Type="VI" URL="../QuadChannelTraceSmall.vi"/>
			<Item Name="SingleChannelSmall.vi" Type="VI" URL="../SingleChannelSmall.vi"/>
			<Item Name="SingleChannelTrace.vi" Type="VI" URL="../SingleChannelTrace.vi"/>
			<Item Name="CurrentMonitors.vi" Type="VI" URL="../CurrentMonitors.vi"/>
			<Item Name="SolenoidPulsers.vi" Type="VI" URL="../SolenoidPulsers.vi"/>
			<Item Name="VoltageMonitors.vi" Type="VI" URL="../VoltageMonitors.vi"/>
			<Item Name="AcquisitionSettingChannel.ctl" Type="VI" URL="../AcquisitionSettingChannel.ctl"/>
			<Item Name="BlumleinsMonitors.vi" Type="VI" URL="../BlumleinsMonitors.vi"/>
			<Item Name="BlumleinsMonitors-upstairs.vi" Type="VI" URL="../BlumleinsMonitors-upstairs.vi"/>
			<Item Name="graph-low-pass-filter.vi" Type="VI" URL="../graph-low-pass-filter.vi"/>
			<Item Name="BPM1-7.vi" Type="VI" URL="../BPM1-7.vi"/>
			<Item Name="FEPS.vi" Type="VI" URL="../FEPS.vi"/>
			<Item Name="FCAPS.vi" Type="VI" URL="../FCAPS.vi"/>
		</Item>
		<Item Name="UtilityFunctions" Type="Folder">
			<Property Name="NI.SortType" Type="Int">0</Property>
			<Item Name="CloseMultiVIRef.vi" Type="VI" URL="../CloseMultiVIRef.vi"/>
			<Item Name="CloseSingleVIRef.vi" Type="VI" URL="../CloseSingleVIRef.vi"/>
			<Item Name="CreateBPMWindow.vi" Type="VI" URL="../CreateBPMWindow.vi"/>
			<Item Name="CreateF-cup.vi" Type="VI" URL="../CreateF-cup.vi"/>
			<Item Name="LED-blink-for-n-seconds.vi" Type="VI" URL="../LED-blink-for-n-seconds.vi"/>
			<Item Name="OpenGenericScopeViRef.vi" Type="VI" URL="../OpenGenericScopeViRef.vi"/>
			<Item Name="OpenGenericTimingViRef.vi" Type="VI" URL="../OpenGenericTimingViRef.vi"/>
			<Item Name="OpenMultiViRef.vi" Type="VI" URL="../OpenMultiViRef.vi"/>
			<Item Name="OpenSingleViRef.vi" Type="VI" URL="../OpenSingleViRef.vi"/>
			<Item Name="Set-Timing-Modules.vi" Type="VI" URL="../Set-Timing-Modules.vi"/>
			<Item Name="SetAllScopeProperties.vi" Type="VI" URL="../SetAllScopeProperties.vi"/>
			<Item Name="SetSingleScopeProperties.vi" Type="VI" URL="../SetSingleScopeProperties.vi"/>
			<Item Name="ShowHideVi.vi" Type="VI" URL="../ShowHideVi.vi"/>
			<Item Name="StopeScopeVIRef.vi" Type="VI" URL="../StopeScopeVIRef.vi"/>
			<Item Name="Wait-ms.vi" Type="VI" URL="../Wait-ms.vi"/>
			<Item Name="WaveSelect.vi" Type="VI" URL="../WaveSelect.vi"/>
		</Item>
		<Item Name="Timing" Type="Folder">
			<Item Name="PXI0Time.vi" Type="VI" URL="../PXI0Time.vi"/>
			<Item Name="NDCXII-ZMQ-request-timestamp.vi" Type="VI" URL="../../Timing/NDCXII-ZMQ-request-timestamp.vi"/>
			<Item Name="SolenoidTimeControl.ctl" Type="VI" URL="../SolenoidTimeControl.ctl"/>
			<Item Name="updateSolenoidTiming.vi" Type="VI" URL="../updateSolenoidTiming.vi"/>
			<Item Name="readSolenoidTiming.vi" Type="VI" URL="../readSolenoidTiming.vi"/>
			<Item Name="ChangeTimingMode.vi" Type="VI" URL="../ChangeTimingMode.vi"/>
			<Item Name="GetTimingSetting.vi" Type="VI" URL="../../Timing/GetTimingSetting.vi"/>
			<Item Name="CheckForAutomaticTrigger.vi" Type="VI" URL="../../Timing/CheckForAutomaticTrigger.vi"/>
		</Item>
		<Item Name="PXI0" Type="Folder">
			<Item Name="PXI-0-Startup.vi" Type="VI" URL="../../PXI0/PXI-0-Startup.vi"/>
			<Item Name="PXI-0-RemoteTime.vi" Type="VI" URL="../../PXI0/Timing/PXI-0-RemoteTime.vi"/>
		</Item>
		<Item Name="PXI1" Type="Folder">
			<Item Name="PXI-1-Startup.vi" Type="VI" URL="../../PXI1/PXI-1-Startup.vi"/>
			<Item Name="PXI-1-RemoteTime.vi" Type="VI" URL="../../PXI1/Timing/PXI-1-RemoteTime.vi"/>
		</Item>
		<Item Name="PXI3" Type="Folder">
			<Item Name="PXI-3-Startup.vi" Type="VI" URL="../../PXI3/PXI-3-Startup.vi"/>
			<Item Name="PXI-3-RemoteTime.vi" Type="VI" URL="../../PXI3/Timing/PXI-3-RemoteTime.vi"/>
		</Item>
		<Item Name="PXI4" Type="Folder">
			<Item Name="PXI-4-Startup.vi" Type="VI" URL="../../PXI4/PXI-4-Startup.vi"/>
			<Item Name="PXI-4-MasterTime.vi" Type="VI" URL="../../PXI4/Timing/PXI-4-MasterTime.vi"/>
		</Item>
		<Item Name="PXI5" Type="Folder">
			<Item Name="PXI-5-startup.vi" Type="VI" URL="../../PXI5/PXI-5-startup.vi"/>
		</Item>
		<Item Name="PXIICommon" Type="Folder">
			<Item Name="Update-Scope-Settings.vi" Type="VI" URL="../../PXIcommon/Update-Scope-Settings.vi"/>
			<Item Name="PXI-Scope-generic.vi" Type="VI" URL="../../PXIcommon/PXI-Scope-generic.vi"/>
			<Item Name="PXI6652_sequencer.vi" Type="VI" URL="../../PXIcommon/PXI6652_sequencer.vi"/>
			<Item Name="PXI6652_single.vi" Type="VI" URL="../../PXIcommon/PXI6652_single.vi"/>
			<Item Name="GFT9404_seq.vi" Type="VI" URL="../../PXIcommon/GFT9404_seq.vi"/>
			<Item Name="GFT9404_single.vi" Type="VI" URL="../../PXIcommon/GFT9404_single.vi"/>
			<Item Name="All-PXI-Scopes.ctl" Type="VI" URL="../../PXIcommon/All-PXI-Scopes.ctl"/>
			<Item Name="PXI-RemoteTime.vi" Type="VI" URL="../../PXIcommon/PXI-RemoteTime.vi"/>
		</Item>
		<Item Name="docs" Type="Folder"/>
		<Item Name="ZMQ-events" Type="Folder">
			<Item Name="ZMQ-subscribe-to-shot-events.vi" Type="VI" URL="../../ZMQ-events/ZMQ-subscribe-to-shot-events.vi"/>
		</Item>
		<Item Name="NDCXII-cassandra-library.lvlib" Type="Library" URL="../../NDCXII-cassandra-library.lvlib"/>
		<Item Name="Startup.vi" Type="VI" URL="../../Timing/Startup.vi"/>
		<Item Name="Devicelist.vi" Type="VI" URL="../Devicelist.vi"/>
		<Item Name="TimingControls.ctl" Type="VI" URL="../TimingControls.ctl"/>
		<Item Name="InitTiming.vi" Type="VI" URL="../InitTiming.vi"/>
		<Item Name="UpdateTimingDevice.vi" Type="VI" URL="../UpdateTimingDevice.vi"/>
		<Item Name="FindDevice.vi" Type="VI" URL="../FindDevice.vi"/>
		<Item Name="GetPicoScopeSetting.vi" Type="VI" URL="../../Timing/GetPicoScopeSetting.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="instr.lib" Type="Folder">
				<Item Name="gft14xx.dll" Type="Document" URL="/&lt;instrlib&gt;/GreenfieldTechnology GFT9404/gft14xx.dll"/>
				<Item Name="GFT9404_Channel Definition.ctl" Type="VI" URL="/&lt;instrlib&gt;/GreenfieldTechnology GFT9404/_Typedefs/GFT9404_Channel Definition.ctl"/>
				<Item Name="GFTy 9404.lvlib" Type="Library" URL="/&lt;instrlib&gt;/GreenfieldTechnology GFT9404/GFTy 9404.lvlib"/>
				<Item Name="niScope trigger coupling.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl"/>
				<Item Name="niScope trigger slope.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl"/>
				<Item Name="niScope trigger window mode.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl"/>
				<Item Name="niScope vertical coupling.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl"/>
				<Item Name="niScope Initialize.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/niScope Initialize.vi"/>
				<Item Name="niScope Close.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/niScope Close.vi"/>
				<Item Name="niScope Get Session Reference.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Utility/niScope Get Session Reference.vi"/>
				<Item Name="niScope Configure Vertical.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Configure/Vertical/niScope Configure Vertical.vi"/>
				<Item Name="niScope LabVIEW Error.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Utility/niScope LabVIEW Error.vi"/>
				<Item Name="niScope Configure Chan Characteristics.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Configure/Vertical/niScope Configure Chan Characteristics.vi"/>
				<Item Name="niScope Configure Horizontal Timing.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Configure/Horizontal/niScope Configure Horizontal Timing.vi"/>
				<Item Name="niScope Configure Trigger (poly).vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Configure/Trigger/niScope Configure Trigger (poly).vi"/>
				<Item Name="niScope Configure Trigger Digital.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Configure/Trigger/niScope Configure Trigger Digital.vi"/>
				<Item Name="niScope trigger source digital.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope trigger source digital.ctl"/>
				<Item Name="niScope Configure Trigger Edge.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Configure/Trigger/niScope Configure Trigger Edge.vi"/>
				<Item Name="niScope trigger source.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope trigger source.ctl"/>
				<Item Name="niScope Configure Trigger Hysteresis.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Configure/Trigger/niScope Configure Trigger Hysteresis.vi"/>
				<Item Name="niScope Configure Trigger Immediate.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Configure/Trigger/niScope Configure Trigger Immediate.vi"/>
				<Item Name="niScope Configure Trigger Software.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Configure/Trigger/niScope Configure Trigger Software.vi"/>
				<Item Name="niScope Configure Trigger Window.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Configure/Trigger/niScope Configure Trigger Window.vi"/>
				<Item Name="niScope Configure Video Trigger.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Configure/Trigger/niScope Configure Video Trigger.vi"/>
				<Item Name="niScope tv event.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope tv event.ctl"/>
				<Item Name="niScope polarity.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope polarity.ctl"/>
				<Item Name="niScope signal format.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope signal format.ctl"/>
				<Item Name="niScope Initiate Acquisition.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Initiate Acquisition.vi"/>
				<Item Name="niScope Acquisition Status.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Acquisition Status.vi"/>
				<Item Name="niScope Abort.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Abort.vi"/>
				<Item Name="niScope Fetch (poly).vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Fetch (poly).vi"/>
				<Item Name="niScope Fetch Binary 8.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Fetch Binary 8.vi"/>
				<Item Name="niScope Fetch Error Chain.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Fetch Error Chain.vi"/>
				<Item Name="niScope Multi Fetch Binary 16.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Multi Fetch Binary 16.vi"/>
				<Item Name="niScope Fetch Binary 16.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Fetch Binary 16.vi"/>
				<Item Name="niScope Multi Fetch Binary 32.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Multi Fetch Binary 32.vi"/>
				<Item Name="niScope Fetch Binary 32.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Fetch Binary 32.vi"/>
				<Item Name="niScope Multi Fetch Binary 8.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Multi Fetch Binary 8.vi"/>
				<Item Name="niScope Fetch Cluster.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Fetch Cluster.vi"/>
				<Item Name="niScope Multi Fetch Cluster.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Multi Fetch Cluster.vi"/>
				<Item Name="niScope Fetch.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Fetch.vi"/>
				<Item Name="niScope Multi Fetch.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Multi Fetch.vi"/>
				<Item Name="niScope Fetch WDT.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Fetch WDT.vi"/>
				<Item Name="niScope timestamp type.ctl" Type="VI" URL="/&lt;instrlib&gt;/niScope/Controls/niScope timestamp type.ctl"/>
				<Item Name="niScope Multi Fetch WDT.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Multi Fetch WDT.vi"/>
				<Item Name="niScope Fetch Complex Double.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Fetch Complex Double.vi"/>
				<Item Name="niScope Multi Fetch Complex Double.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Multi Fetch Complex Double.vi"/>
				<Item Name="niScope Fetch Cluster Complex Double.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Fetch Cluster Complex Double.vi"/>
				<Item Name="niScope Multi Fetch Cluster Complex Double.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Multi Fetch Cluster Complex Double.vi"/>
				<Item Name="niScope Fetch Complex WDT.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Fetch Complex WDT.vi"/>
				<Item Name="niScope Multi Fetch Complex WDT.vi" Type="VI" URL="/&lt;instrlib&gt;/niScope/Acquire/Fetch/niScope Multi Fetch Complex WDT.vi"/>
				<Item Name="niSync Initialize.vi" Type="VI" URL="/&lt;instrlib&gt;/niSync/niSync.llb/niSync Initialize.vi"/>
				<Item Name="niSync Initialize (IVI).vi" Type="VI" URL="/&lt;instrlib&gt;/niSync/niSync.llb/niSync Initialize (IVI).vi"/>
				<Item Name="niSync IVI Error Converter.vi" Type="VI" URL="/&lt;instrlib&gt;/niSync/niSync.llb/niSync IVI Error Converter.vi"/>
				<Item Name="niSync Initialize (String).vi" Type="VI" URL="/&lt;instrlib&gt;/niSync/niSync.llb/niSync Initialize (String).vi"/>
				<Item Name="niSync Connect Trigger Terminals.vi" Type="VI" URL="/&lt;instrlib&gt;/niSync/niSync.llb/niSync Connect Trigger Terminals.vi"/>
				<Item Name="niSync Disconnect Trigger Terminals.vi" Type="VI" URL="/&lt;instrlib&gt;/niSync/niSync.llb/niSync Disconnect Trigger Terminals.vi"/>
				<Item Name="niSync Close.vi" Type="VI" URL="/&lt;instrlib&gt;/niSync/niSync.llb/niSync Close.vi"/>
				<Item Name="niSync Connect Clock Terminals.vi" Type="VI" URL="/&lt;instrlib&gt;/niSync/niSync.llb/niSync Connect Clock Terminals.vi"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="NI_AALPro.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALPro.lvlib"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="zeromq.lvlib" Type="Library" URL="/&lt;vilib&gt;/addons/zeromq/zeromq.lvlib"/>
				<Item Name="IVI Error Message Builder.vi" Type="VI" URL="/&lt;vilib&gt;/errclust.llb/IVI Error Message Builder.vi"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="CQL-Read-basic-string.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Read-basic-string.vi"/>
				<Item Name="CQL-close-DB.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-close-DB.vi"/>
				<Item Name="CQL-Read-basic-byte.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Read-basic-byte.vi"/>
				<Item Name="CQL-Read-basic-short.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Read-basic-short.vi"/>
				<Item Name="CQL-option.ctl" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-option.ctl"/>
				<Item Name="CQL-frame.ctl" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-frame.ctl"/>
				<Item Name="CQL-opcode.ctl" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-opcode.ctl"/>
				<Item Name="CQL-flags.ctl" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-flags.ctl"/>
				<Item Name="CQL-request-response.ctl" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-request-response.ctl"/>
				<Item Name="CQL-Read-basic-int.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Read-basic-int.vi"/>
				<Item Name="CQL-consistencylevel.ctl" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-consistencylevel.ctl"/>
				<Item Name="CQL-Write-basic-string.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Write-basic-string.vi"/>
				<Item Name="CQL-Write-basic-long-string.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Write-basic-long-string.vi"/>
				<Item Name="CQL-Read-basic-string-list.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Read-basic-string-list.vi"/>
				<Item Name="CQL-Write-basic-string-map.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Write-basic-string-map.vi"/>
				<Item Name="CQL-query.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-query.vi"/>
				<Item Name="CQL-read-header.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-read-header.vi"/>
				<Item Name="CQL-Read-basic-string-multimap.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Read-basic-string-multimap.vi"/>
				<Item Name="CQL-Read-basic-bytes.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Read-basic-bytes.vi"/>
				<Item Name="CQL-open-DB.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-open-DB.vi"/>
				<Item Name="CQL-Read-result-column-type.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Read-result-column-type.vi"/>
				<Item Name="CQL-use-keyspace.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-use-keyspace.vi"/>
				<Item Name="CQL-read-data-from-byte-array.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-read-data-from-byte-array.vi"/>
				<Item Name="CQL-conv-type-json.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-conv-type-json.vi"/>
				<Item Name="CQL-read-result.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-read-result.vi"/>
				<Item Name="CQL-Read-query-result-metadata.vi" Type="VI" URL="/&lt;vilib&gt;/database/Cassandra-driver.llb/CQL-Read-query-result-metadata.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
			</Item>
			<Item Name="user.lib" Type="Folder">
				<Item Name="MD5 Message Digest__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 Message Digest__ogtk.vi"/>
				<Item Name="MD5 Message Digest (Binary String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 Message Digest (Binary String)__ogtk.vi"/>
				<Item Name="MD5 Padding__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 Padding__ogtk.vi"/>
				<Item Name="MD5 Unrecoverable character padding__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 Unrecoverable character padding__ogtk.vi"/>
				<Item Name="MD5 ti__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 ti__ogtk.vi"/>
				<Item Name="MD5 FGHI functions__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 FGHI functions__ogtk.vi"/>
				<Item Name="MD5 F function__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 F function__ogtk.vi"/>
				<Item Name="MD5 G function__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 G function__ogtk.vi"/>
				<Item Name="MD5 H function__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 H function__ogtk.vi"/>
				<Item Name="MD5 I function__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 I function__ogtk.vi"/>
				<Item Name="MD5 Message Digest (Hexadecimal String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 Message Digest (Hexadecimal String)__ogtk.vi"/>
				<Item Name="Wait (ms)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/time/time.llb/Wait (ms)__ogtk.vi"/>
				<Item Name="MD5 Hash__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/md5/md5.llb/MD5 Hash__ogtk.vi"/>
			</Item>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="niScope_32.dll" Type="Document" URL="niScope_32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="niSync.dll" Type="Document" URL="niSync.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="bandwidth-selector.ctl" Type="VI" URL="../../BeamlineScope/bandwidth-selector.ctl"/>
			<Item Name="ZMQ-REP-load-hash.vi" Type="VI" URL="../../ZMQ-events/ZMQ-REP-load-hash.vi"/>
			<Item Name="DG535-Settings.ctl" Type="VI" URL="../../cassandra-NDCXII-interface/DG535-Settings.ctl"/>
			<Item Name="Conv-DG535Setting-To-JSON.vi" Type="VI" URL="../../cassandra-NDCXII-interface/Conv-DG535Setting-To-JSON.vi"/>
			<Item Name="DG535-Settings-DB-v1.ctl" Type="VI" URL="../../cassandra-NDCXII-interface/DG535-Settings-DB-v1.ctl"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="NDCXII-cassandra" Type="Packed Library">
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{E407E52F-2BF7-41CA-8E47-AA955F0106FD}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">NDCXII-cassandra</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{8C94DC9B-7282-4141-BC39-26A2FF1AE516}</Property>
				<Property Name="Bld_version.build" Type="Int">3</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">NDCXII-cassandra.lvlibp</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NDCXII-cassandra.lvlibp</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="PackedLib_callersAdapt" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{C31A70A2-DD67-4137-BD25-8AEB1A7FB47F}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/NDCXII-cassandra-library.lvlib</Property>
				<Property Name="Source[1].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[1].Library.atomicCopy" Type="Bool">true</Property>
				<Property Name="Source[1].Library.LVLIBPtopLevel" Type="Bool">true</Property>
				<Property Name="Source[1].preventRename" Type="Bool">true</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">Library</Property>
				<Property Name="Source[10].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[10].itemID" Type="Ref">/My Computer/NDCXII-cassandra-library.lvlib/NDCXII-CQL-read-setting.vi</Property>
				<Property Name="Source[10].type" Type="Str">VI</Property>
				<Property Name="Source[11].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[11].itemID" Type="Ref">/My Computer/NDCXII-cassandra-library.lvlib/NDCXII-CQL-trigger-device-load-setting.vi</Property>
				<Property Name="Source[11].type" Type="Str">VI</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/NDCXII-cassandra-library.lvlib/NDCXII-CQL-comment-add-shot-number.vi</Property>
				<Property Name="Source[2].type" Type="Str">VI</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/NDCXII-cassandra-library.lvlib/NDCXII-CQL-insert-background-shot-as-device.vi</Property>
				<Property Name="Source[3].type" Type="Str">VI</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/NDCXII-cassandra-library.lvlib/NDCXII-CQL-insert-comment.vi</Property>
				<Property Name="Source[4].type" Type="Str">VI</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/NDCXII-cassandra-library.lvlib/NDCXII-CQL-insert-data.vi</Property>
				<Property Name="Source[5].type" Type="Str">VI</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/NDCXII-cassandra-library.lvlib/NDCXII-CQL-insert-setting.vi</Property>
				<Property Name="Source[6].type" Type="Str">VI</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/NDCXII-cassandra-library.lvlib/NDCXII-CQL-insert-shot.vi</Property>
				<Property Name="Source[7].type" Type="Str">VI</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/NDCXII-cassandra-library.lvlib/NDCXII-CQL-insert-time-device.vi</Property>
				<Property Name="Source[8].type" Type="Str">VI</Property>
				<Property Name="Source[9].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[9].itemID" Type="Ref">/My Computer/NDCXII-cassandra-library.lvlib/NDCXII-CQL-read-device-setting-hash.vi</Property>
				<Property Name="Source[9].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">12</Property>
				<Property Name="TgtF_companyName" Type="Str">Lawrence Berkeley National Laboratory</Property>
				<Property Name="TgtF_fileDescription" Type="Str">NDCXII-cassandra</Property>
				<Property Name="TgtF_internalName" Type="Str">NDCXII-cassandra</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2015 UC Regents</Property>
				<Property Name="TgtF_productName" Type="Str">NDCXII-cassandra</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{BBCDC379-7939-4AC8-9B6A-27209731F470}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">NDCXII-cassandra.lvlibp</Property>
			</Item>
		</Item>
	</Item>
</Project>
