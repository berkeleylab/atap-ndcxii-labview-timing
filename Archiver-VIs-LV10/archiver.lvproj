﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="10008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="ArchArray.vi" Type="VI" URL="../ArchArray.vi"/>
		<Item Name="ArchArrayString.vi" Type="VI" URL="../ArchArrayString.vi"/>
		<Item Name="ArchWave.vi" Type="VI" URL="../ArchWave.vi"/>
		<Item Name="BoolArrToJSONArr.vi" Type="VI" URL="../BoolArrToJSONArr.vi"/>
		<Item Name="BoolToJSONBool.vi" Type="VI" URL="../BoolToJSONBool.vi"/>
		<Item Name="JSONArrToStrArr.vi" Type="VI" URL="../JSONArrToStrArr.vi"/>
		<Item Name="JSONStrToStr.vi" Type="VI" URL="../JSONStrToStr.vi"/>
		<Item Name="NumArrToJSONArr.vi" Type="VI" URL="../NumArrToJSONArr.vi"/>
		<Item Name="NumToJSONNum.vi" Type="VI" URL="../NumToJSONNum.vi"/>
		<Item Name="OpenTCPConn.vi" Type="VI" URL="../OpenTCPConn.vi"/>
		<Item Name="ReadValueString.vi" Type="VI" URL="../ReadValueString.vi"/>
		<Item Name="SendValue.vi" Type="VI" URL="../SendValue.vi"/>
		<Item Name="SendValueString.vi" Type="VI" URL="../SendValueString.vi"/>
		<Item Name="SendValueSuperCol.vi" Type="VI" URL="../SendValueSuperCol.vi"/>
		<Item Name="StrArrToJSONArr.vi" Type="VI" URL="../StrArrToJSONArr.vi"/>
		<Item Name="StrToJSONStr.vi" Type="VI" URL="../StrToJSONStr.vi"/>
		<Item Name="U64ArrToJSONArr.vi" Type="VI" URL="../U64ArrToJSONArr.vi"/>
		<Item Name="U64ToJSONNum.vi" Type="VI" URL="../U64ToJSONNum.vi"/>
		<Item Name="Dependencies" Type="Dependencies"/>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
