from __future__ import print_function

import zmq
import sys

import time
import select
import json
import hashlib

import picoscope
from picoscope import ps5000a as PS

import cassandra
from cassandra.cqlengine import columns
from cassandra.cqlengine.models import Model
from cassandra.cqlengine.connection import setup

class Data(Model):
    devicename = columns.Text(partition_key=True)
    date = columns.Integer(partition_key=True)
    eventtime = columns.DateTime(primary_key=True)
    value = columns.Float(primary_key=True)


class Setting(Model):
    hash = columns.Text(primary_key=True)
    data = columns.Text()
    version = columns.Integer()


class Shot(Model):
    lvtimestamp = columns.BigInt(primary_key=True)
    devicename = columns.Text(primary_key=True)
    data = columns.Text()
    settinghash = columns.Text()
    version = columns.Integer()

# set according to enum in labview
trigger_ch = ['A', 'B', 'C', 'D', 'External']
trigger_direction = ['Rising', 'Falling']

# some default variables or variables to store current
current_hash = ""
json_last_shot = ""

# default setting for start up; use json string from labview
setting_default = json.loads('{"Timebase":0.0015,"Record Length":4000,"Bit resolution":12,"reference position":10,"Voltage range":15,"Voltage offset":0,"AC/DC":1,"bandwidth filter":0,"External Gain/Attenuator":1,"Calibration factor":1,"Voltage range 2":15,"Voltage offset 2":0,"AC/DC 2":1,"bandwidth filter 2":0,"External Gain/Attenuator 2":1,"Calibration factor 2":0,"Voltage range 3":1,"Voltage offset 3":0,"AC/DC 3":1,"bandwidth filter 3":0,"External Gain/Attenuator 3":1,"Calibration factor 3":0,"Voltage range 4":1,"Voltage offset 4":0,"AC/DC 4":1,"bandwidth filter 4":0,"External Gain/Attenuator 4":1,"Calibration factor 4":0,"Trigger channel":3,"Trigger edge":1,"Trigger level":-0.05,"Ch enable":true,"Ch enable 2":true,"Ch enable 3":true,"Ch enable 4":true}')

setting = setting_default
A = None
B = None
C = None
D = None

dt = 1

DEVICENAME = "picoscope01"

def print_setting():
    global setting
    print("Resolution: ", setting['Bit resolution'])
    print("Trigger channel:", trigger_ch[setting['Trigger channel']])
    print("Trigger level:", setting['Trigger level'])
    print("Trigger edge:", trigger_direction[setting['Trigger edge']])
    print("Ch A: ",
            "  enabled: ", setting['Ch enable'],
            "  Coupling:", setting['AC/DC'],
            "  Bwlimit:", setting['bandwidth filter']==20,
            "  Vrange:", setting['Voltage range'],
            "  Voff:", setting['Voltage offset'])
    print("Ch B: ",
            "  enabled: ", setting['Ch enable 2'],
            "  Coupling:", setting['AC/DC 2'],
            "  Bwlimit:", setting['bandwidth filter 2']==20,
            "  Vrange:", setting['Voltage range 2'],
            "  Voff:", setting['Voltage offset 2'])
    print("Ch C: ",
            "  enabled: ", setting['Ch enable 3'],
            "  Coupling:", setting['AC/DC 3'],
            "  Bwlimit:", setting['bandwidth filter 3']==20,
            "  Vrange:", setting['Voltage range 3'],
            "  Voff:", setting['Voltage offset 3'])
    print("Ch D: ",
            "  enabled: ", setting['Ch enable 4'],
            "  Coupling:", setting['AC/DC 4'],
            "  Bwlimit:", setting['bandwidth filter 4']==20,
            "  Vrange:", setting['Voltage range 4'],
            "  Voff:", setting['Voltage offset 4'])

def setup_scope():
    global scope, dt, setting, setting_default
    if setting['Timebase'] <= 0:
        print("ERROR: timebase must be > 0 ")
        setting = setting_default
        return
    if setting['Record Length'] <= 0:
        print("ERROR: record length must be > 0 ")
        setting = setting_default
        return

    scope.stop()
    scope.setResolution(str(setting['Bit resolution']))

    scope.setChannel('A', setting['AC/DC'],
                     VRange=setting['Voltage range'],
                     VOffset=setting['Voltage offset'],
                     enabled=setting['Ch enable'],
                     BWLimited=(setting['bandwidth filter'] == 20))
    scope.setChannel('B', setting['AC/DC 2'],
                     VRange=setting['Voltage range 2'],
                     VOffset=setting['Voltage offset 2'],
                     enabled=setting['Ch enable 2'],
                     BWLimited=(setting['bandwidth filter 2'] == 20))
    scope.setChannel('C', setting['AC/DC 3'],
                     VRange=setting['Voltage range 3'],
                     VOffset=setting['Voltage offset 3'],
                     enabled=setting['Ch enable 3'],
                     BWLimited=(setting['bandwidth filter 3'] == 20))
    scope.setChannel('D', setting['AC/DC 4'],
                     VRange=setting['Voltage range 4'],
                     VOffset=setting['Voltage offset 4'],
                     enabled=setting['Ch enable 4'],
                     BWLimited=(setting['bandwidth filter 4'] == 20))

    obs_duration = setting['Timebase']
    sampling_interval = obs_duration / setting['Record Length']

    # should be able to go down to 1 ns, seems to be a bug
    if sampling_interval < 8e-9:
        sampling_interval = 8e-9
        obs_duration = sampling_interval * int(obs_duration/sampling_interval)

    print("setting time: ", sampling_interval, obs_duration)

    a = scope.setSamplingInterval(sampling_interval, obs_duration)
    dt, no, _ = a
    print("actual one used: ", a)

    trch = trigger_ch[setting['Trigger channel']]
    trV = setting['Trigger level']
    if trch == "A":
        VRange = setting['Voltage range']
        VOffset = setting['Voltage offset']
    if trch == "B":
        VRange = setting['Voltage range 2']
        VOffset = setting['Voltage offset 2']
    if trch == "C":
        VRange = setting['Voltage range 3']
        VOffset = setting['Voltage offset 3']
    if trch == "D":
        VRange = setting['Voltage range 4']
        VOffset = setting['Voltage offset 4']
    if trch == "External":
        VRange = 5
        VOffset = 0
    if trV < 0:
        trV = max(trV, -VRange+VOffset)
    else:
        trV = min(trV, VRange-VOffset)

    print("setting trigger: ", trch, trV, trigger_direction[setting['Trigger edge']])
    scope.setSimpleTrigger(trch,
                           threshold_V=trV,
                           timeout_ms=0,
                           direction=trigger_direction[setting['Trigger edge']])

    scope.runBlock(pretrig=setting['reference position']/100.0)

# setup database and picoscope
print("Connecting to Cassandra database")
setup(hosts=['128.3.58.20'], default_keyspace='test', protocol_version=3)

scope = PS.PS5000a()
print("Setting up scope...")
setup_scope()
print("   Scope ready for operation")

context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind("tcp://128.3.58.31:6000")

TIME = context.socket(zmq.REQ)
TIME.connect('tcp://128.3.58.16:6000')

poller = zmq.Poller()
poller.register(socket, zmq.POLLIN)
print("ZMQ setup done")

keep_running = True
while keep_running:
    sleep = True  # unless something happens, we sleep for 0.2s
                  # this gets checked at the end

    # check ZMQ
    socks = dict(poller.poll(0))
    if socket in socks and socks[socket] == zmq.POLLIN:
        sleep = False
        command = socket.recv_string()

        if command == "get hash":
            socket.send_string(current_hash)
        if command == "get setting":
            json_setting = json.dumps(setting, sort_keys=True)
            socket.send_string(json_setting)
        elif command == "get graph":
            if setting is None:
                print("no setting data")
                socket.send_string('')
            else:
                print("sending graph data. Len=", len(json_last_shot))
                socket.send_string(json_last_shot)
        else:
            print("request to load hash:", command)
            socket.send_string(DEVICENAME)

            new = Setting.objects(Setting.hash==command).first().data
            setting = json.loads(new)
            setup_scope()
            current_hash = command
                
    # check scope for trigger
    try:
        ready = scope.isReady()
    except:
        # try reconnect to scope on error
        print("!!! Lost scope connection... !!!")
        del scope
        time.sleep(1)
        scope = PS.PS5000a()
        setup_scope()
        print("    reconnected")
        ready = False
    if ready:
        sleep = False
        print("*"*24)
        print("***** got trigger *****")
        print("*"*24)
        if setting['Ch enable']:
            A = list(scope.getDataV('A'))
        else:
            A = [0] * scope.noSamples
        if setting['Ch enable 2']:
            B = list(scope.getDataV('B'))
        else:
            B = [0] * scope.noSamples
        if setting['Ch enable 3']:
            C = list(scope.getDataV('C'))
        else:
            C = [0] * scope.noSamples
        if setting['Ch enable 4']:
            D = list(scope.getDataV('D'))
        else:
            D = [0] * scope.noSamples
        
        TIME.send_string(DEVICENAME)
        timestamp = int(TIME.recv_string())

        print("got timestamp:", timestamp)

        j = {'wave0': A, 'wave1': B,
             'wave2': C, 'wave3': D,
             'Actual Record Length': scope.noSamples,
             'Actual Sample Rate':1.0/scope.sampleInterval}
        data = json.dumps(j, sort_keys=True)
        m = hashlib.md5()
        m.update(bytearray(data, 'utf-8'))
        
        Shot.create(settinghash=current_hash,
                    lvtimestamp=timestamp, data=data,
                    devicename=DEVICENAME, version=1)
        print("wrote data to Cassandra")

        R = setting['reference position']
        T = scope.noSamples*dt
        T0 = -T*R/100.
        
        j = [{'relativeInitialX': T0, 
              'xIncrement': dt, 
              'wfm': A},
             {'relativeInitialX': T0, 
              'xIncrement': dt, 
              'wfm': B},
             {'relativeInitialX': T0, 
              'xIncrement': dt, 
              'wfm': C},
             {'relativeInitialX': T0, 
              'xIncrement': dt, 
              'wfm': D}]
        json_last_shot = json.dumps(j, sort_keys=True)

        scope.runBlock(pretrig=setting['reference position']/100.0)

    # check stdin
    i, o, e = select.select([sys.stdin], [], [], 0.0)
    if len(i) > 0 and i[0] == sys.stdin:
        sleep = False
        input = sys.stdin.readline()
        input = input[:-1]  # remove \n
        print("got input", input)
        if input in ['bye', 'exit', 'quit']:
            print("exiting")
            keep_running = False
        elif input == 'setting':
            if setting is None:
                print("nothing set")
            else:
                print("*** these are the requested settings, not necessaryly the ones of the scope ***")
                print_setting()

    sys.stdout.flush()
    if sleep:
        time.sleep(0.2)

scope.close()
print("Good Bye!")
