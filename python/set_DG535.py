from __future__ import print_function

import zmq
import sys

import time
import select
import json
import hashlib

import control_DG535

import cassandra
from cassandra.cqlengine import columns
from cassandra.cqlengine.models import Model
from cassandra.cqlengine.connection import setup

class Setting(Model):
    hash = columns.Text(primary_key=True)
    data = columns.Text()
    version = columns.Integer()

class Shot(Model):
    lvtimestamp = columns.BigInt(primary_key=True)
    devicename = columns.Text(primary_key=True)
    data = columns.Text()
    settinghash = columns.Text()
    version = columns.Integer()

#sets up database and defines trigger module names

print("Connecting to Cassandra database...")
setup(hosts=['128.3.58.20'], default_keyspace='test', protocol_version=3)

FilamentTiming = control_DG535.DG535("dg535_A")
NDCXIIDelay = control_DG535.DG535("dg535_B")

#sets up zmq
context = zmq.Context()

socket1 = context.socket(zmq.REP)
socket1.bind("tcp://128.3.58.30:6001")

socket2 = context.socket(zmq.REP)
socket2.bind("tcp://128.3.58.30:6002")

TIME = context.socket(zmq.SUB)
TIME.setsockopt(zmq.SUBSCRIBE,'')
TIME.connect("tcp://128.3.58.16:6001")


poller = zmq.Poller()
poller.register(socket1, zmq.POLLIN)
poller.register(socket2, zmq.POLLIN)
poller.register(TIME, zmq.POLLIN)
print("ZMQ setup complete")


current_hash1 = ""
current_hash2 = ""

json_last_shot = ""
keep_running = True

while keep_running:
    sleep = True
    
    socks = dict(poller.poll(0))
    if socket1 in socks and socks[socket1] == zmq.POLLIN:
        sleep = False
        command1 = socket1.recv_string()

        if command1 == "get hash":
            socket1.send_string(current_hash1)
            print("Sent current hash1 to LabVIEW")

        if command1 == "get setting":
            FilamentTiming.read_settings()
            json_setting1 = json.dumps(FilamentTiming.settings)
            socket1.send_string(json_setting1)
            print("Sent current device1 settings to LabVIEW")
        else:
            print("Request to load hash:", command1)
            socket1.send("source-timing1")
    
            new1 = Setting.objects(Setting.hash==command1).first().data
            setting1 = json.loads(new1)
            FilamentTiming.apply_settings(setting1)
            current_hash1 = command1
            print("Applied settings to device1 from hash:", current_hash1) 


    if socket2 in socks and socks[socket2] == zmq.POLLIN:
        sleep = False
        command2 = socket2.recv_string()
            
        if command2 == "get hash":
            socket2.send_string(current_hash2)
            print("Sent current hash2 to LabVIEW")

        if command2 == "get setting":
            NDCXIIDelay.read_settings()
            json_setting2 = json.dumps(NDCXIIDelay.settings)
            socket2.send_string(json_setting2)
            print("Sent current device2 settings to LabVIEW")

        else:
            print("Request to load hash:", command2)
            socket2.send("source-timing2")
                        
            new2 = Setting.objects(Setting.hash==command2).first().data
            setting2 = json.loads(new2)
            NDCXIIDelay.apply_settings(setting2)
            current_hash2 = command2
            print("Applied settings to device2 from hash:", current_hash2) 


    if TIME in socks and socks[TIME] == zmq.POLLIN:
        timestamp = int(TIME.recv_string())
        print("Got shot number")
            
        FilamentTiming.read_settings()
        data1 = json.dumps(FilamentTiming.settings, sort_keys=True)
        m1 = hashlib.md5()
        m1.update(bytearray(data1, "utf-8"))
        Setting.create(hash=current_hash1, data=data1, version=0)
        Shot.create(settinghash=current_hash1,
            lvtimestamp=timestamp, data="",
                devicename="source-timing1", version=0)
                         
        NDCXIIDelay.read_settings()
        data2 = json.dumps(NDCXIIDelay.settings, sort_keys=True)
        m2 = hashlib.md5()
        m2.update(bytearray(data2, "utf-8"))
        Setting.create(hash=current_hash2, data=data2, version=0)
        Shot.create(settinghash=current_hash2,
            lvtimestamp=timestamp, data="",
                devicename="source-timing2", version=0)
        print("Wrote device settings to DB from shot number:", timestamp)
                                  
        
    # check stdin
    i, o, e = select.select([sys.stdin], [], [], 0.0)
    if len(i) > 0 and i[0] == sys.stdin:
        sleep = False
        input = sys.stdin.readline()
        input = input[:-1]  # remove \n
        print("got input", input)
        if input in ['bye', 'exit', 'quit']:
            print("exiting")
            keep_running = False
        elif input == 'setting1':
            if setting1 is None:
                print("nothing set")
            else:
                print("Settings for module 1:")
                FilamentTiming.read_settings()
                print(FilamentTiming.settings)
                      
        elif input == 'setting2':
            if setting2 is None:
                print("nothing set")
            else:
                print("Settings for module 2:")
                NDCXIIDelay.read_settings()
                print(NDCXIIDelay.settings)


    sys.stdout.flush()
    if sleep:
        time.sleep(0.2)
                      
print("Done")


