﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="13008000">
	<Property Name="NI.Lib.Icon" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91P&lt;!FNA#^M#5Y6M96NA"R[WM#WQ"&lt;9A0ZYR'E?G!WPM1$AN&gt;@S(!ZZQG&amp;0%VLZ'@)H8:_X\&lt;^P(^7@8H\4Y;"`NX\;8JZPUX@@MJXC]C.3I6K5S(F/^DHTE)R`ZS%@?]J;XP/5N&lt;XH*3V\SEJ?]Z#F0?=J4HP+5&lt;Y=]Z#%0/&gt;+9@%QU"BU$D-YI-4[':XC':XB]D?%:HO%:HO(2*9:H?):H?)&lt;(&lt;4%]QT-]QT-]BNIEMRVSHO%R@$20]T20]T30+;.Z'K".VA:OAW"%O^B/GK&gt;ZGM&gt;J.%`T.%`T.)`,U4T.UTT.UTROW6;F.]XDE0-9*IKH?)KH?)L(U&amp;%]R6-]R6-]JIPC+:[#+"/7Q2'CX&amp;1[F#`&amp;5TR_2@%54`%54`'YN$WBWF&lt;GI8E==J\E3:\E3:\E-51E4`)E4`)EDW%D?:)H?:)H?5Q6S:-]S:-A;6,42RIMX:A[J3"Z`'S\*&lt;?HV*MENS.C&lt;&gt;Z9GT,7:IOVC7*NDFA00&gt;&lt;$D0719CV_L%7.N6CR&amp;C(7(R=,(1M4;Z*9.T][RNXH46X62:X632X61?X6\H(L8_ZYP^`D&gt;LP&amp;^8K.S_53Z`-Z4K&gt;4()`(/"Q/M&gt;`P9\@&lt;P&lt;U'PDH?8AA`XUMPTP_EXOF`[8`Q&lt;IT0]?OYVOA(5/(_Z!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">318799872</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="PXI0Scope5Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope5ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#7EA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!(E"!!!(`````!!!118*D;'FW:3"S:8.Q&lt;WZT:1!!&amp;%!B$E&amp;S9WBJ&gt;G5A5X2B&gt;(6T!!!,1!A!"&amp;2J&lt;75!!"Z!5!!$!!%!!A!$%%&amp;S9WBJ&gt;G5A5G6B:'*B9WM!!!%!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope6Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope6ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#7EA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!(E"!!!(`````!!!118*D;'FW:3"S:8.Q&lt;WZT:1!!&amp;%!B$E&amp;S9WBJ&gt;G5A5X2B&gt;(6T!!!,1!A!"&amp;2J&lt;75!!"Z!5!!$!!%!!A!$%%&amp;S9WBJ&gt;G5A5G6B:'*B9WM!!!%!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope9Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope9ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#7EA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!(E"!!!(`````!!!118*D;'FW:3"S:8.Q&lt;WZT:1!!&amp;%!B$E&amp;S9WBJ&gt;G5A5X2B&gt;(6T!!!,1!A!"&amp;2J&lt;75!!"Z!5!!$!!%!!A!$%%&amp;S9WBJ&gt;G5A5G6B:'*B9WM!!!%!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope10Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope10ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#7EA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!(E"!!!(`````!!!118*D;'FW:3"S:8.Q&lt;WZT:1!!&amp;%!B$E&amp;S9WBJ&gt;G5A5X2B&gt;(6T!!!,1!A!"&amp;2J&lt;75!!"Z!5!!$!!%!!A!$%%&amp;S9WBJ&gt;G5A5G6B:'*B9WM!!!%!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope11Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope11ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#7EA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!(E"!!!(`````!!!118*D;'FW:3"S:8.Q&lt;WZT:1!!&amp;%!B$E&amp;S9WBJ&gt;G5A5X2B&gt;(6T!!!,1!A!"&amp;2J&lt;75!!"Z!5!!$!!%!!A!$%%&amp;S9WBJ&gt;G5A5G6B:'*B9WM!!!%!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope12Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope12ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#7EA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!(E"!!!(`````!!!118*D;'FW:3"S:8.Q&lt;WZT:1!!&amp;%!B$E&amp;S9WBJ&gt;G5A5X2B&gt;(6T!!!,1!A!"&amp;2J&lt;75!!"Z!5!!$!!%!!A!$%%&amp;S9WBJ&gt;G5A5G6B:'*B9WM!!!%!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope15Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope15ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#7EA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!(E"!!!(`````!!!118*D;'FW:3"S:8.Q&lt;WZT:1!!&amp;%!B$E&amp;S9WBJ&gt;G5A5X2B&gt;(6T!!!,1!A!"&amp;2J&lt;75!!"Z!5!!$!!%!!A!$%%&amp;S9WBJ&gt;G5A5G6B:'*B9WM!!!%!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope16Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope16ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#7EA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!(E"!!!(`````!!!118*D;'FW:3"S:8.Q&lt;WZT:1!!&amp;%!B$E&amp;S9WBJ&gt;G5A5X2B&gt;(6T!!!,1!A!"&amp;2J&lt;75!!"Z!5!!$!!%!!A!$%%&amp;S9WBJ&gt;G5A5G6B:'*B9WM!!!%!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope17Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope17ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#7EA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!(E"!!!(`````!!!118*D;'FW:3"S:8.Q&lt;WZT:1!!&amp;%!B$E&amp;S9WBJ&gt;G5A5X2B&gt;(6T!!!,1!A!"&amp;2J&lt;75!!"Z!5!!$!!%!!A!$%%&amp;S9WBJ&gt;G5A5G6B:'*B9WM!!!%!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope18Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope18ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#7EA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!(E"!!!(`````!!!118*D;'FW:3"S:8.Q&lt;WZT:1!!&amp;%!B$E&amp;S9WBJ&gt;G5A5X2B&gt;(6T!!!,1!A!"&amp;2J&lt;75!!"Z!5!!$!!%!!A!$%%&amp;S9WBJ&gt;G5A5G6B:'*B9WM!!!%!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Scope9Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Scope9ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#7EA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!(E"!!!(`````!!!118*D;'FW:3"S:8.Q&lt;WZT:1!!&amp;%!B$E&amp;S9WBJ&gt;G5A5X2B&gt;(6T!!!,1!A!"&amp;2J&lt;75!!"Z!5!!$!!%!!A!$%%&amp;S9WBJ&gt;G5A5G6B:'*B9WM!!!%!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Scope10Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Scope10ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#7EA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!(E"!!!(`````!!!118*D;'FW:3"S:8.Q&lt;WZT:1!!&amp;%!B$E&amp;S9WBJ&gt;G5A5X2B&gt;(6T!!!,1!A!"&amp;2J&lt;75!!"Z!5!!$!!%!!A!$%%&amp;S9WBJ&gt;G5A5G6B:'*B9WM!!!%!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Scope11Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Scope11ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#7EA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!(E"!!!(`````!!!118*D;'FW:3"S:8.Q&lt;WZT:1!!&amp;%!B$E&amp;S9WBJ&gt;G5A5X2B&gt;(6T!!!,1!A!"&amp;2J&lt;75!!"Z!5!!$!!%!!A!$%%&amp;S9WBJ&gt;G5A5G6B:'*B9WM!!!%!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Scope12Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Scope12ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#7EA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!(E"!!!(`````!!!118*D;'FW:3"S:8.Q&lt;WZT:1!!&amp;%!B$E&amp;S9WBJ&gt;G5A5X2B&gt;(6T!!!,1!A!"&amp;2J&lt;75!!"Z!5!!$!!%!!A!$%%&amp;S9WBJ&gt;G5A5G6B:'*B9WM!!!%!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Scope15Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Scope15SArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#7EA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!(E"!!!(`````!!!118*D;'FW:3"S:8.Q&lt;WZT:1!!&amp;%!B$E&amp;S9WBJ&gt;G5A5X2B&gt;(6T!!!,1!A!"&amp;2J&lt;75!!"Z!5!!$!!%!!A!$%%&amp;S9WBJ&gt;G5A5G6B:'*B9WM!!!%!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Scope16Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Scope16ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#7EA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!(E"!!!(`````!!!118*D;'FW:3"S:8.Q&lt;WZT:1!!&amp;%!B$E&amp;S9WBJ&gt;G5A5X2B&gt;(6T!!!,1!A!"&amp;2J&lt;75!!"Z!5!!$!!%!!A!$%%&amp;S9WBJ&gt;G5A5G6B:'*B9WM!!!%!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Scope17Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Scope17ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#7EA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!(E"!!!(`````!!!118*D;'FW:3"S:8.Q&lt;WZT:1!!&amp;%!B$E&amp;S9WBJ&gt;G5A5X2B&gt;(6T!!!,1!A!"&amp;2J&lt;75!!"Z!5!!$!!%!!A!$%%&amp;S9WBJ&gt;G5A5G6B:'*B9WM!!!%!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Scope18Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Scope18ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#7EA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!(E"!!!(`````!!!118*D;'FW:3"S:8.Q&lt;WZT:1!!&amp;%!B$E&amp;S9WBJ&gt;G5A5X2B&gt;(6T!!!,1!A!"&amp;2J&lt;75!!"Z!5!!$!!%!!A!$%%&amp;S9WBJ&gt;G5A5G6B:'*B9WM!!!%!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope4Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope4ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#7EA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!(E"!!!(`````!!!118*D;'FW:3"S:8.Q&lt;WZT:1!!&amp;%!B$E&amp;S9WBJ&gt;G5A5X2B&gt;(6T!!!,1!A!"&amp;2J&lt;75!!"Z!5!!$!!%!!A!$%%&amp;S9WBJ&gt;G5A5G6B:'*B9WM!!!%!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope5Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope5ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#7EA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!(E"!!!(`````!!!118*D;'FW:3"S:8.Q&lt;WZT:1!!&amp;%!B$E&amp;S9WBJ&gt;G5A5X2B&gt;(6T!!!,1!A!"&amp;2J&lt;75!!"Z!5!!$!!%!!A!$%%&amp;S9WBJ&gt;G5A5G6B:'*B9WM!!!%!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope6Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope6ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#7EA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!(E"!!!(`````!!!118*D;'FW:3"S:8.Q&lt;WZT:1!!&amp;%!B$E&amp;S9WBJ&gt;G5A5X2B&gt;(6T!!!,1!A!"&amp;2J&lt;75!!"Z!5!!$!!%!!A!$%%&amp;S9WBJ&gt;G5A5G6B:'*B9WM!!!%!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope8Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope8ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#7EA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!(E"!!!(`````!!!118*D;'FW:3"S:8.Q&lt;WZT:1!!&amp;%!B$E&amp;S9WBJ&gt;G5A5X2B&gt;(6T!!!,1!A!"&amp;2J&lt;75!!"Z!5!!$!!%!!A!$%%&amp;S9WBJ&gt;G5A5G6B:'*B9WM!!!%!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope9Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope9ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#7EA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!(E"!!!(`````!!!118*D;'FW:3"S:8.Q&lt;WZT:1!!&amp;%!B$E&amp;S9WBJ&gt;G5A5X2B&gt;(6T!!!,1!A!"&amp;2J&lt;75!!"Z!5!!$!!%!!A!$%%&amp;S9WBJ&gt;G5A5G6B:'*B9WM!!!%!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope10Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope10ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#7EA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!(E"!!!(`````!!!118*D;'FW:3"S:8.Q&lt;WZT:1!!&amp;%!B$E&amp;S9WBJ&gt;G5A5X2B&gt;(6T!!!,1!A!"&amp;2J&lt;75!!"Z!5!!$!!%!!A!$%%&amp;S9WBJ&gt;G5A5G6B:'*B9WM!!!%!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope11Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope11ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#7EA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!(E"!!!(`````!!!118*D;'FW:3"S:8.Q&lt;WZT:1!!&amp;%!B$E&amp;S9WBJ&gt;G5A5X2B&gt;(6T!!!,1!A!"&amp;2J&lt;75!!"Z!5!!$!!%!!A!$%%&amp;S9WBJ&gt;G5A5G6B:'*B9WM!!!%!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope12Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope12ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#7EA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!(E"!!!(`````!!!118*D;'FW:3"S:8.Q&lt;WZT:1!!&amp;%!B$E&amp;S9WBJ&gt;G5A5X2B&gt;(6T!!!,1!A!"&amp;2J&lt;75!!"Z!5!!$!!%!!A!$%%&amp;S9WBJ&gt;G5A5G6B:'*B9WM!!!%!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope14Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope14ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#7EA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!(E"!!!(`````!!!118*D;'FW:3"S:8.Q&lt;WZT:1!!&amp;%!B$E&amp;S9WBJ&gt;G5A5X2B&gt;(6T!!!,1!A!"&amp;2J&lt;75!!"Z!5!!$!!%!!A!$%%&amp;S9WBJ&gt;G5A5G6B:'*B9WM!!!%!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope15Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope15ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#7EA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!(E"!!!(`````!!!118*D;'FW:3"S:8.Q&lt;WZT:1!!&amp;%!B$E&amp;S9WBJ&gt;G5A5X2B&gt;(6T!!!,1!A!"&amp;2J&lt;75!!"Z!5!!$!!%!!A!$%%&amp;S9WBJ&gt;G5A5G6B:'*B9WM!!!%!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope16Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope16ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#7EA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!(E"!!!(`````!!!118*D;'FW:3"S:8.Q&lt;WZT:1!!&amp;%!B$E&amp;S9WBJ&gt;G5A5X2B&gt;(6T!!!,1!A!"&amp;2J&lt;75!!"Z!5!!$!!%!!A!$%%&amp;S9WBJ&gt;G5A5G6B:'*B9WM!!!%!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope17Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope17ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#7EA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!(E"!!!(`````!!!118*D;'FW:3"S:8.Q&lt;WZT:1!!&amp;%!B$E&amp;S9WBJ&gt;G5A5X2B&gt;(6T!!!,1!A!"&amp;2J&lt;75!!"Z!5!!$!!%!!A!$%%&amp;S9WBJ&gt;G5A5G6B:'*B9WM!!!%!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope18Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope18ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#7EA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!(E"!!!(`````!!!118*D;'FW:3"S:8.Q&lt;WZT:1!!&amp;%!B$E&amp;S9WBJ&gt;G5A5X2B&gt;(6T!!!,1!A!"&amp;2J&lt;75!!"Z!5!!$!!%!!A!$%%&amp;S9WBJ&gt;G5A5G6B:'*B9WM!!!%!"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI4Scope4Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI4Scope4ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#'AA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!&amp;E"!!!(`````!!!)5G6T='^O=W5!!!N!#!!%6'FN:1!!$%!B"F.U982V=Q!!(E"1!!-!!1!#!!-118*D;'FW:3"3:7&amp;E9G&amp;D;Q!!!1!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI4Scope5Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI4Scope5ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#'AA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!&amp;E"!!!(`````!!!)5G6T='^O=W5!!!N!#!!%6'FN:1!!$%!B"F.U982V=Q!!(E"1!!-!!1!#!!-118*D;'FW:3"3:7&amp;E9G&amp;D;Q!!!1!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI4Scope6Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI4Scope6ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#'AA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!&amp;E"!!!(`````!!!)5G6T='^O=W5!!!N!#!!%6'FN:1!!$%!B"F.U982V=Q!!(E"1!!-!!1!#!!-118*D;'FW:3"3:7&amp;E9G&amp;D;Q!!!1!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI4Scope8Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI4Scope8ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#'AA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!&amp;E"!!!(`````!!!)5G6T='^O=W5!!!N!#!!%6'FN:1!!$%!B"F.U982V=Q!!(E"1!!-!!1!#!!-118*D;'FW:3"3:7&amp;E9G&amp;D;Q!!!1!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI4Scope9Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI4Scope9ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#'AA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!&amp;E"!!!(`````!!!)5G6T='^O=W5!!!N!#!!%6'FN:1!!$%!B"F.U982V=Q!!(E"1!!-!!1!#!!-118*D;'FW:3"3:7&amp;E9G&amp;D;Q!!!1!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI4Scope10Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI4Scope10ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#'AA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!&amp;E"!!!(`````!!!)5G6T='^O=W5!!!N!#!!%6'FN:1!!$%!B"F.U982V=Q!!(E"1!!-!!1!#!!-118*D;'FW:3"3:7&amp;E9G&amp;D;Q!!!1!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI4Scope11Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI4Scope11ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#'AA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!&amp;E"!!!(`````!!!)5G6T='^O=W5!!!N!#!!%6'FN:1!!$%!B"F.U982V=Q!!(E"1!!-!!1!#!!-118*D;'FW:3"3:7&amp;E9G&amp;D;Q!!!1!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI4Scope12Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI4Scope12ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#'AA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!&amp;E"!!!(`````!!!)5G6T='^O=W5!!!N!#!!%6'FN:1!!$%!B"F.U982V=Q!!(E"1!!-!!1!#!!-118*D;'FW:3"3:7&amp;E9G&amp;D;Q!!!1!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI4Scope14Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI4Scope14ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#'AA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!&amp;E"!!!(`````!!!)5G6T='^O=W5!!!N!#!!%6'FN:1!!$%!B"F.U982V=Q!!(E"1!!-!!1!#!!-118*D;'FW:3"3:7&amp;E9G&amp;D;Q!!!1!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI4Scope15Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI4Scope15ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#'AA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!&amp;E"!!!(`````!!!)5G6T='^O=W5!!!N!#!!%6'FN:1!!$%!B"F.U982V=Q!!(E"1!!-!!1!#!!-118*D;'FW:3"3:7&amp;E9G&amp;D;Q!!!1!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI4Scope16Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI4Scope16ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#'AA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!&amp;E"!!!(`````!!!)5G6T='^O=W5!!!N!#!!%6'FN:1!!$%!B"F.U982V=Q!!(E"1!!-!!1!#!!-118*D;'FW:3"3:7&amp;E9G&amp;D;Q!!!1!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI4Scope17Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI4Scope17ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#'AA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!&amp;E"!!!(`````!!!)5G6T='^O=W5!!!N!#!!%6'FN:1!!$%!B"F.U982V=Q!!(E"1!!-!!1!#!!-118*D;'FW:3"3:7&amp;E9G&amp;D;Q!!!1!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI4Scope18Archive" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!":61!!!"-!A!!!!!!$!!R!)1:&amp;&lt;G&amp;C&lt;'5!!"J!-0````]12'&amp;U93"E:8.D=GFQ&gt;'FP&lt;A!!'E"1!!)!!!!"$U&amp;S9WBJ&gt;G5A1W^O&gt;(*P&lt;!!"!!)!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI4Scope18ArchiveReadback" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#'AA!!!"-!A!!!!!!&amp;!"J!-0````]118*D;'FW:3"3:8.Q&lt;WZT:1!!&amp;E"!!!(`````!!!)5G6T='^O=W5!!!N!#!!%6'FN:1!!$%!B"F.U982V=Q!!(E"1!!-!!1!#!!-118*D;'FW:3"3:7&amp;E9G&amp;D;Q!!!1!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
</Library>
