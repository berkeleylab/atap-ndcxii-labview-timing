﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="10008000">
	<Property Name="NI.Lib.Icon" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"=&gt;MQ%!8143;(8.6"2CVM#WJ",7Q,SN&amp;(N&lt;!NK!7VM#WI"&lt;8A0$%94UZ2$P%E"Y.?G@I%A7=11U&gt;M\7P%FXB^VL\`NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAG_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y!#/7SO!!!!!!</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="GTime" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-s0\GTiming\GTime</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"!!A!!!!!!"!!V!#!!'65FO&gt;$9U!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope5" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi0\AcqLib\PXI0ScopeData5</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;03Q%!!"!!A!!!!!!0!"F!#A!397.U&gt;7&amp;M)(.B&lt;8"M:3"S982F!!!51#%/986U&lt;S"U=GFH:W6S:71!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!":!5!!$!!)!!Q!%#76S=G^S)'^V&gt;!!81!I!%(*F&lt;'&amp;U;8:F37ZJ&gt;'FB&lt;&amp;A!!"&amp;!#A!+?%FO9X*F&lt;76O&gt;!!!"1!+!!!11%!!!@````]!#!.X:GU!$!"1!!-!"A!(!!E!(%"!!!(`````!!I/6W&amp;W:7:P=GUA2X*B='A!!"N!!Q!597.U&gt;7&amp;M)(*F9W^S:#"M:7ZH&gt;'A!!!V!#!!'&gt;'FN:49U!!!C1&amp;!!"A!!!!%!"1!,!!Q!$1Z49W^Q:3"3:7&amp;E9G&amp;D;Q!!!1!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope5Settings" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi0\AcqLib\PXI0ScopeControl5</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">4</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">niScope trigger coupling.ctl</Property>
		<Property Name="typedefName2" Type="Str">niScope trigger slope.ctl</Property>
		<Property Name="typedefName3" Type="Str">niScope trigger window mode.ctl</Property>
		<Property Name="typedefName4" Type="Str">niScope vertical coupling.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"!!A!!!!!!9!":!)2"F&lt;G:P=G.F)(*F97RU;7VF!!!61!I!$H:F=H2J9W&amp;M)(*B&lt;G&gt;F!!!61!I!$WVJ&lt;C"T97VQ&lt;'5A=G&amp;U:1!81!-!%7VJ&lt;C"S:7.P=G1A&lt;'6O:X2I!"6!#A!0&gt;G6S&gt;'FD97QA&lt;W:G=W6U!"&gt;!#A!2=(*P9G5A982U:7ZV982J&lt;WY!&amp;5!(!!^J&lt;H"V&gt;#"J&lt;8"F:'&amp;O9W5!(5!+!"&gt;N98BJ&lt;86N)'FO=(6U)':S:8&amp;V:7ZD?1""!0%!!!!!!!!!!2VO;6.D&lt;X"F)(:F=H2J9W&amp;M)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!-!%8:F=H2J9W&amp;M)'.P&gt;8"M;7ZH!""!)1J&amp;&lt;G&amp;C&lt;'5A6%F4!!!`1"9!"12&amp;:'&gt;F#EBZ=X2F=G6T;8-(2'FH;82B&lt;!:8;7ZE&lt;X=*37VN:72J982F!!!-&gt;(*J:W&gt;F=C"U?8"F!!!41!I!$82S;7&gt;H:8)A:'6M98E!'5!+!"*S:7:F=G6O9W5A='^T;82J&lt;WY!!".!#A!.&gt;(*J:W&gt;F=C"M:8:F&lt;!!21!I!#GBZ=X2F=G6T;8-!!!^!#A!*&lt;'^X)'RF&gt;G6M!"&amp;!#A!+;'FH;#"M:8:F&lt;!!!1!$R!!!!!!!!!!%=&lt;GF49W^Q:3"U=GFH:W6S)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!9!%(2S;7&gt;H:8)A9W^V='RJ&lt;G=!!%5!]1!!!!!!!!!"(WZJ5W.P='5A&gt;(*J:W&gt;F=C"X;7ZE&lt;X=A&lt;7^E:3ZD&gt;'Q!(5!$!".U=GFH:W6S)(&gt;J&lt;G2P&gt;S"N&lt;W2F!$E!]1!!!!!!!!!"'7ZJ5W.P='5A&gt;(*J:W&gt;F=C"T&lt;'^Q:3ZD&gt;'Q!&amp;U!$!!VU=GFH:W6S)(.M&lt;X"F!"B!-0````]/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!!Q!1&gt;(*J:W&gt;F=C"N&lt;W2J:GFF=A!!&amp;E!Q`````QRD;'&amp;O&lt;G6M)'ZB&lt;75!!%*!5!!8!!!!!1!#!!-!"!!&amp;!!9!"Q!)!!E!#A!,!!Q!$1!/!!]!%!!2!")!%Q!5!"5!&amp;AR49W^Q:3"4;7ZH&lt;'5!!!%!&amp;Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope6" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi0\AcqLib\PXI0ScopeData6</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;03Q%!!"!!A!!!!!!0!"F!#A!397.U&gt;7&amp;M)(.B&lt;8"M:3"S982F!!!51#%/986U&lt;S"U=GFH:W6S:71!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!":!5!!$!!)!!Q!%#76S=G^S)'^V&gt;!!81!I!%(*F&lt;'&amp;U;8:F37ZJ&gt;'FB&lt;&amp;A!!"&amp;!#A!+?%FO9X*F&lt;76O&gt;!!!"1!+!!!11%!!!@````]!#!.X:GU!$!"1!!-!"A!(!!E!(%"!!!(`````!!I/6W&amp;W:7:P=GUA2X*B='A!!"N!!Q!597.U&gt;7&amp;M)(*F9W^S:#"M:7ZH&gt;'A!!!V!#!!'&gt;'FN:49U!!!C1&amp;!!"A!!!!%!"1!,!!Q!$1Z49W^Q:3"3:7&amp;E9G&amp;D;Q!!!1!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope6Settings" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi0\AcqLib\PXI0ScopeControl6</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">4</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">niScope trigger coupling.ctl</Property>
		<Property Name="typedefName2" Type="Str">niScope trigger slope.ctl</Property>
		<Property Name="typedefName3" Type="Str">niScope trigger window mode.ctl</Property>
		<Property Name="typedefName4" Type="Str">niScope vertical coupling.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"!!A!!!!!!9!":!)2"F&lt;G:P=G.F)(*F97RU;7VF!!!61!I!$H:F=H2J9W&amp;M)(*B&lt;G&gt;F!!!61!I!$WVJ&lt;C"T97VQ&lt;'5A=G&amp;U:1!81!-!%7VJ&lt;C"S:7.P=G1A&lt;'6O:X2I!"6!#A!0&gt;G6S&gt;'FD97QA&lt;W:G=W6U!"&gt;!#A!2=(*P9G5A982U:7ZV982J&lt;WY!&amp;5!(!!^J&lt;H"V&gt;#"J&lt;8"F:'&amp;O9W5!(5!+!"&gt;N98BJ&lt;86N)'FO=(6U)':S:8&amp;V:7ZD?1""!0%!!!!!!!!!!2VO;6.D&lt;X"F)(:F=H2J9W&amp;M)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!-!%8:F=H2J9W&amp;M)'.P&gt;8"M;7ZH!""!)1J&amp;&lt;G&amp;C&lt;'5A6%F4!!!`1"9!"12&amp;:'&gt;F#EBZ=X2F=G6T;8-(2'FH;82B&lt;!:8;7ZE&lt;X=*37VN:72J982F!!!-&gt;(*J:W&gt;F=C"U?8"F!!!41!I!$82S;7&gt;H:8)A:'6M98E!'5!+!"*S:7:F=G6O9W5A='^T;82J&lt;WY!!".!#A!.&gt;(*J:W&gt;F=C"M:8:F&lt;!!21!I!#GBZ=X2F=G6T;8-!!!^!#A!*&lt;'^X)'RF&gt;G6M!"&amp;!#A!+;'FH;#"M:8:F&lt;!!!1!$R!!!!!!!!!!%=&lt;GF49W^Q:3"U=GFH:W6S)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!9!%(2S;7&gt;H:8)A9W^V='RJ&lt;G=!!%5!]1!!!!!!!!!"(WZJ5W.P='5A&gt;(*J:W&gt;F=C"X;7ZE&lt;X=A&lt;7^E:3ZD&gt;'Q!(5!$!".U=GFH:W6S)(&gt;J&lt;G2P&gt;S"N&lt;W2F!$E!]1!!!!!!!!!"'7ZJ5W.P='5A&gt;(*J:W&gt;F=C"T&lt;'^Q:3ZD&gt;'Q!&amp;U!$!!VU=GFH:W6S)(.M&lt;X"F!"B!-0````]/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!!Q!1&gt;(*J:W&gt;F=C"N&lt;W2J:GFF=A!!&amp;E!Q`````QRD;'&amp;O&lt;G6M)'ZB&lt;75!!%*!5!!8!!!!!1!#!!-!"!!&amp;!!9!"Q!)!!E!#A!,!!Q!$1!/!!]!%!!2!")!%Q!5!"5!&amp;AR49W^Q:3"4;7ZH&lt;'5!!!%!&amp;Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope9" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi0\AcqLib\PXI0ScopeData9</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;03Q%!!"!!A!!!!!!0!"F!#A!397.U&gt;7&amp;M)(.B&lt;8"M:3"S982F!!!51#%/986U&lt;S"U=GFH:W6S:71!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!":!5!!$!!)!!Q!%#76S=G^S)'^V&gt;!!81!I!%(*F&lt;'&amp;U;8:F37ZJ&gt;'FB&lt;&amp;A!!"&amp;!#A!+?%FO9X*F&lt;76O&gt;!!!"1!+!!!11%!!!@````]!#!.X:GU!$!"1!!-!"A!(!!E!(%"!!!(`````!!I/6W&amp;W:7:P=GUA2X*B='A!!"N!!Q!597.U&gt;7&amp;M)(*F9W^S:#"M:7ZH&gt;'A!!!V!#!!'&gt;'FN:49U!!!C1&amp;!!"A!!!!%!"1!,!!Q!$1Z49W^Q:3"3:7&amp;E9G&amp;D;Q!!!1!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope9Settings" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi0\AcqLib\PXI0ScopeControl9</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">4</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">niScope trigger coupling.ctl</Property>
		<Property Name="typedefName2" Type="Str">niScope trigger slope.ctl</Property>
		<Property Name="typedefName3" Type="Str">niScope trigger window mode.ctl</Property>
		<Property Name="typedefName4" Type="Str">niScope vertical coupling.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"!!A!!!!!!9!":!)2"F&lt;G:P=G.F)(*F97RU;7VF!!!61!I!$H:F=H2J9W&amp;M)(*B&lt;G&gt;F!!!61!I!$WVJ&lt;C"T97VQ&lt;'5A=G&amp;U:1!81!-!%7VJ&lt;C"S:7.P=G1A&lt;'6O:X2I!"6!#A!0&gt;G6S&gt;'FD97QA&lt;W:G=W6U!"&gt;!#A!2=(*P9G5A982U:7ZV982J&lt;WY!&amp;5!(!!^J&lt;H"V&gt;#"J&lt;8"F:'&amp;O9W5!(5!+!"&gt;N98BJ&lt;86N)'FO=(6U)':S:8&amp;V:7ZD?1""!0%!!!!!!!!!!2VO;6.D&lt;X"F)(:F=H2J9W&amp;M)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!-!%8:F=H2J9W&amp;M)'.P&gt;8"M;7ZH!""!)1J&amp;&lt;G&amp;C&lt;'5A6%F4!!!`1"9!"12&amp;:'&gt;F#EBZ=X2F=G6T;8-(2'FH;82B&lt;!:8;7ZE&lt;X=*37VN:72J982F!!!-&gt;(*J:W&gt;F=C"U?8"F!!!41!I!$82S;7&gt;H:8)A:'6M98E!'5!+!"*S:7:F=G6O9W5A='^T;82J&lt;WY!!".!#A!.&gt;(*J:W&gt;F=C"M:8:F&lt;!!21!I!#GBZ=X2F=G6T;8-!!!^!#A!*&lt;'^X)'RF&gt;G6M!"&amp;!#A!+;'FH;#"M:8:F&lt;!!!1!$R!!!!!!!!!!%=&lt;GF49W^Q:3"U=GFH:W6S)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!9!%(2S;7&gt;H:8)A9W^V='RJ&lt;G=!!%5!]1!!!!!!!!!"(WZJ5W.P='5A&gt;(*J:W&gt;F=C"X;7ZE&lt;X=A&lt;7^E:3ZD&gt;'Q!(5!$!".U=GFH:W6S)(&gt;J&lt;G2P&gt;S"N&lt;W2F!$E!]1!!!!!!!!!"'7ZJ5W.P='5A&gt;(*J:W&gt;F=C"T&lt;'^Q:3ZD&gt;'Q!&amp;U!$!!VU=GFH:W6S)(.M&lt;X"F!"B!-0````]/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!!Q!1&gt;(*J:W&gt;F=C"N&lt;W2J:GFF=A!!&amp;E!Q`````QRD;'&amp;O&lt;G6M)'ZB&lt;75!!%*!5!!8!!!!!1!#!!-!"!!&amp;!!9!"Q!)!!E!#A!,!!Q!$1!/!!]!%!!2!")!%Q!5!"5!&amp;AR49W^Q:3"4;7ZH&lt;'5!!!%!&amp;Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope10" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi0\AcqLib\PXI0ScopeData10</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;03Q%!!"!!A!!!!!!0!"F!#A!397.U&gt;7&amp;M)(.B&lt;8"M:3"S982F!!!51#%/986U&lt;S"U=GFH:W6S:71!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!":!5!!$!!)!!Q!%#76S=G^S)'^V&gt;!!81!I!%(*F&lt;'&amp;U;8:F37ZJ&gt;'FB&lt;&amp;A!!"&amp;!#A!+?%FO9X*F&lt;76O&gt;!!!"1!+!!!11%!!!@````]!#!.X:GU!$!"1!!-!"A!(!!E!(%"!!!(`````!!I/6W&amp;W:7:P=GUA2X*B='A!!"N!!Q!597.U&gt;7&amp;M)(*F9W^S:#"M:7ZH&gt;'A!!!V!#!!'&gt;'FN:49U!!!C1&amp;!!"A!!!!%!"1!,!!Q!$1Z49W^Q:3"3:7&amp;E9G&amp;D;Q!!!1!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope10Settings" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi0\AcqLib\PXI0ScopeControl10</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">4</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">niScope trigger coupling.ctl</Property>
		<Property Name="typedefName2" Type="Str">niScope trigger slope.ctl</Property>
		<Property Name="typedefName3" Type="Str">niScope trigger window mode.ctl</Property>
		<Property Name="typedefName4" Type="Str">niScope vertical coupling.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"!!A!!!!!!9!":!)2"F&lt;G:P=G.F)(*F97RU;7VF!!!61!I!$H:F=H2J9W&amp;M)(*B&lt;G&gt;F!!!61!I!$WVJ&lt;C"T97VQ&lt;'5A=G&amp;U:1!81!-!%7VJ&lt;C"S:7.P=G1A&lt;'6O:X2I!"6!#A!0&gt;G6S&gt;'FD97QA&lt;W:G=W6U!"&gt;!#A!2=(*P9G5A982U:7ZV982J&lt;WY!&amp;5!(!!^J&lt;H"V&gt;#"J&lt;8"F:'&amp;O9W5!(5!+!"&gt;N98BJ&lt;86N)'FO=(6U)':S:8&amp;V:7ZD?1""!0%!!!!!!!!!!2VO;6.D&lt;X"F)(:F=H2J9W&amp;M)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!-!%8:F=H2J9W&amp;M)'.P&gt;8"M;7ZH!""!)1J&amp;&lt;G&amp;C&lt;'5A6%F4!!!`1"9!"12&amp;:'&gt;F#EBZ=X2F=G6T;8-(2'FH;82B&lt;!:8;7ZE&lt;X=*37VN:72J982F!!!-&gt;(*J:W&gt;F=C"U?8"F!!!41!I!$82S;7&gt;H:8)A:'6M98E!'5!+!"*S:7:F=G6O9W5A='^T;82J&lt;WY!!".!#A!.&gt;(*J:W&gt;F=C"M:8:F&lt;!!21!I!#GBZ=X2F=G6T;8-!!!^!#A!*&lt;'^X)'RF&gt;G6M!"&amp;!#A!+;'FH;#"M:8:F&lt;!!!1!$R!!!!!!!!!!%=&lt;GF49W^Q:3"U=GFH:W6S)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!9!%(2S;7&gt;H:8)A9W^V='RJ&lt;G=!!%5!]1!!!!!!!!!"(WZJ5W.P='5A&gt;(*J:W&gt;F=C"X;7ZE&lt;X=A&lt;7^E:3ZD&gt;'Q!(5!$!".U=GFH:W6S)(&gt;J&lt;G2P&gt;S"N&lt;W2F!$E!]1!!!!!!!!!"'7ZJ5W.P='5A&gt;(*J:W&gt;F=C"T&lt;'^Q:3ZD&gt;'Q!&amp;U!$!!VU=GFH:W6S)(.M&lt;X"F!"B!-0````]/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!!Q!1&gt;(*J:W&gt;F=C"N&lt;W2J:GFF=A!!&amp;E!Q`````QRD;'&amp;O&lt;G6M)'ZB&lt;75!!%*!5!!8!!!!!1!#!!-!"!!&amp;!!9!"Q!)!!E!#A!,!!Q!$1!/!!]!%!!2!")!%Q!5!"5!&amp;AR49W^Q:3"4;7ZH&lt;'5!!!%!&amp;Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope11" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi0\AcqLib\PXI0ScopeData11</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;03Q%!!"!!A!!!!!!0!"F!#A!397.U&gt;7&amp;M)(.B&lt;8"M:3"S982F!!!51#%/986U&lt;S"U=GFH:W6S:71!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!":!5!!$!!)!!Q!%#76S=G^S)'^V&gt;!!81!I!%(*F&lt;'&amp;U;8:F37ZJ&gt;'FB&lt;&amp;A!!"&amp;!#A!+?%FO9X*F&lt;76O&gt;!!!"1!+!!!11%!!!@````]!#!.X:GU!$!"1!!-!"A!(!!E!(%"!!!(`````!!I/6W&amp;W:7:P=GUA2X*B='A!!"N!!Q!597.U&gt;7&amp;M)(*F9W^S:#"M:7ZH&gt;'A!!!V!#!!'&gt;'FN:49U!!!C1&amp;!!"A!!!!%!"1!,!!Q!$1Z49W^Q:3"3:7&amp;E9G&amp;D;Q!!!1!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope11Settings" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi0\AcqLib\PXI0ScopeControl11</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">4</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">niScope trigger coupling.ctl</Property>
		<Property Name="typedefName2" Type="Str">niScope trigger slope.ctl</Property>
		<Property Name="typedefName3" Type="Str">niScope trigger window mode.ctl</Property>
		<Property Name="typedefName4" Type="Str">niScope vertical coupling.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"!!A!!!!!!9!":!)2"F&lt;G:P=G.F)(*F97RU;7VF!!!61!I!$H:F=H2J9W&amp;M)(*B&lt;G&gt;F!!!61!I!$WVJ&lt;C"T97VQ&lt;'5A=G&amp;U:1!81!-!%7VJ&lt;C"S:7.P=G1A&lt;'6O:X2I!"6!#A!0&gt;G6S&gt;'FD97QA&lt;W:G=W6U!"&gt;!#A!2=(*P9G5A982U:7ZV982J&lt;WY!&amp;5!(!!^J&lt;H"V&gt;#"J&lt;8"F:'&amp;O9W5!(5!+!"&gt;N98BJ&lt;86N)'FO=(6U)':S:8&amp;V:7ZD?1""!0%!!!!!!!!!!2VO;6.D&lt;X"F)(:F=H2J9W&amp;M)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!-!%8:F=H2J9W&amp;M)'.P&gt;8"M;7ZH!""!)1J&amp;&lt;G&amp;C&lt;'5A6%F4!!!`1"9!"12&amp;:'&gt;F#EBZ=X2F=G6T;8-(2'FH;82B&lt;!:8;7ZE&lt;X=*37VN:72J982F!!!-&gt;(*J:W&gt;F=C"U?8"F!!!41!I!$82S;7&gt;H:8)A:'6M98E!'5!+!"*S:7:F=G6O9W5A='^T;82J&lt;WY!!".!#A!.&gt;(*J:W&gt;F=C"M:8:F&lt;!!21!I!#GBZ=X2F=G6T;8-!!!^!#A!*&lt;'^X)'RF&gt;G6M!"&amp;!#A!+;'FH;#"M:8:F&lt;!!!1!$R!!!!!!!!!!%=&lt;GF49W^Q:3"U=GFH:W6S)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!9!%(2S;7&gt;H:8)A9W^V='RJ&lt;G=!!%5!]1!!!!!!!!!"(WZJ5W.P='5A&gt;(*J:W&gt;F=C"X;7ZE&lt;X=A&lt;7^E:3ZD&gt;'Q!(5!$!".U=GFH:W6S)(&gt;J&lt;G2P&gt;S"N&lt;W2F!$E!]1!!!!!!!!!"'7ZJ5W.P='5A&gt;(*J:W&gt;F=C"T&lt;'^Q:3ZD&gt;'Q!&amp;U!$!!VU=GFH:W6S)(.M&lt;X"F!"B!-0````]/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!!Q!1&gt;(*J:W&gt;F=C"N&lt;W2J:GFF=A!!&amp;E!Q`````QRD;'&amp;O&lt;G6M)'ZB&lt;75!!%*!5!!8!!!!!1!#!!-!"!!&amp;!!9!"Q!)!!E!#A!,!!Q!$1!/!!]!%!!2!")!%Q!5!"5!&amp;AR49W^Q:3"4;7ZH&lt;'5!!!%!&amp;Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope12" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi0\AcqLib\PXI0ScopeData12</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;03Q%!!"!!A!!!!!!0!"F!#A!397.U&gt;7&amp;M)(.B&lt;8"M:3"S982F!!!51#%/986U&lt;S"U=GFH:W6S:71!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!":!5!!$!!)!!Q!%#76S=G^S)'^V&gt;!!81!I!%(*F&lt;'&amp;U;8:F37ZJ&gt;'FB&lt;&amp;A!!"&amp;!#A!+?%FO9X*F&lt;76O&gt;!!!"1!+!!!11%!!!@````]!#!.X:GU!$!"1!!-!"A!(!!E!(%"!!!(`````!!I/6W&amp;W:7:P=GUA2X*B='A!!"N!!Q!597.U&gt;7&amp;M)(*F9W^S:#"M:7ZH&gt;'A!!!V!#!!'&gt;'FN:49U!!!C1&amp;!!"A!!!!%!"1!,!!Q!$1Z49W^Q:3"3:7&amp;E9G&amp;D;Q!!!1!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope12Settings" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi0\AcqLib\PXI0ScopeControl12</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">4</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">niScope trigger coupling.ctl</Property>
		<Property Name="typedefName2" Type="Str">niScope trigger slope.ctl</Property>
		<Property Name="typedefName3" Type="Str">niScope trigger window mode.ctl</Property>
		<Property Name="typedefName4" Type="Str">niScope vertical coupling.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"!!A!!!!!!9!":!)2"F&lt;G:P=G.F)(*F97RU;7VF!!!61!I!$H:F=H2J9W&amp;M)(*B&lt;G&gt;F!!!61!I!$WVJ&lt;C"T97VQ&lt;'5A=G&amp;U:1!81!-!%7VJ&lt;C"S:7.P=G1A&lt;'6O:X2I!"6!#A!0&gt;G6S&gt;'FD97QA&lt;W:G=W6U!"&gt;!#A!2=(*P9G5A982U:7ZV982J&lt;WY!&amp;5!(!!^J&lt;H"V&gt;#"J&lt;8"F:'&amp;O9W5!(5!+!"&gt;N98BJ&lt;86N)'FO=(6U)':S:8&amp;V:7ZD?1""!0%!!!!!!!!!!2VO;6.D&lt;X"F)(:F=H2J9W&amp;M)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!-!%8:F=H2J9W&amp;M)'.P&gt;8"M;7ZH!""!)1J&amp;&lt;G&amp;C&lt;'5A6%F4!!!`1"9!"12&amp;:'&gt;F#EBZ=X2F=G6T;8-(2'FH;82B&lt;!:8;7ZE&lt;X=*37VN:72J982F!!!-&gt;(*J:W&gt;F=C"U?8"F!!!41!I!$82S;7&gt;H:8)A:'6M98E!'5!+!"*S:7:F=G6O9W5A='^T;82J&lt;WY!!".!#A!.&gt;(*J:W&gt;F=C"M:8:F&lt;!!21!I!#GBZ=X2F=G6T;8-!!!^!#A!*&lt;'^X)'RF&gt;G6M!"&amp;!#A!+;'FH;#"M:8:F&lt;!!!1!$R!!!!!!!!!!%=&lt;GF49W^Q:3"U=GFH:W6S)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!9!%(2S;7&gt;H:8)A9W^V='RJ&lt;G=!!%5!]1!!!!!!!!!"(WZJ5W.P='5A&gt;(*J:W&gt;F=C"X;7ZE&lt;X=A&lt;7^E:3ZD&gt;'Q!(5!$!".U=GFH:W6S)(&gt;J&lt;G2P&gt;S"N&lt;W2F!$E!]1!!!!!!!!!"'7ZJ5W.P='5A&gt;(*J:W&gt;F=C"T&lt;'^Q:3ZD&gt;'Q!&amp;U!$!!VU=GFH:W6S)(.M&lt;X"F!"B!-0````]/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!!Q!1&gt;(*J:W&gt;F=C"N&lt;W2J:GFF=A!!&amp;E!Q`````QRD;'&amp;O&lt;G6M)'ZB&lt;75!!%*!5!!8!!!!!1!#!!-!"!!&amp;!!9!"Q!)!!E!#A!,!!Q!$1!/!!]!%!!2!")!%Q!5!"5!&amp;AR49W^Q:3"4;7ZH&lt;'5!!!%!&amp;Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope15" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi0\AcqLib\PXI0ScopeData15</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;03Q%!!"!!A!!!!!!0!"F!#A!397.U&gt;7&amp;M)(.B&lt;8"M:3"S982F!!!51#%/986U&lt;S"U=GFH:W6S:71!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!":!5!!$!!)!!Q!%#76S=G^S)'^V&gt;!!81!I!%(*F&lt;'&amp;U;8:F37ZJ&gt;'FB&lt;&amp;A!!"&amp;!#A!+?%FO9X*F&lt;76O&gt;!!!"1!+!!!11%!!!@````]!#!.X:GU!$!"1!!-!"A!(!!E!(%"!!!(`````!!I/6W&amp;W:7:P=GUA2X*B='A!!"N!!Q!597.U&gt;7&amp;M)(*F9W^S:#"M:7ZH&gt;'A!!!V!#!!'&gt;'FN:49U!!!C1&amp;!!"A!!!!%!"1!,!!Q!$1Z49W^Q:3"3:7&amp;E9G&amp;D;Q!!!1!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope15Settings" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi0\AcqLib\PXI0ScopeControl15</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">4</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">niScope trigger coupling.ctl</Property>
		<Property Name="typedefName2" Type="Str">niScope trigger slope.ctl</Property>
		<Property Name="typedefName3" Type="Str">niScope trigger window mode.ctl</Property>
		<Property Name="typedefName4" Type="Str">niScope vertical coupling.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"!!A!!!!!!9!":!)2"F&lt;G:P=G.F)(*F97RU;7VF!!!61!I!$H:F=H2J9W&amp;M)(*B&lt;G&gt;F!!!61!I!$WVJ&lt;C"T97VQ&lt;'5A=G&amp;U:1!81!-!%7VJ&lt;C"S:7.P=G1A&lt;'6O:X2I!"6!#A!0&gt;G6S&gt;'FD97QA&lt;W:G=W6U!"&gt;!#A!2=(*P9G5A982U:7ZV982J&lt;WY!&amp;5!(!!^J&lt;H"V&gt;#"J&lt;8"F:'&amp;O9W5!(5!+!"&gt;N98BJ&lt;86N)'FO=(6U)':S:8&amp;V:7ZD?1""!0%!!!!!!!!!!2VO;6.D&lt;X"F)(:F=H2J9W&amp;M)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!-!%8:F=H2J9W&amp;M)'.P&gt;8"M;7ZH!""!)1J&amp;&lt;G&amp;C&lt;'5A6%F4!!!`1"9!"12&amp;:'&gt;F#EBZ=X2F=G6T;8-(2'FH;82B&lt;!:8;7ZE&lt;X=*37VN:72J982F!!!-&gt;(*J:W&gt;F=C"U?8"F!!!41!I!$82S;7&gt;H:8)A:'6M98E!'5!+!"*S:7:F=G6O9W5A='^T;82J&lt;WY!!".!#A!.&gt;(*J:W&gt;F=C"M:8:F&lt;!!21!I!#GBZ=X2F=G6T;8-!!!^!#A!*&lt;'^X)'RF&gt;G6M!"&amp;!#A!+;'FH;#"M:8:F&lt;!!!1!$R!!!!!!!!!!%=&lt;GF49W^Q:3"U=GFH:W6S)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!9!%(2S;7&gt;H:8)A9W^V='RJ&lt;G=!!%5!]1!!!!!!!!!"(WZJ5W.P='5A&gt;(*J:W&gt;F=C"X;7ZE&lt;X=A&lt;7^E:3ZD&gt;'Q!(5!$!".U=GFH:W6S)(&gt;J&lt;G2P&gt;S"N&lt;W2F!$E!]1!!!!!!!!!"'7ZJ5W.P='5A&gt;(*J:W&gt;F=C"T&lt;'^Q:3ZD&gt;'Q!&amp;U!$!!VU=GFH:W6S)(.M&lt;X"F!"B!-0````]/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!!Q!1&gt;(*J:W&gt;F=C"N&lt;W2J:GFF=A!!&amp;E!Q`````QRD;'&amp;O&lt;G6M)'ZB&lt;75!!%*!5!!8!!!!!1!#!!-!"!!&amp;!!9!"Q!)!!E!#A!,!!Q!$1!/!!]!%!!2!")!%Q!5!"5!&amp;AR49W^Q:3"4;7ZH&lt;'5!!!%!&amp;Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope16" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi0\AcqLib\PXI0ScopeData16</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;03Q%!!"!!A!!!!!!0!"F!#A!397.U&gt;7&amp;M)(.B&lt;8"M:3"S982F!!!51#%/986U&lt;S"U=GFH:W6S:71!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!":!5!!$!!)!!Q!%#76S=G^S)'^V&gt;!!81!I!%(*F&lt;'&amp;U;8:F37ZJ&gt;'FB&lt;&amp;A!!"&amp;!#A!+?%FO9X*F&lt;76O&gt;!!!"1!+!!!11%!!!@````]!#!.X:GU!$!"1!!-!"A!(!!E!(%"!!!(`````!!I/6W&amp;W:7:P=GUA2X*B='A!!"N!!Q!597.U&gt;7&amp;M)(*F9W^S:#"M:7ZH&gt;'A!!!V!#!!'&gt;'FN:49U!!!C1&amp;!!"A!!!!%!"1!,!!Q!$1Z49W^Q:3"3:7&amp;E9G&amp;D;Q!!!1!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope16Settings" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi0\AcqLib\PXI0ScopeControl16</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">4</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">niScope trigger coupling.ctl</Property>
		<Property Name="typedefName2" Type="Str">niScope trigger slope.ctl</Property>
		<Property Name="typedefName3" Type="Str">niScope trigger window mode.ctl</Property>
		<Property Name="typedefName4" Type="Str">niScope vertical coupling.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"!!A!!!!!!9!":!)2"F&lt;G:P=G.F)(*F97RU;7VF!!!61!I!$H:F=H2J9W&amp;M)(*B&lt;G&gt;F!!!61!I!$WVJ&lt;C"T97VQ&lt;'5A=G&amp;U:1!81!-!%7VJ&lt;C"S:7.P=G1A&lt;'6O:X2I!"6!#A!0&gt;G6S&gt;'FD97QA&lt;W:G=W6U!"&gt;!#A!2=(*P9G5A982U:7ZV982J&lt;WY!&amp;5!(!!^J&lt;H"V&gt;#"J&lt;8"F:'&amp;O9W5!(5!+!"&gt;N98BJ&lt;86N)'FO=(6U)':S:8&amp;V:7ZD?1""!0%!!!!!!!!!!2VO;6.D&lt;X"F)(:F=H2J9W&amp;M)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!-!%8:F=H2J9W&amp;M)'.P&gt;8"M;7ZH!""!)1J&amp;&lt;G&amp;C&lt;'5A6%F4!!!`1"9!"12&amp;:'&gt;F#EBZ=X2F=G6T;8-(2'FH;82B&lt;!:8;7ZE&lt;X=*37VN:72J982F!!!-&gt;(*J:W&gt;F=C"U?8"F!!!41!I!$82S;7&gt;H:8)A:'6M98E!'5!+!"*S:7:F=G6O9W5A='^T;82J&lt;WY!!".!#A!.&gt;(*J:W&gt;F=C"M:8:F&lt;!!21!I!#GBZ=X2F=G6T;8-!!!^!#A!*&lt;'^X)'RF&gt;G6M!"&amp;!#A!+;'FH;#"M:8:F&lt;!!!1!$R!!!!!!!!!!%=&lt;GF49W^Q:3"U=GFH:W6S)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!9!%(2S;7&gt;H:8)A9W^V='RJ&lt;G=!!%5!]1!!!!!!!!!"(WZJ5W.P='5A&gt;(*J:W&gt;F=C"X;7ZE&lt;X=A&lt;7^E:3ZD&gt;'Q!(5!$!".U=GFH:W6S)(&gt;J&lt;G2P&gt;S"N&lt;W2F!$E!]1!!!!!!!!!"'7ZJ5W.P='5A&gt;(*J:W&gt;F=C"T&lt;'^Q:3ZD&gt;'Q!&amp;U!$!!VU=GFH:W6S)(.M&lt;X"F!"B!-0````]/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!!Q!1&gt;(*J:W&gt;F=C"N&lt;W2J:GFF=A!!&amp;E!Q`````QRD;'&amp;O&lt;G6M)'ZB&lt;75!!%*!5!!8!!!!!1!#!!-!"!!&amp;!!9!"Q!)!!E!#A!,!!Q!$1!/!!]!%!!2!")!%Q!5!"5!&amp;AR49W^Q:3"4;7ZH&lt;'5!!!%!&amp;Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope17" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi0\AcqLib\PXI0ScopeData17</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;03Q%!!"!!A!!!!!!0!"F!#A!397.U&gt;7&amp;M)(.B&lt;8"M:3"S982F!!!51#%/986U&lt;S"U=GFH:W6S:71!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!":!5!!$!!)!!Q!%#76S=G^S)'^V&gt;!!81!I!%(*F&lt;'&amp;U;8:F37ZJ&gt;'FB&lt;&amp;A!!"&amp;!#A!+?%FO9X*F&lt;76O&gt;!!!"1!+!!!11%!!!@````]!#!.X:GU!$!"1!!-!"A!(!!E!(%"!!!(`````!!I/6W&amp;W:7:P=GUA2X*B='A!!"N!!Q!597.U&gt;7&amp;M)(*F9W^S:#"M:7ZH&gt;'A!!!V!#!!'&gt;'FN:49U!!!C1&amp;!!"A!!!!%!"1!,!!Q!$1Z49W^Q:3"3:7&amp;E9G&amp;D;Q!!!1!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope17Settings" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi0\AcqLib\PXI0ScopeControl17</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">4</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">niScope trigger coupling.ctl</Property>
		<Property Name="typedefName2" Type="Str">niScope trigger slope.ctl</Property>
		<Property Name="typedefName3" Type="Str">niScope trigger window mode.ctl</Property>
		<Property Name="typedefName4" Type="Str">niScope vertical coupling.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"!!A!!!!!!9!":!)2"F&lt;G:P=G.F)(*F97RU;7VF!!!61!I!$H:F=H2J9W&amp;M)(*B&lt;G&gt;F!!!61!I!$WVJ&lt;C"T97VQ&lt;'5A=G&amp;U:1!81!-!%7VJ&lt;C"S:7.P=G1A&lt;'6O:X2I!"6!#A!0&gt;G6S&gt;'FD97QA&lt;W:G=W6U!"&gt;!#A!2=(*P9G5A982U:7ZV982J&lt;WY!&amp;5!(!!^J&lt;H"V&gt;#"J&lt;8"F:'&amp;O9W5!(5!+!"&gt;N98BJ&lt;86N)'FO=(6U)':S:8&amp;V:7ZD?1""!0%!!!!!!!!!!2VO;6.D&lt;X"F)(:F=H2J9W&amp;M)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!-!%8:F=H2J9W&amp;M)'.P&gt;8"M;7ZH!""!)1J&amp;&lt;G&amp;C&lt;'5A6%F4!!!`1"9!"12&amp;:'&gt;F#EBZ=X2F=G6T;8-(2'FH;82B&lt;!:8;7ZE&lt;X=*37VN:72J982F!!!-&gt;(*J:W&gt;F=C"U?8"F!!!41!I!$82S;7&gt;H:8)A:'6M98E!'5!+!"*S:7:F=G6O9W5A='^T;82J&lt;WY!!".!#A!.&gt;(*J:W&gt;F=C"M:8:F&lt;!!21!I!#GBZ=X2F=G6T;8-!!!^!#A!*&lt;'^X)'RF&gt;G6M!"&amp;!#A!+;'FH;#"M:8:F&lt;!!!1!$R!!!!!!!!!!%=&lt;GF49W^Q:3"U=GFH:W6S)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!9!%(2S;7&gt;H:8)A9W^V='RJ&lt;G=!!%5!]1!!!!!!!!!"(WZJ5W.P='5A&gt;(*J:W&gt;F=C"X;7ZE&lt;X=A&lt;7^E:3ZD&gt;'Q!(5!$!".U=GFH:W6S)(&gt;J&lt;G2P&gt;S"N&lt;W2F!$E!]1!!!!!!!!!"'7ZJ5W.P='5A&gt;(*J:W&gt;F=C"T&lt;'^Q:3ZD&gt;'Q!&amp;U!$!!VU=GFH:W6S)(.M&lt;X"F!"B!-0````]/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!!Q!1&gt;(*J:W&gt;F=C"N&lt;W2J:GFF=A!!&amp;E!Q`````QRD;'&amp;O&lt;G6M)'ZB&lt;75!!%*!5!!8!!!!!1!#!!-!"!!&amp;!!9!"Q!)!!E!#A!,!!Q!$1!/!!]!%!!2!")!%Q!5!"5!&amp;AR49W^Q:3"4;7ZH&lt;'5!!!%!&amp;Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope18" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi0\AcqLib\PXI0ScopeData18</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;03Q%!!"!!A!!!!!!0!"F!#A!397.U&gt;7&amp;M)(.B&lt;8"M:3"S982F!!!51#%/986U&lt;S"U=GFH:W6S:71!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!":!5!!$!!)!!Q!%#76S=G^S)'^V&gt;!!81!I!%(*F&lt;'&amp;U;8:F37ZJ&gt;'FB&lt;&amp;A!!"&amp;!#A!+?%FO9X*F&lt;76O&gt;!!!"1!+!!!11%!!!@````]!#!.X:GU!$!"1!!-!"A!(!!E!(%"!!!(`````!!I/6W&amp;W:7:P=GUA2X*B='A!!"N!!Q!597.U&gt;7&amp;M)(*F9W^S:#"M:7ZH&gt;'A!!!V!#!!'&gt;'FN:49U!!!C1&amp;!!"A!!!!%!"1!,!!Q!$1Z49W^Q:3"3:7&amp;E9G&amp;D;Q!!!1!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Scope18Settings" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi0\AcqLib\PXI0ScopeControl18</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">4</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">niScope trigger coupling.ctl</Property>
		<Property Name="typedefName2" Type="Str">niScope trigger slope.ctl</Property>
		<Property Name="typedefName3" Type="Str">niScope trigger window mode.ctl</Property>
		<Property Name="typedefName4" Type="Str">niScope vertical coupling.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"!!A!!!!!!9!":!)2"F&lt;G:P=G.F)(*F97RU;7VF!!!61!I!$H:F=H2J9W&amp;M)(*B&lt;G&gt;F!!!61!I!$WVJ&lt;C"T97VQ&lt;'5A=G&amp;U:1!81!-!%7VJ&lt;C"S:7.P=G1A&lt;'6O:X2I!"6!#A!0&gt;G6S&gt;'FD97QA&lt;W:G=W6U!"&gt;!#A!2=(*P9G5A982U:7ZV982J&lt;WY!&amp;5!(!!^J&lt;H"V&gt;#"J&lt;8"F:'&amp;O9W5!(5!+!"&gt;N98BJ&lt;86N)'FO=(6U)':S:8&amp;V:7ZD?1""!0%!!!!!!!!!!2VO;6.D&lt;X"F)(:F=H2J9W&amp;M)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!-!%8:F=H2J9W&amp;M)'.P&gt;8"M;7ZH!""!)1J&amp;&lt;G&amp;C&lt;'5A6%F4!!!`1"9!"12&amp;:'&gt;F#EBZ=X2F=G6T;8-(2'FH;82B&lt;!:8;7ZE&lt;X=*37VN:72J982F!!!-&gt;(*J:W&gt;F=C"U?8"F!!!41!I!$82S;7&gt;H:8)A:'6M98E!'5!+!"*S:7:F=G6O9W5A='^T;82J&lt;WY!!".!#A!.&gt;(*J:W&gt;F=C"M:8:F&lt;!!21!I!#GBZ=X2F=G6T;8-!!!^!#A!*&lt;'^X)'RF&gt;G6M!"&amp;!#A!+;'FH;#"M:8:F&lt;!!!1!$R!!!!!!!!!!%=&lt;GF49W^Q:3"U=GFH:W6S)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!9!%(2S;7&gt;H:8)A9W^V='RJ&lt;G=!!%5!]1!!!!!!!!!"(WZJ5W.P='5A&gt;(*J:W&gt;F=C"X;7ZE&lt;X=A&lt;7^E:3ZD&gt;'Q!(5!$!".U=GFH:W6S)(&gt;J&lt;G2P&gt;S"N&lt;W2F!$E!]1!!!!!!!!!"'7ZJ5W.P='5A&gt;(*J:W&gt;F=C"T&lt;'^Q:3ZD&gt;'Q!&amp;U!$!!VU=GFH:W6S)(.M&lt;X"F!"B!-0````]/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!!Q!1&gt;(*J:W&gt;F=C"N&lt;W2J:GFF=A!!&amp;E!Q`````QRD;'&amp;O&lt;G6M)'ZB&lt;75!!%*!5!!8!!!!!1!#!!-!"!!&amp;!!9!"Q!)!!E!#A!,!!Q!$1!/!!]!%!!2!")!%Q!5!"5!&amp;AR49W^Q:3"4;7ZH&lt;'5!!!%!&amp;Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Time" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-s0\GTiming\PXI0Timing</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">2</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">GFTy 9404.lvlib:GFT9404_Auxiliary Delay Channels.ctl</Property>
		<Property Name="typedefName2" Type="Str">GFTy 9404.lvlib:GFT9404_Precision Delay Channels.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/GreenfieldTechnology GFT9404/_Typedefs/GFT9404_Auxiliary Delay Channels.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/GreenfieldTechnology GFT9404/_Typedefs/GFT9404_Precision Delay Channels.ctl</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"!!A!!!!!!@!"B!-0````]0=W^V=G.F)(2F=GVJ&lt;G&amp;M!"2!1!!"`````Q!!"F.P&gt;8*D:1!!(E!Q`````R2E:8.U;7ZB&gt;'FP&lt;C"U:8*N;7ZB&lt;!!!'%"!!!(`````!!),2'6T&gt;'FO982J&lt;WY!&amp;E"1!!)!!1!$#U.P&lt;GZF9X2J&lt;WZT!!1!)1!-1#%'68"E982F!!!/1#%)47&amp;O)&amp;2S;7=!!#F!&amp;A!##%^O:3"4;'^U#F*F='6U;82J&gt;G5!$(2S;7&gt;H:8)A&lt;7^E:1!!-5!7!!-)37ZU:8*O97Q(6&amp;**2S"*4AB17%EA5V2"5A!/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!#A!2&gt;(*J:W&gt;F=C"M:8:F&lt;#!I6CE!*U!7!!))5'^T;82J&gt;G5)4G6H982J&gt;G5!$82S;7&gt;H:8)A=WRP='5!(5!+!":U=GFH:W6S)':S:8&amp;V:7ZD?3!I3(IJ!!!91&amp;!!"1!)!!E!#A!,!!Q(6(*J:W&gt;F=A"5!0%!!!!!!!!!!A^(2F2Z)$EU-$1O&lt;(:M;7)E2U:5/41Q.&amp;^1=G6D;8.J&lt;WYA2'6M98EA1WBB&lt;GZF&lt;(-O9X2M!"&gt;!!Q!.:'6M98EA9WBB&lt;GZF&lt;!!01!I!#72F&lt;'&amp;Z)#BT+1!41!I!$7&amp;N='RJ&gt;(6E:3!I6CE!%5!+!!JX;72U;#!I&lt;H-J!!!31#%.&lt;X6U=(6U)'6O97*M:1!?1&amp;!!"1!/!!]!%!!2!").2'6M98EA1WBB&lt;GZF&lt;!!71%!!!@````]!%QB$;'&amp;O&lt;G6M=Q!!6!$R!!!!!!!!!!)02U:5?3!Z.$!U,GRW&lt;'FC*%&gt;'6$EU-$2@186Y;7RJ98*Z)%2F&lt;'&amp;Z)%.I97ZO:7RT,G.U&lt;!!81!-!$72F&lt;'&amp;Z)'.I97ZO:7Q!&amp;%!B$G.P&lt;GZF9X1A&gt;']A5&amp;B*!!!71&amp;!!"!!6!!]!%A!7"U.M&gt;8.U:8)!'E"!!!(`````!"=-186Y)%.I97ZO:7RT!!!F1"9!!AB3:7RB&gt;'FW:1B"9H.P&lt;(6U:1!+:'6M98EA&gt;(FQ:1!!&amp;%!B$EVB&lt;H6B&lt;#"5=GFH:W6S!!!?1&amp;!!"1!.!"1!'!!:!"I-1G^B=G1A1W^O:GFH!!!11&amp;!!!1!&lt;"U&gt;'6$EU-$1!(%"!!!(`````!"Q06(*J:W&gt;F=C".&lt;W2V&lt;'6T!"Z!5!!&amp;!!1!"1!'!!=!(1R$=G&amp;U:3"5;7VJ&lt;G=!!!%!(A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Scope9" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi1\AcqLib\PXI1ScopeData9</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;03Q%!!"!!A!!!!!!0!"F!#A!397.U&gt;7&amp;M)(.B&lt;8"M:3"S982F!!!51#%/986U&lt;S"U=GFH:W6S:71!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!":!5!!$!!)!!Q!%#76S=G^S)'^V&gt;!!81!I!%(*F&lt;'&amp;U;8:F37ZJ&gt;'FB&lt;&amp;A!!"&amp;!#A!+?%FO9X*F&lt;76O&gt;!!!"1!+!!!11%!!!@````]!#!.X:GU!$!"1!!-!"A!(!!E!(%"!!!(`````!!I/6W&amp;W:7:P=GUA2X*B='A!!"N!!Q!597.U&gt;7&amp;M)(*F9W^S:#"M:7ZH&gt;'A!!!V!#!!'&gt;'FN:49U!!!C1&amp;!!"A!!!!%!"1!,!!Q!$1Z49W^Q:3"3:7&amp;E9G&amp;D;Q!!!1!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Scope9Settings" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi1\AcqLib\PXI1ScopeControl9</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">4</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">niScope trigger coupling.ctl</Property>
		<Property Name="typedefName2" Type="Str">niScope trigger slope.ctl</Property>
		<Property Name="typedefName3" Type="Str">niScope trigger window mode.ctl</Property>
		<Property Name="typedefName4" Type="Str">niScope vertical coupling.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"!!A!!!!!!9!":!)2"F&lt;G:P=G.F)(*F97RU;7VF!!!61!I!$H:F=H2J9W&amp;M)(*B&lt;G&gt;F!!!61!I!$WVJ&lt;C"T97VQ&lt;'5A=G&amp;U:1!81!-!%7VJ&lt;C"S:7.P=G1A&lt;'6O:X2I!"6!#A!0&gt;G6S&gt;'FD97QA&lt;W:G=W6U!"&gt;!#A!2=(*P9G5A982U:7ZV982J&lt;WY!&amp;5!(!!^J&lt;H"V&gt;#"J&lt;8"F:'&amp;O9W5!(5!+!"&gt;N98BJ&lt;86N)'FO=(6U)':S:8&amp;V:7ZD?1""!0%!!!!!!!!!!2VO;6.D&lt;X"F)(:F=H2J9W&amp;M)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!-!%8:F=H2J9W&amp;M)'.P&gt;8"M;7ZH!""!)1J&amp;&lt;G&amp;C&lt;'5A6%F4!!!`1"9!"12&amp;:'&gt;F#EBZ=X2F=G6T;8-(2'FH;82B&lt;!:8;7ZE&lt;X=*37VN:72J982F!!!-&gt;(*J:W&gt;F=C"U?8"F!!!41!I!$82S;7&gt;H:8)A:'6M98E!'5!+!"*S:7:F=G6O9W5A='^T;82J&lt;WY!!".!#A!.&gt;(*J:W&gt;F=C"M:8:F&lt;!!21!I!#GBZ=X2F=G6T;8-!!!^!#A!*&lt;'^X)'RF&gt;G6M!"&amp;!#A!+;'FH;#"M:8:F&lt;!!!1!$R!!!!!!!!!!%=&lt;GF49W^Q:3"U=GFH:W6S)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!9!%(2S;7&gt;H:8)A9W^V='RJ&lt;G=!!%5!]1!!!!!!!!!"(WZJ5W.P='5A&gt;(*J:W&gt;F=C"X;7ZE&lt;X=A&lt;7^E:3ZD&gt;'Q!(5!$!".U=GFH:W6S)(&gt;J&lt;G2P&gt;S"N&lt;W2F!$E!]1!!!!!!!!!"'7ZJ5W.P='5A&gt;(*J:W&gt;F=C"T&lt;'^Q:3ZD&gt;'Q!&amp;U!$!!VU=GFH:W6S)(.M&lt;X"F!"B!-0````]/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!!Q!1&gt;(*J:W&gt;F=C"N&lt;W2J:GFF=A!!&amp;E!Q`````QRD;'&amp;O&lt;G6M)'ZB&lt;75!!%*!5!!8!!!!!1!#!!-!"!!&amp;!!9!"Q!)!!E!#A!,!!Q!$1!/!!]!%!!2!")!%Q!5!"5!&amp;AR49W^Q:3"4;7ZH&lt;'5!!!%!&amp;Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Scope10" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi1\AcqLib\PXI1ScopeData10</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;03Q%!!"!!A!!!!!!0!"F!#A!397.U&gt;7&amp;M)(.B&lt;8"M:3"S982F!!!51#%/986U&lt;S"U=GFH:W6S:71!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!":!5!!$!!)!!Q!%#76S=G^S)'^V&gt;!!81!I!%(*F&lt;'&amp;U;8:F37ZJ&gt;'FB&lt;&amp;A!!"&amp;!#A!+?%FO9X*F&lt;76O&gt;!!!"1!+!!!11%!!!@````]!#!.X:GU!$!"1!!-!"A!(!!E!(%"!!!(`````!!I/6W&amp;W:7:P=GUA2X*B='A!!"N!!Q!597.U&gt;7&amp;M)(*F9W^S:#"M:7ZH&gt;'A!!!V!#!!'&gt;'FN:49U!!!C1&amp;!!"A!!!!%!"1!,!!Q!$1Z49W^Q:3"3:7&amp;E9G&amp;D;Q!!!1!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Scope10Settings" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi1\AcqLib\PXI1ScopeControl10</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">4</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">niScope trigger coupling.ctl</Property>
		<Property Name="typedefName2" Type="Str">niScope trigger slope.ctl</Property>
		<Property Name="typedefName3" Type="Str">niScope trigger window mode.ctl</Property>
		<Property Name="typedefName4" Type="Str">niScope vertical coupling.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"!!A!!!!!!9!":!)2"F&lt;G:P=G.F)(*F97RU;7VF!!!61!I!$H:F=H2J9W&amp;M)(*B&lt;G&gt;F!!!61!I!$WVJ&lt;C"T97VQ&lt;'5A=G&amp;U:1!81!-!%7VJ&lt;C"S:7.P=G1A&lt;'6O:X2I!"6!#A!0&gt;G6S&gt;'FD97QA&lt;W:G=W6U!"&gt;!#A!2=(*P9G5A982U:7ZV982J&lt;WY!&amp;5!(!!^J&lt;H"V&gt;#"J&lt;8"F:'&amp;O9W5!(5!+!"&gt;N98BJ&lt;86N)'FO=(6U)':S:8&amp;V:7ZD?1""!0%!!!!!!!!!!2VO;6.D&lt;X"F)(:F=H2J9W&amp;M)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!-!%8:F=H2J9W&amp;M)'.P&gt;8"M;7ZH!""!)1J&amp;&lt;G&amp;C&lt;'5A6%F4!!!`1"9!"12&amp;:'&gt;F#EBZ=X2F=G6T;8-(2'FH;82B&lt;!:8;7ZE&lt;X=*37VN:72J982F!!!-&gt;(*J:W&gt;F=C"U?8"F!!!41!I!$82S;7&gt;H:8)A:'6M98E!'5!+!"*S:7:F=G6O9W5A='^T;82J&lt;WY!!".!#A!.&gt;(*J:W&gt;F=C"M:8:F&lt;!!21!I!#GBZ=X2F=G6T;8-!!!^!#A!*&lt;'^X)'RF&gt;G6M!"&amp;!#A!+;'FH;#"M:8:F&lt;!!!1!$R!!!!!!!!!!%=&lt;GF49W^Q:3"U=GFH:W6S)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!9!%(2S;7&gt;H:8)A9W^V='RJ&lt;G=!!%5!]1!!!!!!!!!"(WZJ5W.P='5A&gt;(*J:W&gt;F=C"X;7ZE&lt;X=A&lt;7^E:3ZD&gt;'Q!(5!$!".U=GFH:W6S)(&gt;J&lt;G2P&gt;S"N&lt;W2F!$E!]1!!!!!!!!!"'7ZJ5W.P='5A&gt;(*J:W&gt;F=C"T&lt;'^Q:3ZD&gt;'Q!&amp;U!$!!VU=GFH:W6S)(.M&lt;X"F!"B!-0````]/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!!Q!1&gt;(*J:W&gt;F=C"N&lt;W2J:GFF=A!!&amp;E!Q`````QRD;'&amp;O&lt;G6M)'ZB&lt;75!!%*!5!!8!!!!!1!#!!-!"!!&amp;!!9!"Q!)!!E!#A!,!!Q!$1!/!!]!%!!2!")!%Q!5!"5!&amp;AR49W^Q:3"4;7ZH&lt;'5!!!%!&amp;Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Scope11" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi1\AcqLib\PXI1ScopeData11</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;03Q%!!"!!A!!!!!!0!"F!#A!397.U&gt;7&amp;M)(.B&lt;8"M:3"S982F!!!51#%/986U&lt;S"U=GFH:W6S:71!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!":!5!!$!!)!!Q!%#76S=G^S)'^V&gt;!!81!I!%(*F&lt;'&amp;U;8:F37ZJ&gt;'FB&lt;&amp;A!!"&amp;!#A!+?%FO9X*F&lt;76O&gt;!!!"1!+!!!11%!!!@````]!#!.X:GU!$!"1!!-!"A!(!!E!(%"!!!(`````!!I/6W&amp;W:7:P=GUA2X*B='A!!"N!!Q!597.U&gt;7&amp;M)(*F9W^S:#"M:7ZH&gt;'A!!!V!#!!'&gt;'FN:49U!!!C1&amp;!!"A!!!!%!"1!,!!Q!$1Z49W^Q:3"3:7&amp;E9G&amp;D;Q!!!1!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Scope11Settings" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi1\AcqLib\PXI1ScopeControl11</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">4</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">niScope trigger coupling.ctl</Property>
		<Property Name="typedefName2" Type="Str">niScope trigger slope.ctl</Property>
		<Property Name="typedefName3" Type="Str">niScope trigger window mode.ctl</Property>
		<Property Name="typedefName4" Type="Str">niScope vertical coupling.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"!!A!!!!!!9!":!)2"F&lt;G:P=G.F)(*F97RU;7VF!!!61!I!$H:F=H2J9W&amp;M)(*B&lt;G&gt;F!!!61!I!$WVJ&lt;C"T97VQ&lt;'5A=G&amp;U:1!81!-!%7VJ&lt;C"S:7.P=G1A&lt;'6O:X2I!"6!#A!0&gt;G6S&gt;'FD97QA&lt;W:G=W6U!"&gt;!#A!2=(*P9G5A982U:7ZV982J&lt;WY!&amp;5!(!!^J&lt;H"V&gt;#"J&lt;8"F:'&amp;O9W5!(5!+!"&gt;N98BJ&lt;86N)'FO=(6U)':S:8&amp;V:7ZD?1""!0%!!!!!!!!!!2VO;6.D&lt;X"F)(:F=H2J9W&amp;M)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!-!%8:F=H2J9W&amp;M)'.P&gt;8"M;7ZH!""!)1J&amp;&lt;G&amp;C&lt;'5A6%F4!!!`1"9!"12&amp;:'&gt;F#EBZ=X2F=G6T;8-(2'FH;82B&lt;!:8;7ZE&lt;X=*37VN:72J982F!!!-&gt;(*J:W&gt;F=C"U?8"F!!!41!I!$82S;7&gt;H:8)A:'6M98E!'5!+!"*S:7:F=G6O9W5A='^T;82J&lt;WY!!".!#A!.&gt;(*J:W&gt;F=C"M:8:F&lt;!!21!I!#GBZ=X2F=G6T;8-!!!^!#A!*&lt;'^X)'RF&gt;G6M!"&amp;!#A!+;'FH;#"M:8:F&lt;!!!1!$R!!!!!!!!!!%=&lt;GF49W^Q:3"U=GFH:W6S)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!9!%(2S;7&gt;H:8)A9W^V='RJ&lt;G=!!%5!]1!!!!!!!!!"(WZJ5W.P='5A&gt;(*J:W&gt;F=C"X;7ZE&lt;X=A&lt;7^E:3ZD&gt;'Q!(5!$!".U=GFH:W6S)(&gt;J&lt;G2P&gt;S"N&lt;W2F!$E!]1!!!!!!!!!"'7ZJ5W.P='5A&gt;(*J:W&gt;F=C"T&lt;'^Q:3ZD&gt;'Q!&amp;U!$!!VU=GFH:W6S)(.M&lt;X"F!"B!-0````]/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!!Q!1&gt;(*J:W&gt;F=C"N&lt;W2J:GFF=A!!&amp;E!Q`````QRD;'&amp;O&lt;G6M)'ZB&lt;75!!%*!5!!8!!!!!1!#!!-!"!!&amp;!!9!"Q!)!!E!#A!,!!Q!$1!/!!]!%!!2!")!%Q!5!"5!&amp;AR49W^Q:3"4;7ZH&lt;'5!!!%!&amp;Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Scope12" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi1\AcqLib\PXI1ScopeData12</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;03Q%!!"!!A!!!!!!0!"F!#A!397.U&gt;7&amp;M)(.B&lt;8"M:3"S982F!!!51#%/986U&lt;S"U=GFH:W6S:71!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!":!5!!$!!)!!Q!%#76S=G^S)'^V&gt;!!81!I!%(*F&lt;'&amp;U;8:F37ZJ&gt;'FB&lt;&amp;A!!"&amp;!#A!+?%FO9X*F&lt;76O&gt;!!!"1!+!!!11%!!!@````]!#!.X:GU!$!"1!!-!"A!(!!E!(%"!!!(`````!!I/6W&amp;W:7:P=GUA2X*B='A!!"N!!Q!597.U&gt;7&amp;M)(*F9W^S:#"M:7ZH&gt;'A!!!V!#!!'&gt;'FN:49U!!!C1&amp;!!"A!!!!%!"1!,!!Q!$1Z49W^Q:3"3:7&amp;E9G&amp;D;Q!!!1!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Scope12Settings" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi1\AcqLib\PXI1ScopeControl12</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">4</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">niScope trigger coupling.ctl</Property>
		<Property Name="typedefName2" Type="Str">niScope trigger slope.ctl</Property>
		<Property Name="typedefName3" Type="Str">niScope trigger window mode.ctl</Property>
		<Property Name="typedefName4" Type="Str">niScope vertical coupling.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"!!A!!!!!!9!":!)2"F&lt;G:P=G.F)(*F97RU;7VF!!!61!I!$H:F=H2J9W&amp;M)(*B&lt;G&gt;F!!!61!I!$WVJ&lt;C"T97VQ&lt;'5A=G&amp;U:1!81!-!%7VJ&lt;C"S:7.P=G1A&lt;'6O:X2I!"6!#A!0&gt;G6S&gt;'FD97QA&lt;W:G=W6U!"&gt;!#A!2=(*P9G5A982U:7ZV982J&lt;WY!&amp;5!(!!^J&lt;H"V&gt;#"J&lt;8"F:'&amp;O9W5!(5!+!"&gt;N98BJ&lt;86N)'FO=(6U)':S:8&amp;V:7ZD?1""!0%!!!!!!!!!!2VO;6.D&lt;X"F)(:F=H2J9W&amp;M)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!-!%8:F=H2J9W&amp;M)'.P&gt;8"M;7ZH!""!)1J&amp;&lt;G&amp;C&lt;'5A6%F4!!!`1"9!"12&amp;:'&gt;F#EBZ=X2F=G6T;8-(2'FH;82B&lt;!:8;7ZE&lt;X=*37VN:72J982F!!!-&gt;(*J:W&gt;F=C"U?8"F!!!41!I!$82S;7&gt;H:8)A:'6M98E!'5!+!"*S:7:F=G6O9W5A='^T;82J&lt;WY!!".!#A!.&gt;(*J:W&gt;F=C"M:8:F&lt;!!21!I!#GBZ=X2F=G6T;8-!!!^!#A!*&lt;'^X)'RF&gt;G6M!"&amp;!#A!+;'FH;#"M:8:F&lt;!!!1!$R!!!!!!!!!!%=&lt;GF49W^Q:3"U=GFH:W6S)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!9!%(2S;7&gt;H:8)A9W^V='RJ&lt;G=!!%5!]1!!!!!!!!!"(WZJ5W.P='5A&gt;(*J:W&gt;F=C"X;7ZE&lt;X=A&lt;7^E:3ZD&gt;'Q!(5!$!".U=GFH:W6S)(&gt;J&lt;G2P&gt;S"N&lt;W2F!$E!]1!!!!!!!!!"'7ZJ5W.P='5A&gt;(*J:W&gt;F=C"T&lt;'^Q:3ZD&gt;'Q!&amp;U!$!!VU=GFH:W6S)(.M&lt;X"F!"B!-0````]/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!!Q!1&gt;(*J:W&gt;F=C"N&lt;W2J:GFF=A!!&amp;E!Q`````QRD;'&amp;O&lt;G6M)'ZB&lt;75!!%*!5!!8!!!!!1!#!!-!"!!&amp;!!9!"Q!)!!E!#A!,!!Q!$1!/!!]!%!!2!")!%Q!5!"5!&amp;AR49W^Q:3"4;7ZH&lt;'5!!!%!&amp;Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Scope15" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi1\AcqLib\PXI1ScopeData15</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;03Q%!!"!!A!!!!!!0!"F!#A!397.U&gt;7&amp;M)(.B&lt;8"M:3"S982F!!!51#%/986U&lt;S"U=GFH:W6S:71!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!":!5!!$!!)!!Q!%#76S=G^S)'^V&gt;!!81!I!%(*F&lt;'&amp;U;8:F37ZJ&gt;'FB&lt;&amp;A!!"&amp;!#A!+?%FO9X*F&lt;76O&gt;!!!"1!+!!!11%!!!@````]!#!.X:GU!$!"1!!-!"A!(!!E!(%"!!!(`````!!I/6W&amp;W:7:P=GUA2X*B='A!!"N!!Q!597.U&gt;7&amp;M)(*F9W^S:#"M:7ZH&gt;'A!!!V!#!!'&gt;'FN:49U!!!C1&amp;!!"A!!!!%!"1!,!!Q!$1Z49W^Q:3"3:7&amp;E9G&amp;D;Q!!!1!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Scope15Settings" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi1\AcqLib\PXI1ScopeControl15</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">4</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">niScope trigger coupling.ctl</Property>
		<Property Name="typedefName2" Type="Str">niScope trigger slope.ctl</Property>
		<Property Name="typedefName3" Type="Str">niScope trigger window mode.ctl</Property>
		<Property Name="typedefName4" Type="Str">niScope vertical coupling.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"!!A!!!!!!9!":!)2"F&lt;G:P=G.F)(*F97RU;7VF!!!61!I!$H:F=H2J9W&amp;M)(*B&lt;G&gt;F!!!61!I!$WVJ&lt;C"T97VQ&lt;'5A=G&amp;U:1!81!-!%7VJ&lt;C"S:7.P=G1A&lt;'6O:X2I!"6!#A!0&gt;G6S&gt;'FD97QA&lt;W:G=W6U!"&gt;!#A!2=(*P9G5A982U:7ZV982J&lt;WY!&amp;5!(!!^J&lt;H"V&gt;#"J&lt;8"F:'&amp;O9W5!(5!+!"&gt;N98BJ&lt;86N)'FO=(6U)':S:8&amp;V:7ZD?1""!0%!!!!!!!!!!2VO;6.D&lt;X"F)(:F=H2J9W&amp;M)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!-!%8:F=H2J9W&amp;M)'.P&gt;8"M;7ZH!""!)1J&amp;&lt;G&amp;C&lt;'5A6%F4!!!`1"9!"12&amp;:'&gt;F#EBZ=X2F=G6T;8-(2'FH;82B&lt;!:8;7ZE&lt;X=*37VN:72J982F!!!-&gt;(*J:W&gt;F=C"U?8"F!!!41!I!$82S;7&gt;H:8)A:'6M98E!'5!+!"*S:7:F=G6O9W5A='^T;82J&lt;WY!!".!#A!.&gt;(*J:W&gt;F=C"M:8:F&lt;!!21!I!#GBZ=X2F=G6T;8-!!!^!#A!*&lt;'^X)'RF&gt;G6M!"&amp;!#A!+;'FH;#"M:8:F&lt;!!!1!$R!!!!!!!!!!%=&lt;GF49W^Q:3"U=GFH:W6S)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!9!%(2S;7&gt;H:8)A9W^V='RJ&lt;G=!!%5!]1!!!!!!!!!"(WZJ5W.P='5A&gt;(*J:W&gt;F=C"X;7ZE&lt;X=A&lt;7^E:3ZD&gt;'Q!(5!$!".U=GFH:W6S)(&gt;J&lt;G2P&gt;S"N&lt;W2F!$E!]1!!!!!!!!!"'7ZJ5W.P='5A&gt;(*J:W&gt;F=C"T&lt;'^Q:3ZD&gt;'Q!&amp;U!$!!VU=GFH:W6S)(.M&lt;X"F!"B!-0````]/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!!Q!1&gt;(*J:W&gt;F=C"N&lt;W2J:GFF=A!!&amp;E!Q`````QRD;'&amp;O&lt;G6M)'ZB&lt;75!!%*!5!!8!!!!!1!#!!-!"!!&amp;!!9!"Q!)!!E!#A!,!!Q!$1!/!!]!%!!2!")!%Q!5!"5!&amp;AR49W^Q:3"4;7ZH&lt;'5!!!%!&amp;Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Scope16" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi1\AcqLib\PXI1ScopeData16</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;03Q%!!"!!A!!!!!!0!"F!#A!397.U&gt;7&amp;M)(.B&lt;8"M:3"S982F!!!51#%/986U&lt;S"U=GFH:W6S:71!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!":!5!!$!!)!!Q!%#76S=G^S)'^V&gt;!!81!I!%(*F&lt;'&amp;U;8:F37ZJ&gt;'FB&lt;&amp;A!!"&amp;!#A!+?%FO9X*F&lt;76O&gt;!!!"1!+!!!11%!!!@````]!#!.X:GU!$!"1!!-!"A!(!!E!(%"!!!(`````!!I/6W&amp;W:7:P=GUA2X*B='A!!"N!!Q!597.U&gt;7&amp;M)(*F9W^S:#"M:7ZH&gt;'A!!!V!#!!'&gt;'FN:49U!!!C1&amp;!!"A!!!!%!"1!,!!Q!$1Z49W^Q:3"3:7&amp;E9G&amp;D;Q!!!1!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Scope16Settings" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi1\AcqLib\PXI1ScopeControl16</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">4</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">niScope trigger coupling.ctl</Property>
		<Property Name="typedefName2" Type="Str">niScope trigger slope.ctl</Property>
		<Property Name="typedefName3" Type="Str">niScope trigger window mode.ctl</Property>
		<Property Name="typedefName4" Type="Str">niScope vertical coupling.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"!!A!!!!!!9!":!)2"F&lt;G:P=G.F)(*F97RU;7VF!!!61!I!$H:F=H2J9W&amp;M)(*B&lt;G&gt;F!!!61!I!$WVJ&lt;C"T97VQ&lt;'5A=G&amp;U:1!81!-!%7VJ&lt;C"S:7.P=G1A&lt;'6O:X2I!"6!#A!0&gt;G6S&gt;'FD97QA&lt;W:G=W6U!"&gt;!#A!2=(*P9G5A982U:7ZV982J&lt;WY!&amp;5!(!!^J&lt;H"V&gt;#"J&lt;8"F:'&amp;O9W5!(5!+!"&gt;N98BJ&lt;86N)'FO=(6U)':S:8&amp;V:7ZD?1""!0%!!!!!!!!!!2VO;6.D&lt;X"F)(:F=H2J9W&amp;M)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!-!%8:F=H2J9W&amp;M)'.P&gt;8"M;7ZH!""!)1J&amp;&lt;G&amp;C&lt;'5A6%F4!!!`1"9!"12&amp;:'&gt;F#EBZ=X2F=G6T;8-(2'FH;82B&lt;!:8;7ZE&lt;X=*37VN:72J982F!!!-&gt;(*J:W&gt;F=C"U?8"F!!!41!I!$82S;7&gt;H:8)A:'6M98E!'5!+!"*S:7:F=G6O9W5A='^T;82J&lt;WY!!".!#A!.&gt;(*J:W&gt;F=C"M:8:F&lt;!!21!I!#GBZ=X2F=G6T;8-!!!^!#A!*&lt;'^X)'RF&gt;G6M!"&amp;!#A!+;'FH;#"M:8:F&lt;!!!1!$R!!!!!!!!!!%=&lt;GF49W^Q:3"U=GFH:W6S)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!9!%(2S;7&gt;H:8)A9W^V='RJ&lt;G=!!%5!]1!!!!!!!!!"(WZJ5W.P='5A&gt;(*J:W&gt;F=C"X;7ZE&lt;X=A&lt;7^E:3ZD&gt;'Q!(5!$!".U=GFH:W6S)(&gt;J&lt;G2P&gt;S"N&lt;W2F!$E!]1!!!!!!!!!"'7ZJ5W.P='5A&gt;(*J:W&gt;F=C"T&lt;'^Q:3ZD&gt;'Q!&amp;U!$!!VU=GFH:W6S)(.M&lt;X"F!"B!-0````]/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!!Q!1&gt;(*J:W&gt;F=C"N&lt;W2J:GFF=A!!&amp;E!Q`````QRD;'&amp;O&lt;G6M)'ZB&lt;75!!%*!5!!8!!!!!1!#!!-!"!!&amp;!!9!"Q!)!!E!#A!,!!Q!$1!/!!]!%!!2!")!%Q!5!"5!&amp;AR49W^Q:3"4;7ZH&lt;'5!!!%!&amp;Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Scope17" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi1\AcqLib\PXI1ScopeData17</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;03Q%!!"!!A!!!!!!0!"F!#A!397.U&gt;7&amp;M)(.B&lt;8"M:3"S982F!!!51#%/986U&lt;S"U=GFH:W6S:71!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!":!5!!$!!)!!Q!%#76S=G^S)'^V&gt;!!81!I!%(*F&lt;'&amp;U;8:F37ZJ&gt;'FB&lt;&amp;A!!"&amp;!#A!+?%FO9X*F&lt;76O&gt;!!!"1!+!!!11%!!!@````]!#!.X:GU!$!"1!!-!"A!(!!E!(%"!!!(`````!!I/6W&amp;W:7:P=GUA2X*B='A!!"N!!Q!597.U&gt;7&amp;M)(*F9W^S:#"M:7ZH&gt;'A!!!V!#!!'&gt;'FN:49U!!!C1&amp;!!"A!!!!%!"1!,!!Q!$1Z49W^Q:3"3:7&amp;E9G&amp;D;Q!!!1!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Scope17Settings" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi1\AcqLib\PXI1ScopeControl17</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">4</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">niScope trigger coupling.ctl</Property>
		<Property Name="typedefName2" Type="Str">niScope trigger slope.ctl</Property>
		<Property Name="typedefName3" Type="Str">niScope trigger window mode.ctl</Property>
		<Property Name="typedefName4" Type="Str">niScope vertical coupling.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"!!A!!!!!!9!":!)2"F&lt;G:P=G.F)(*F97RU;7VF!!!61!I!$H:F=H2J9W&amp;M)(*B&lt;G&gt;F!!!61!I!$WVJ&lt;C"T97VQ&lt;'5A=G&amp;U:1!81!-!%7VJ&lt;C"S:7.P=G1A&lt;'6O:X2I!"6!#A!0&gt;G6S&gt;'FD97QA&lt;W:G=W6U!"&gt;!#A!2=(*P9G5A982U:7ZV982J&lt;WY!&amp;5!(!!^J&lt;H"V&gt;#"J&lt;8"F:'&amp;O9W5!(5!+!"&gt;N98BJ&lt;86N)'FO=(6U)':S:8&amp;V:7ZD?1""!0%!!!!!!!!!!2VO;6.D&lt;X"F)(:F=H2J9W&amp;M)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!-!%8:F=H2J9W&amp;M)'.P&gt;8"M;7ZH!""!)1J&amp;&lt;G&amp;C&lt;'5A6%F4!!!`1"9!"12&amp;:'&gt;F#EBZ=X2F=G6T;8-(2'FH;82B&lt;!:8;7ZE&lt;X=*37VN:72J982F!!!-&gt;(*J:W&gt;F=C"U?8"F!!!41!I!$82S;7&gt;H:8)A:'6M98E!'5!+!"*S:7:F=G6O9W5A='^T;82J&lt;WY!!".!#A!.&gt;(*J:W&gt;F=C"M:8:F&lt;!!21!I!#GBZ=X2F=G6T;8-!!!^!#A!*&lt;'^X)'RF&gt;G6M!"&amp;!#A!+;'FH;#"M:8:F&lt;!!!1!$R!!!!!!!!!!%=&lt;GF49W^Q:3"U=GFH:W6S)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!9!%(2S;7&gt;H:8)A9W^V='RJ&lt;G=!!%5!]1!!!!!!!!!"(WZJ5W.P='5A&gt;(*J:W&gt;F=C"X;7ZE&lt;X=A&lt;7^E:3ZD&gt;'Q!(5!$!".U=GFH:W6S)(&gt;J&lt;G2P&gt;S"N&lt;W2F!$E!]1!!!!!!!!!"'7ZJ5W.P='5A&gt;(*J:W&gt;F=C"T&lt;'^Q:3ZD&gt;'Q!&amp;U!$!!VU=GFH:W6S)(.M&lt;X"F!"B!-0````]/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!!Q!1&gt;(*J:W&gt;F=C"N&lt;W2J:GFF=A!!&amp;E!Q`````QRD;'&amp;O&lt;G6M)'ZB&lt;75!!%*!5!!8!!!!!1!#!!-!"!!&amp;!!9!"Q!)!!E!#A!,!!Q!$1!/!!]!%!!2!")!%Q!5!"5!&amp;AR49W^Q:3"4;7ZH&lt;'5!!!%!&amp;Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Scope18" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi1\AcqLib\PXI1ScopeData18</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;03Q%!!"!!A!!!!!!0!"F!#A!397.U&gt;7&amp;M)(.B&lt;8"M:3"S982F!!!51#%/986U&lt;S"U=GFH:W6S:71!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!":!5!!$!!)!!Q!%#76S=G^S)'^V&gt;!!81!I!%(*F&lt;'&amp;U;8:F37ZJ&gt;'FB&lt;&amp;A!!"&amp;!#A!+?%FO9X*F&lt;76O&gt;!!!"1!+!!!11%!!!@````]!#!.X:GU!$!"1!!-!"A!(!!E!(%"!!!(`````!!I/6W&amp;W:7:P=GUA2X*B='A!!"N!!Q!597.U&gt;7&amp;M)(*F9W^S:#"M:7ZH&gt;'A!!!V!#!!'&gt;'FN:49U!!!C1&amp;!!"A!!!!%!"1!,!!Q!$1Z49W^Q:3"3:7&amp;E9G&amp;D;Q!!!1!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Scope18Settings" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-pxi1\AcqLib\PXI1ScopeControl18</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">4</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">niScope trigger coupling.ctl</Property>
		<Property Name="typedefName2" Type="Str">niScope trigger slope.ctl</Property>
		<Property Name="typedefName3" Type="Str">niScope trigger window mode.ctl</Property>
		<Property Name="typedefName4" Type="Str">niScope vertical coupling.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"!!A!!!!!!9!":!)2"F&lt;G:P=G.F)(*F97RU;7VF!!!61!I!$H:F=H2J9W&amp;M)(*B&lt;G&gt;F!!!61!I!$WVJ&lt;C"T97VQ&lt;'5A=G&amp;U:1!81!-!%7VJ&lt;C"S:7.P=G1A&lt;'6O:X2I!"6!#A!0&gt;G6S&gt;'FD97QA&lt;W:G=W6U!"&gt;!#A!2=(*P9G5A982U:7ZV982J&lt;WY!&amp;5!(!!^J&lt;H"V&gt;#"J&lt;8"F:'&amp;O9W5!(5!+!"&gt;N98BJ&lt;86N)'FO=(6U)':S:8&amp;V:7ZD?1""!0%!!!!!!!!!!2VO;6.D&lt;X"F)(:F=H2J9W&amp;M)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!-!%8:F=H2J9W&amp;M)'.P&gt;8"M;7ZH!""!)1J&amp;&lt;G&amp;C&lt;'5A6%F4!!!`1"9!"12&amp;:'&gt;F#EBZ=X2F=G6T;8-(2'FH;82B&lt;!:8;7ZE&lt;X=*37VN:72J982F!!!-&gt;(*J:W&gt;F=C"U?8"F!!!41!I!$82S;7&gt;H:8)A:'6M98E!'5!+!"*S:7:F=G6O9W5A='^T;82J&lt;WY!!".!#A!.&gt;(*J:W&gt;F=C"M:8:F&lt;!!21!I!#GBZ=X2F=G6T;8-!!!^!#A!*&lt;'^X)'RF&gt;G6M!"&amp;!#A!+;'FH;#"M:8:F&lt;!!!1!$R!!!!!!!!!!%=&lt;GF49W^Q:3"U=GFH:W6S)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!9!%(2S;7&gt;H:8)A9W^V='RJ&lt;G=!!%5!]1!!!!!!!!!"(WZJ5W.P='5A&gt;(*J:W&gt;F=C"X;7ZE&lt;X=A&lt;7^E:3ZD&gt;'Q!(5!$!".U=GFH:W6S)(&gt;J&lt;G2P&gt;S"N&lt;W2F!$E!]1!!!!!!!!!"'7ZJ5W.P='5A&gt;(*J:W&gt;F=C"T&lt;'^Q:3ZD&gt;'Q!&amp;U!$!!VU=GFH:W6S)(.M&lt;X"F!"B!-0````]/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!!Q!1&gt;(*J:W&gt;F=C"N&lt;W2J:GFF=A!!&amp;E!Q`````QRD;'&amp;O&lt;G6M)'ZB&lt;75!!%*!5!!8!!!!!1!#!!-!"!!&amp;!!9!"Q!)!!E!#A!,!!Q!$1!/!!]!%!!2!")!%Q!5!"5!&amp;AR49W^Q:3"4;7ZH&lt;'5!!!%!&amp;Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Time" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\ndcxii-s0\GTiming\PXI1Timing</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">2</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">GFTy 9404.lvlib:GFT9404_Auxiliary Delay Channels.ctl</Property>
		<Property Name="typedefName2" Type="Str">GFTy 9404.lvlib:GFT9404_Precision Delay Channels.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/GreenfieldTechnology GFT9404/_Typedefs/GFT9404_Auxiliary Delay Channels.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/GreenfieldTechnology GFT9404/_Typedefs/GFT9404_Precision Delay Channels.ctl</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"!!A!!!!!!@!"B!-0````]0=W^V=G.F)(2F=GVJ&lt;G&amp;M!"2!1!!"`````Q!!"F.P&gt;8*D:1!!(E!Q`````R2E:8.U;7ZB&gt;'FP&lt;C"U:8*N;7ZB&lt;!!!'%"!!!(`````!!),2'6T&gt;'FO982J&lt;WY!&amp;E"1!!)!!1!$#U.P&lt;GZF9X2J&lt;WZT!!1!)1!-1#%'68"E982F!!!/1#%)47&amp;O)&amp;2S;7=!!#F!&amp;A!##%^O:3"4;'^U#F*F='6U;82J&gt;G5!$(2S;7&gt;H:8)A&lt;7^E:1!!-5!7!!-)37ZU:8*O97Q(6&amp;**2S"*4AB17%EA5V2"5A!/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!#A!2&gt;(*J:W&gt;F=C"M:8:F&lt;#!I6CE!*U!7!!))5'^T;82J&gt;G5)4G6H982J&gt;G5!$82S;7&gt;H:8)A=WRP='5!(5!+!":U=GFH:W6S)':S:8&amp;V:7ZD?3!I3(IJ!!!91&amp;!!"1!)!!E!#A!,!!Q(6(*J:W&gt;F=A"5!0%!!!!!!!!!!A^(2F2Z)$EU-$1O&lt;(:M;7)E2U:5/41Q.&amp;^1=G6D;8.J&lt;WYA2'6M98EA1WBB&lt;GZF&lt;(-O9X2M!"&gt;!!Q!.:'6M98EA9WBB&lt;GZF&lt;!!01!I!#72F&lt;'&amp;Z)#BT+1!41!I!$7&amp;N='RJ&gt;(6E:3!I6CE!%5!+!!JX;72U;#!I&lt;H-J!!!31#%.&lt;X6U=(6U)'6O97*M:1!?1&amp;!!"1!/!!]!%!!2!").2'6M98EA1WBB&lt;GZF&lt;!!71%!!!@````]!%QB$;'&amp;O&lt;G6M=Q!!6!$R!!!!!!!!!!)02U:5?3!Z.$!U,GRW&lt;'FC*%&gt;'6$EU-$2@186Y;7RJ98*Z)%2F&lt;'&amp;Z)%.I97ZO:7RT,G.U&lt;!!81!-!$72F&lt;'&amp;Z)'.I97ZO:7Q!&amp;%!B$G.P&lt;GZF9X1A&gt;']A5&amp;B*!!!71&amp;!!"!!6!!]!%A!7"U.M&gt;8.U:8)!'E"!!!(`````!"=-186Y)%.I97ZO:7RT!!!F1"9!!AB3:7RB&gt;'FW:1B"9H.P&lt;(6U:1!+:'6M98EA&gt;(FQ:1!!&amp;%!B$EVB&lt;H6B&lt;#"5=GFH:W6S!!!?1&amp;!!"1!.!"1!'!!:!"I-1G^B=G1A1W^O:GFH!!!11&amp;!!!1!&lt;"U&gt;'6$EU-$1!(%"!!!(`````!"Q06(*J:W&gt;F=C".&lt;W2V&lt;'6T!"Z!5!!&amp;!!1!"1!'!!=!(1R$=G&amp;U:3"5;7VJ&lt;G=!!!%!(A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope4" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\NDCXII-PXI3\AcqLib\PXI3ScopeData4</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;03Q%!!"!!A!!!!!!0!"F!#A!397.U&gt;7&amp;M)(.B&lt;8"M:3"S982F!!!51#%/986U&lt;S"U=GFH:W6S:71!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!":!5!!$!!)!!Q!%#76S=G^S)'^V&gt;!!81!I!%(*F&lt;'&amp;U;8:F37ZJ&gt;'FB&lt;&amp;A!!"&amp;!#A!+?%FO9X*F&lt;76O&gt;!!!"1!+!!!11%!!!@````]!#!.X:GU!$!"1!!-!"A!(!!E!(%"!!!(`````!!I/6W&amp;W:7:P=GUA2X*B='A!!"N!!Q!597.U&gt;7&amp;M)(*F9W^S:#"M:7ZH&gt;'A!!!V!#!!'&gt;'FN:49U!!!C1&amp;!!"A!!!!%!"1!,!!Q!$1Z49W^Q:3"3:7&amp;E9G&amp;D;Q!!!1!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope4Settings" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\NDCXII-PXI3\AcqLib\PXI3ScopeControl4</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">4</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">niScope trigger coupling.ctl</Property>
		<Property Name="typedefName2" Type="Str">niScope trigger slope.ctl</Property>
		<Property Name="typedefName3" Type="Str">niScope trigger window mode.ctl</Property>
		<Property Name="typedefName4" Type="Str">niScope vertical coupling.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"!!A!!!!!!9!":!)2"F&lt;G:P=G.F)(*F97RU;7VF!!!61!I!$H:F=H2J9W&amp;M)(*B&lt;G&gt;F!!!61!I!$WVJ&lt;C"T97VQ&lt;'5A=G&amp;U:1!81!-!%7VJ&lt;C"S:7.P=G1A&lt;'6O:X2I!"6!#A!0&gt;G6S&gt;'FD97QA&lt;W:G=W6U!"&gt;!#A!2=(*P9G5A982U:7ZV982J&lt;WY!&amp;5!(!!^J&lt;H"V&gt;#"J&lt;8"F:'&amp;O9W5!(5!+!"&gt;N98BJ&lt;86N)'FO=(6U)':S:8&amp;V:7ZD?1""!0%!!!!!!!!!!2VO;6.D&lt;X"F)(:F=H2J9W&amp;M)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!-!%8:F=H2J9W&amp;M)'.P&gt;8"M;7ZH!""!)1J&amp;&lt;G&amp;C&lt;'5A6%F4!!!`1"9!"12&amp;:'&gt;F#EBZ=X2F=G6T;8-(2'FH;82B&lt;!:8;7ZE&lt;X=*37VN:72J982F!!!-&gt;(*J:W&gt;F=C"U?8"F!!!41!I!$82S;7&gt;H:8)A:'6M98E!'5!+!"*S:7:F=G6O9W5A='^T;82J&lt;WY!!".!#A!.&gt;(*J:W&gt;F=C"M:8:F&lt;!!21!I!#GBZ=X2F=G6T;8-!!!^!#A!*&lt;'^X)'RF&gt;G6M!"&amp;!#A!+;'FH;#"M:8:F&lt;!!!1!$R!!!!!!!!!!%=&lt;GF49W^Q:3"U=GFH:W6S)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!9!%(2S;7&gt;H:8)A9W^V='RJ&lt;G=!!%5!]1!!!!!!!!!"(WZJ5W.P='5A&gt;(*J:W&gt;F=C"X;7ZE&lt;X=A&lt;7^E:3ZD&gt;'Q!(5!$!".U=GFH:W6S)(&gt;J&lt;G2P&gt;S"N&lt;W2F!$E!]1!!!!!!!!!"'7ZJ5W.P='5A&gt;(*J:W&gt;F=C"T&lt;'^Q:3ZD&gt;'Q!&amp;U!$!!VU=GFH:W6S)(.M&lt;X"F!"B!-0````]/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!!Q!1&gt;(*J:W&gt;F=C"N&lt;W2J:GFF=A!!&amp;E!Q`````QRD;'&amp;O&lt;G6M)'ZB&lt;75!!%*!5!!8!!!!!1!#!!-!"!!&amp;!!9!"Q!)!!E!#A!,!!Q!$1!/!!]!%!!2!")!%Q!5!"5!&amp;AR49W^Q:3"4;7ZH&lt;'5!!!%!&amp;Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope5" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\NDCXII-PXI3\AcqLib\PXI3ScopeData5</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;03Q%!!"!!A!!!!!!0!"F!#A!397.U&gt;7&amp;M)(.B&lt;8"M:3"S982F!!!51#%/986U&lt;S"U=GFH:W6S:71!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!":!5!!$!!)!!Q!%#76S=G^S)'^V&gt;!!81!I!%(*F&lt;'&amp;U;8:F37ZJ&gt;'FB&lt;&amp;A!!"&amp;!#A!+?%FO9X*F&lt;76O&gt;!!!"1!+!!!11%!!!@````]!#!.X:GU!$!"1!!-!"A!(!!E!(%"!!!(`````!!I/6W&amp;W:7:P=GUA2X*B='A!!"N!!Q!597.U&gt;7&amp;M)(*F9W^S:#"M:7ZH&gt;'A!!!V!#!!'&gt;'FN:49U!!!C1&amp;!!"A!!!!%!"1!,!!Q!$1Z49W^Q:3"3:7&amp;E9G&amp;D;Q!!!1!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope5Settings" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\NDCXII-PXI3\AcqLib\PXI3ScopeControl5</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">4</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">niScope trigger coupling.ctl</Property>
		<Property Name="typedefName2" Type="Str">niScope trigger slope.ctl</Property>
		<Property Name="typedefName3" Type="Str">niScope trigger window mode.ctl</Property>
		<Property Name="typedefName4" Type="Str">niScope vertical coupling.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"!!A!!!!!!9!":!)2"F&lt;G:P=G.F)(*F97RU;7VF!!!61!I!$H:F=H2J9W&amp;M)(*B&lt;G&gt;F!!!61!I!$WVJ&lt;C"T97VQ&lt;'5A=G&amp;U:1!81!-!%7VJ&lt;C"S:7.P=G1A&lt;'6O:X2I!"6!#A!0&gt;G6S&gt;'FD97QA&lt;W:G=W6U!"&gt;!#A!2=(*P9G5A982U:7ZV982J&lt;WY!&amp;5!(!!^J&lt;H"V&gt;#"J&lt;8"F:'&amp;O9W5!(5!+!"&gt;N98BJ&lt;86N)'FO=(6U)':S:8&amp;V:7ZD?1""!0%!!!!!!!!!!2VO;6.D&lt;X"F)(:F=H2J9W&amp;M)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!-!%8:F=H2J9W&amp;M)'.P&gt;8"M;7ZH!""!)1J&amp;&lt;G&amp;C&lt;'5A6%F4!!!`1"9!"12&amp;:'&gt;F#EBZ=X2F=G6T;8-(2'FH;82B&lt;!:8;7ZE&lt;X=*37VN:72J982F!!!-&gt;(*J:W&gt;F=C"U?8"F!!!41!I!$82S;7&gt;H:8)A:'6M98E!'5!+!"*S:7:F=G6O9W5A='^T;82J&lt;WY!!".!#A!.&gt;(*J:W&gt;F=C"M:8:F&lt;!!21!I!#GBZ=X2F=G6T;8-!!!^!#A!*&lt;'^X)'RF&gt;G6M!"&amp;!#A!+;'FH;#"M:8:F&lt;!!!1!$R!!!!!!!!!!%=&lt;GF49W^Q:3"U=GFH:W6S)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!9!%(2S;7&gt;H:8)A9W^V='RJ&lt;G=!!%5!]1!!!!!!!!!"(WZJ5W.P='5A&gt;(*J:W&gt;F=C"X;7ZE&lt;X=A&lt;7^E:3ZD&gt;'Q!(5!$!".U=GFH:W6S)(&gt;J&lt;G2P&gt;S"N&lt;W2F!$E!]1!!!!!!!!!"'7ZJ5W.P='5A&gt;(*J:W&gt;F=C"T&lt;'^Q:3ZD&gt;'Q!&amp;U!$!!VU=GFH:W6S)(.M&lt;X"F!"B!-0````]/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!!Q!1&gt;(*J:W&gt;F=C"N&lt;W2J:GFF=A!!&amp;E!Q`````QRD;'&amp;O&lt;G6M)'ZB&lt;75!!%*!5!!8!!!!!1!#!!-!"!!&amp;!!9!"Q!)!!E!#A!,!!Q!$1!/!!]!%!!2!")!%Q!5!"5!&amp;AR49W^Q:3"4;7ZH&lt;'5!!!%!&amp;Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope6" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\NDCXII-PXI3\AcqLib\PXI3ScopeData6</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;03Q%!!"!!A!!!!!!0!"F!#A!397.U&gt;7&amp;M)(.B&lt;8"M:3"S982F!!!51#%/986U&lt;S"U=GFH:W6S:71!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!":!5!!$!!)!!Q!%#76S=G^S)'^V&gt;!!81!I!%(*F&lt;'&amp;U;8:F37ZJ&gt;'FB&lt;&amp;A!!"&amp;!#A!+?%FO9X*F&lt;76O&gt;!!!"1!+!!!11%!!!@````]!#!.X:GU!$!"1!!-!"A!(!!E!(%"!!!(`````!!I/6W&amp;W:7:P=GUA2X*B='A!!"N!!Q!597.U&gt;7&amp;M)(*F9W^S:#"M:7ZH&gt;'A!!!V!#!!'&gt;'FN:49U!!!C1&amp;!!"A!!!!%!"1!,!!Q!$1Z49W^Q:3"3:7&amp;E9G&amp;D;Q!!!1!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope6Settings" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\NDCXII-PXI3\AcqLib\PXI3ScopeControl6</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">4</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">niScope trigger coupling.ctl</Property>
		<Property Name="typedefName2" Type="Str">niScope trigger slope.ctl</Property>
		<Property Name="typedefName3" Type="Str">niScope trigger window mode.ctl</Property>
		<Property Name="typedefName4" Type="Str">niScope vertical coupling.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"!!A!!!!!!9!":!)2"F&lt;G:P=G.F)(*F97RU;7VF!!!61!I!$H:F=H2J9W&amp;M)(*B&lt;G&gt;F!!!61!I!$WVJ&lt;C"T97VQ&lt;'5A=G&amp;U:1!81!-!%7VJ&lt;C"S:7.P=G1A&lt;'6O:X2I!"6!#A!0&gt;G6S&gt;'FD97QA&lt;W:G=W6U!"&gt;!#A!2=(*P9G5A982U:7ZV982J&lt;WY!&amp;5!(!!^J&lt;H"V&gt;#"J&lt;8"F:'&amp;O9W5!(5!+!"&gt;N98BJ&lt;86N)'FO=(6U)':S:8&amp;V:7ZD?1""!0%!!!!!!!!!!2VO;6.D&lt;X"F)(:F=H2J9W&amp;M)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!-!%8:F=H2J9W&amp;M)'.P&gt;8"M;7ZH!""!)1J&amp;&lt;G&amp;C&lt;'5A6%F4!!!`1"9!"12&amp;:'&gt;F#EBZ=X2F=G6T;8-(2'FH;82B&lt;!:8;7ZE&lt;X=*37VN:72J982F!!!-&gt;(*J:W&gt;F=C"U?8"F!!!41!I!$82S;7&gt;H:8)A:'6M98E!'5!+!"*S:7:F=G6O9W5A='^T;82J&lt;WY!!".!#A!.&gt;(*J:W&gt;F=C"M:8:F&lt;!!21!I!#GBZ=X2F=G6T;8-!!!^!#A!*&lt;'^X)'RF&gt;G6M!"&amp;!#A!+;'FH;#"M:8:F&lt;!!!1!$R!!!!!!!!!!%=&lt;GF49W^Q:3"U=GFH:W6S)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!9!%(2S;7&gt;H:8)A9W^V='RJ&lt;G=!!%5!]1!!!!!!!!!"(WZJ5W.P='5A&gt;(*J:W&gt;F=C"X;7ZE&lt;X=A&lt;7^E:3ZD&gt;'Q!(5!$!".U=GFH:W6S)(&gt;J&lt;G2P&gt;S"N&lt;W2F!$E!]1!!!!!!!!!"'7ZJ5W.P='5A&gt;(*J:W&gt;F=C"T&lt;'^Q:3ZD&gt;'Q!&amp;U!$!!VU=GFH:W6S)(.M&lt;X"F!"B!-0````]/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!!Q!1&gt;(*J:W&gt;F=C"N&lt;W2J:GFF=A!!&amp;E!Q`````QRD;'&amp;O&lt;G6M)'ZB&lt;75!!%*!5!!8!!!!!1!#!!-!"!!&amp;!!9!"Q!)!!E!#A!,!!Q!$1!/!!]!%!!2!")!%Q!5!"5!&amp;AR49W^Q:3"4;7ZH&lt;'5!!!%!&amp;Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope8" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\NDCXII-PXI3\AcqLib\PXI3ScopeData8</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;03Q%!!"!!A!!!!!!0!"F!#A!397.U&gt;7&amp;M)(.B&lt;8"M:3"S982F!!!51#%/986U&lt;S"U=GFH:W6S:71!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!":!5!!$!!)!!Q!%#76S=G^S)'^V&gt;!!81!I!%(*F&lt;'&amp;U;8:F37ZJ&gt;'FB&lt;&amp;A!!"&amp;!#A!+?%FO9X*F&lt;76O&gt;!!!"1!+!!!11%!!!@````]!#!.X:GU!$!"1!!-!"A!(!!E!(%"!!!(`````!!I/6W&amp;W:7:P=GUA2X*B='A!!"N!!Q!597.U&gt;7&amp;M)(*F9W^S:#"M:7ZH&gt;'A!!!V!#!!'&gt;'FN:49U!!!C1&amp;!!"A!!!!%!"1!,!!Q!$1Z49W^Q:3"3:7&amp;E9G&amp;D;Q!!!1!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope8Settings" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\NDCXII-PXI3\AcqLib\PXI3ScopeControl8</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">4</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">niScope trigger coupling.ctl</Property>
		<Property Name="typedefName2" Type="Str">niScope trigger slope.ctl</Property>
		<Property Name="typedefName3" Type="Str">niScope trigger window mode.ctl</Property>
		<Property Name="typedefName4" Type="Str">niScope vertical coupling.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"!!A!!!!!!9!":!)2"F&lt;G:P=G.F)(*F97RU;7VF!!!61!I!$H:F=H2J9W&amp;M)(*B&lt;G&gt;F!!!61!I!$WVJ&lt;C"T97VQ&lt;'5A=G&amp;U:1!81!-!%7VJ&lt;C"S:7.P=G1A&lt;'6O:X2I!"6!#A!0&gt;G6S&gt;'FD97QA&lt;W:G=W6U!"&gt;!#A!2=(*P9G5A982U:7ZV982J&lt;WY!&amp;5!(!!^J&lt;H"V&gt;#"J&lt;8"F:'&amp;O9W5!(5!+!"&gt;N98BJ&lt;86N)'FO=(6U)':S:8&amp;V:7ZD?1""!0%!!!!!!!!!!2VO;6.D&lt;X"F)(:F=H2J9W&amp;M)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!-!%8:F=H2J9W&amp;M)'.P&gt;8"M;7ZH!""!)1J&amp;&lt;G&amp;C&lt;'5A6%F4!!!`1"9!"12&amp;:'&gt;F#EBZ=X2F=G6T;8-(2'FH;82B&lt;!:8;7ZE&lt;X=*37VN:72J982F!!!-&gt;(*J:W&gt;F=C"U?8"F!!!41!I!$82S;7&gt;H:8)A:'6M98E!'5!+!"*S:7:F=G6O9W5A='^T;82J&lt;WY!!".!#A!.&gt;(*J:W&gt;F=C"M:8:F&lt;!!21!I!#GBZ=X2F=G6T;8-!!!^!#A!*&lt;'^X)'RF&gt;G6M!"&amp;!#A!+;'FH;#"M:8:F&lt;!!!1!$R!!!!!!!!!!%=&lt;GF49W^Q:3"U=GFH:W6S)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!9!%(2S;7&gt;H:8)A9W^V='RJ&lt;G=!!%5!]1!!!!!!!!!"(WZJ5W.P='5A&gt;(*J:W&gt;F=C"X;7ZE&lt;X=A&lt;7^E:3ZD&gt;'Q!(5!$!".U=GFH:W6S)(&gt;J&lt;G2P&gt;S"N&lt;W2F!$E!]1!!!!!!!!!"'7ZJ5W.P='5A&gt;(*J:W&gt;F=C"T&lt;'^Q:3ZD&gt;'Q!&amp;U!$!!VU=GFH:W6S)(.M&lt;X"F!"B!-0````]/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!!Q!1&gt;(*J:W&gt;F=C"N&lt;W2J:GFF=A!!&amp;E!Q`````QRD;'&amp;O&lt;G6M)'ZB&lt;75!!%*!5!!8!!!!!1!#!!-!"!!&amp;!!9!"Q!)!!E!#A!,!!Q!$1!/!!]!%!!2!")!%Q!5!"5!&amp;AR49W^Q:3"4;7ZH&lt;'5!!!%!&amp;Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope9" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\NDCXII-PXI3\AcqLib\PXI3ScopeData9</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;03Q%!!"!!A!!!!!!0!"F!#A!397.U&gt;7&amp;M)(.B&lt;8"M:3"S982F!!!51#%/986U&lt;S"U=GFH:W6S:71!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!":!5!!$!!)!!Q!%#76S=G^S)'^V&gt;!!81!I!%(*F&lt;'&amp;U;8:F37ZJ&gt;'FB&lt;&amp;A!!"&amp;!#A!+?%FO9X*F&lt;76O&gt;!!!"1!+!!!11%!!!@````]!#!.X:GU!$!"1!!-!"A!(!!E!(%"!!!(`````!!I/6W&amp;W:7:P=GUA2X*B='A!!"N!!Q!597.U&gt;7&amp;M)(*F9W^S:#"M:7ZH&gt;'A!!!V!#!!'&gt;'FN:49U!!!C1&amp;!!"A!!!!%!"1!,!!Q!$1Z49W^Q:3"3:7&amp;E9G&amp;D;Q!!!1!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope9Settings" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\NDCXII-PXI3\AcqLib\PXI3ScopeControl9</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">4</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">niScope trigger coupling.ctl</Property>
		<Property Name="typedefName2" Type="Str">niScope trigger slope.ctl</Property>
		<Property Name="typedefName3" Type="Str">niScope trigger window mode.ctl</Property>
		<Property Name="typedefName4" Type="Str">niScope vertical coupling.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"!!A!!!!!!9!":!)2"F&lt;G:P=G.F)(*F97RU;7VF!!!61!I!$H:F=H2J9W&amp;M)(*B&lt;G&gt;F!!!61!I!$WVJ&lt;C"T97VQ&lt;'5A=G&amp;U:1!81!-!%7VJ&lt;C"S:7.P=G1A&lt;'6O:X2I!"6!#A!0&gt;G6S&gt;'FD97QA&lt;W:G=W6U!"&gt;!#A!2=(*P9G5A982U:7ZV982J&lt;WY!&amp;5!(!!^J&lt;H"V&gt;#"J&lt;8"F:'&amp;O9W5!(5!+!"&gt;N98BJ&lt;86N)'FO=(6U)':S:8&amp;V:7ZD?1""!0%!!!!!!!!!!2VO;6.D&lt;X"F)(:F=H2J9W&amp;M)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!-!%8:F=H2J9W&amp;M)'.P&gt;8"M;7ZH!""!)1J&amp;&lt;G&amp;C&lt;'5A6%F4!!!`1"9!"12&amp;:'&gt;F#EBZ=X2F=G6T;8-(2'FH;82B&lt;!:8;7ZE&lt;X=*37VN:72J982F!!!-&gt;(*J:W&gt;F=C"U?8"F!!!41!I!$82S;7&gt;H:8)A:'6M98E!'5!+!"*S:7:F=G6O9W5A='^T;82J&lt;WY!!".!#A!.&gt;(*J:W&gt;F=C"M:8:F&lt;!!21!I!#GBZ=X2F=G6T;8-!!!^!#A!*&lt;'^X)'RF&gt;G6M!"&amp;!#A!+;'FH;#"M:8:F&lt;!!!1!$R!!!!!!!!!!%=&lt;GF49W^Q:3"U=GFH:W6S)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!9!%(2S;7&gt;H:8)A9W^V='RJ&lt;G=!!%5!]1!!!!!!!!!"(WZJ5W.P='5A&gt;(*J:W&gt;F=C"X;7ZE&lt;X=A&lt;7^E:3ZD&gt;'Q!(5!$!".U=GFH:W6S)(&gt;J&lt;G2P&gt;S"N&lt;W2F!$E!]1!!!!!!!!!"'7ZJ5W.P='5A&gt;(*J:W&gt;F=C"T&lt;'^Q:3ZD&gt;'Q!&amp;U!$!!VU=GFH:W6S)(.M&lt;X"F!"B!-0````]/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!!Q!1&gt;(*J:W&gt;F=C"N&lt;W2J:GFF=A!!&amp;E!Q`````QRD;'&amp;O&lt;G6M)'ZB&lt;75!!%*!5!!8!!!!!1!#!!-!"!!&amp;!!9!"Q!)!!E!#A!,!!Q!$1!/!!]!%!!2!")!%Q!5!"5!&amp;AR49W^Q:3"4;7ZH&lt;'5!!!%!&amp;Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope10" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\NDCXII-PXI3\AcqLib\PXI3ScopeData10</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;03Q%!!"!!A!!!!!!0!"F!#A!397.U&gt;7&amp;M)(.B&lt;8"M:3"S982F!!!51#%/986U&lt;S"U=GFH:W6S:71!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!":!5!!$!!)!!Q!%#76S=G^S)'^V&gt;!!81!I!%(*F&lt;'&amp;U;8:F37ZJ&gt;'FB&lt;&amp;A!!"&amp;!#A!+?%FO9X*F&lt;76O&gt;!!!"1!+!!!11%!!!@````]!#!.X:GU!$!"1!!-!"A!(!!E!(%"!!!(`````!!I/6W&amp;W:7:P=GUA2X*B='A!!"N!!Q!597.U&gt;7&amp;M)(*F9W^S:#"M:7ZH&gt;'A!!!V!#!!'&gt;'FN:49U!!!C1&amp;!!"A!!!!%!"1!,!!Q!$1Z49W^Q:3"3:7&amp;E9G&amp;D;Q!!!1!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope10Settings" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\NDCXII-PXI3\AcqLib\PXI3ScopeControl10</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">4</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">niScope trigger coupling.ctl</Property>
		<Property Name="typedefName2" Type="Str">niScope trigger slope.ctl</Property>
		<Property Name="typedefName3" Type="Str">niScope trigger window mode.ctl</Property>
		<Property Name="typedefName4" Type="Str">niScope vertical coupling.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"!!A!!!!!!9!":!)2"F&lt;G:P=G.F)(*F97RU;7VF!!!61!I!$H:F=H2J9W&amp;M)(*B&lt;G&gt;F!!!61!I!$WVJ&lt;C"T97VQ&lt;'5A=G&amp;U:1!81!-!%7VJ&lt;C"S:7.P=G1A&lt;'6O:X2I!"6!#A!0&gt;G6S&gt;'FD97QA&lt;W:G=W6U!"&gt;!#A!2=(*P9G5A982U:7ZV982J&lt;WY!&amp;5!(!!^J&lt;H"V&gt;#"J&lt;8"F:'&amp;O9W5!(5!+!"&gt;N98BJ&lt;86N)'FO=(6U)':S:8&amp;V:7ZD?1""!0%!!!!!!!!!!2VO;6.D&lt;X"F)(:F=H2J9W&amp;M)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!-!%8:F=H2J9W&amp;M)'.P&gt;8"M;7ZH!""!)1J&amp;&lt;G&amp;C&lt;'5A6%F4!!!`1"9!"12&amp;:'&gt;F#EBZ=X2F=G6T;8-(2'FH;82B&lt;!:8;7ZE&lt;X=*37VN:72J982F!!!-&gt;(*J:W&gt;F=C"U?8"F!!!41!I!$82S;7&gt;H:8)A:'6M98E!'5!+!"*S:7:F=G6O9W5A='^T;82J&lt;WY!!".!#A!.&gt;(*J:W&gt;F=C"M:8:F&lt;!!21!I!#GBZ=X2F=G6T;8-!!!^!#A!*&lt;'^X)'RF&gt;G6M!"&amp;!#A!+;'FH;#"M:8:F&lt;!!!1!$R!!!!!!!!!!%=&lt;GF49W^Q:3"U=GFH:W6S)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!9!%(2S;7&gt;H:8)A9W^V='RJ&lt;G=!!%5!]1!!!!!!!!!"(WZJ5W.P='5A&gt;(*J:W&gt;F=C"X;7ZE&lt;X=A&lt;7^E:3ZD&gt;'Q!(5!$!".U=GFH:W6S)(&gt;J&lt;G2P&gt;S"N&lt;W2F!$E!]1!!!!!!!!!"'7ZJ5W.P='5A&gt;(*J:W&gt;F=C"T&lt;'^Q:3ZD&gt;'Q!&amp;U!$!!VU=GFH:W6S)(.M&lt;X"F!"B!-0````]/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!!Q!1&gt;(*J:W&gt;F=C"N&lt;W2J:GFF=A!!&amp;E!Q`````QRD;'&amp;O&lt;G6M)'ZB&lt;75!!%*!5!!8!!!!!1!#!!-!"!!&amp;!!9!"Q!)!!E!#A!,!!Q!$1!/!!]!%!!2!")!%Q!5!"5!&amp;AR49W^Q:3"4;7ZH&lt;'5!!!%!&amp;Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope11" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\NDCXII-PXI3\AcqLib\PXI3ScopeData11</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;03Q%!!"!!A!!!!!!0!"F!#A!397.U&gt;7&amp;M)(.B&lt;8"M:3"S982F!!!51#%/986U&lt;S"U=GFH:W6S:71!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!":!5!!$!!)!!Q!%#76S=G^S)'^V&gt;!!81!I!%(*F&lt;'&amp;U;8:F37ZJ&gt;'FB&lt;&amp;A!!"&amp;!#A!+?%FO9X*F&lt;76O&gt;!!!"1!+!!!11%!!!@````]!#!.X:GU!$!"1!!-!"A!(!!E!(%"!!!(`````!!I/6W&amp;W:7:P=GUA2X*B='A!!"N!!Q!597.U&gt;7&amp;M)(*F9W^S:#"M:7ZH&gt;'A!!!V!#!!'&gt;'FN:49U!!!C1&amp;!!"A!!!!%!"1!,!!Q!$1Z49W^Q:3"3:7&amp;E9G&amp;D;Q!!!1!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope11Settings" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\NDCXII-PXI3\AcqLib\PXI3ScopeControl11</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">4</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">niScope trigger coupling.ctl</Property>
		<Property Name="typedefName2" Type="Str">niScope trigger slope.ctl</Property>
		<Property Name="typedefName3" Type="Str">niScope trigger window mode.ctl</Property>
		<Property Name="typedefName4" Type="Str">niScope vertical coupling.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"!!A!!!!!!9!":!)2"F&lt;G:P=G.F)(*F97RU;7VF!!!61!I!$H:F=H2J9W&amp;M)(*B&lt;G&gt;F!!!61!I!$WVJ&lt;C"T97VQ&lt;'5A=G&amp;U:1!81!-!%7VJ&lt;C"S:7.P=G1A&lt;'6O:X2I!"6!#A!0&gt;G6S&gt;'FD97QA&lt;W:G=W6U!"&gt;!#A!2=(*P9G5A982U:7ZV982J&lt;WY!&amp;5!(!!^J&lt;H"V&gt;#"J&lt;8"F:'&amp;O9W5!(5!+!"&gt;N98BJ&lt;86N)'FO=(6U)':S:8&amp;V:7ZD?1""!0%!!!!!!!!!!2VO;6.D&lt;X"F)(:F=H2J9W&amp;M)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!-!%8:F=H2J9W&amp;M)'.P&gt;8"M;7ZH!""!)1J&amp;&lt;G&amp;C&lt;'5A6%F4!!!`1"9!"12&amp;:'&gt;F#EBZ=X2F=G6T;8-(2'FH;82B&lt;!:8;7ZE&lt;X=*37VN:72J982F!!!-&gt;(*J:W&gt;F=C"U?8"F!!!41!I!$82S;7&gt;H:8)A:'6M98E!'5!+!"*S:7:F=G6O9W5A='^T;82J&lt;WY!!".!#A!.&gt;(*J:W&gt;F=C"M:8:F&lt;!!21!I!#GBZ=X2F=G6T;8-!!!^!#A!*&lt;'^X)'RF&gt;G6M!"&amp;!#A!+;'FH;#"M:8:F&lt;!!!1!$R!!!!!!!!!!%=&lt;GF49W^Q:3"U=GFH:W6S)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!9!%(2S;7&gt;H:8)A9W^V='RJ&lt;G=!!%5!]1!!!!!!!!!"(WZJ5W.P='5A&gt;(*J:W&gt;F=C"X;7ZE&lt;X=A&lt;7^E:3ZD&gt;'Q!(5!$!".U=GFH:W6S)(&gt;J&lt;G2P&gt;S"N&lt;W2F!$E!]1!!!!!!!!!"'7ZJ5W.P='5A&gt;(*J:W&gt;F=C"T&lt;'^Q:3ZD&gt;'Q!&amp;U!$!!VU=GFH:W6S)(.M&lt;X"F!"B!-0````]/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!!Q!1&gt;(*J:W&gt;F=C"N&lt;W2J:GFF=A!!&amp;E!Q`````QRD;'&amp;O&lt;G6M)'ZB&lt;75!!%*!5!!8!!!!!1!#!!-!"!!&amp;!!9!"Q!)!!E!#A!,!!Q!$1!/!!]!%!!2!")!%Q!5!"5!&amp;AR49W^Q:3"4;7ZH&lt;'5!!!%!&amp;Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope12" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\NDCXII-PXI3\AcqLib\PXI3ScopeData12</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;03Q%!!"!!A!!!!!!0!"F!#A!397.U&gt;7&amp;M)(.B&lt;8"M:3"S982F!!!51#%/986U&lt;S"U=GFH:W6S:71!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!":!5!!$!!)!!Q!%#76S=G^S)'^V&gt;!!81!I!%(*F&lt;'&amp;U;8:F37ZJ&gt;'FB&lt;&amp;A!!"&amp;!#A!+?%FO9X*F&lt;76O&gt;!!!"1!+!!!11%!!!@````]!#!.X:GU!$!"1!!-!"A!(!!E!(%"!!!(`````!!I/6W&amp;W:7:P=GUA2X*B='A!!"N!!Q!597.U&gt;7&amp;M)(*F9W^S:#"M:7ZH&gt;'A!!!V!#!!'&gt;'FN:49U!!!C1&amp;!!"A!!!!%!"1!,!!Q!$1Z49W^Q:3"3:7&amp;E9G&amp;D;Q!!!1!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope12Settings" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\NDCXII-PXI3\AcqLib\PXI3ScopeControl12</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">4</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">niScope trigger coupling.ctl</Property>
		<Property Name="typedefName2" Type="Str">niScope trigger slope.ctl</Property>
		<Property Name="typedefName3" Type="Str">niScope trigger window mode.ctl</Property>
		<Property Name="typedefName4" Type="Str">niScope vertical coupling.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"!!A!!!!!!9!":!)2"F&lt;G:P=G.F)(*F97RU;7VF!!!61!I!$H:F=H2J9W&amp;M)(*B&lt;G&gt;F!!!61!I!$WVJ&lt;C"T97VQ&lt;'5A=G&amp;U:1!81!-!%7VJ&lt;C"S:7.P=G1A&lt;'6O:X2I!"6!#A!0&gt;G6S&gt;'FD97QA&lt;W:G=W6U!"&gt;!#A!2=(*P9G5A982U:7ZV982J&lt;WY!&amp;5!(!!^J&lt;H"V&gt;#"J&lt;8"F:'&amp;O9W5!(5!+!"&gt;N98BJ&lt;86N)'FO=(6U)':S:8&amp;V:7ZD?1""!0%!!!!!!!!!!2VO;6.D&lt;X"F)(:F=H2J9W&amp;M)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!-!%8:F=H2J9W&amp;M)'.P&gt;8"M;7ZH!""!)1J&amp;&lt;G&amp;C&lt;'5A6%F4!!!`1"9!"12&amp;:'&gt;F#EBZ=X2F=G6T;8-(2'FH;82B&lt;!:8;7ZE&lt;X=*37VN:72J982F!!!-&gt;(*J:W&gt;F=C"U?8"F!!!41!I!$82S;7&gt;H:8)A:'6M98E!'5!+!"*S:7:F=G6O9W5A='^T;82J&lt;WY!!".!#A!.&gt;(*J:W&gt;F=C"M:8:F&lt;!!21!I!#GBZ=X2F=G6T;8-!!!^!#A!*&lt;'^X)'RF&gt;G6M!"&amp;!#A!+;'FH;#"M:8:F&lt;!!!1!$R!!!!!!!!!!%=&lt;GF49W^Q:3"U=GFH:W6S)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!9!%(2S;7&gt;H:8)A9W^V='RJ&lt;G=!!%5!]1!!!!!!!!!"(WZJ5W.P='5A&gt;(*J:W&gt;F=C"X;7ZE&lt;X=A&lt;7^E:3ZD&gt;'Q!(5!$!".U=GFH:W6S)(&gt;J&lt;G2P&gt;S"N&lt;W2F!$E!]1!!!!!!!!!"'7ZJ5W.P='5A&gt;(*J:W&gt;F=C"T&lt;'^Q:3ZD&gt;'Q!&amp;U!$!!VU=GFH:W6S)(.M&lt;X"F!"B!-0````]/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!!Q!1&gt;(*J:W&gt;F=C"N&lt;W2J:GFF=A!!&amp;E!Q`````QRD;'&amp;O&lt;G6M)'ZB&lt;75!!%*!5!!8!!!!!1!#!!-!"!!&amp;!!9!"Q!)!!E!#A!,!!Q!$1!/!!]!%!!2!")!%Q!5!"5!&amp;AR49W^Q:3"4;7ZH&lt;'5!!!%!&amp;Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope14" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\NDCXII-PXI3\AcqLib\PXI3ScopeData14</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;03Q%!!"!!A!!!!!!0!"F!#A!397.U&gt;7&amp;M)(.B&lt;8"M:3"S982F!!!51#%/986U&lt;S"U=GFH:W6S:71!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!":!5!!$!!)!!Q!%#76S=G^S)'^V&gt;!!81!I!%(*F&lt;'&amp;U;8:F37ZJ&gt;'FB&lt;&amp;A!!"&amp;!#A!+?%FO9X*F&lt;76O&gt;!!!"1!+!!!11%!!!@````]!#!.X:GU!$!"1!!-!"A!(!!E!(%"!!!(`````!!I/6W&amp;W:7:P=GUA2X*B='A!!"N!!Q!597.U&gt;7&amp;M)(*F9W^S:#"M:7ZH&gt;'A!!!V!#!!'&gt;'FN:49U!!!C1&amp;!!"A!!!!%!"1!,!!Q!$1Z49W^Q:3"3:7&amp;E9G&amp;D;Q!!!1!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope14Settings" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\NDCXII-PXI3\AcqLib\PXI3ScopeControl14</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">4</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">niScope trigger coupling.ctl</Property>
		<Property Name="typedefName2" Type="Str">niScope trigger slope.ctl</Property>
		<Property Name="typedefName3" Type="Str">niScope trigger window mode.ctl</Property>
		<Property Name="typedefName4" Type="Str">niScope vertical coupling.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"!!A!!!!!!9!":!)2"F&lt;G:P=G.F)(*F97RU;7VF!!!61!I!$H:F=H2J9W&amp;M)(*B&lt;G&gt;F!!!61!I!$WVJ&lt;C"T97VQ&lt;'5A=G&amp;U:1!81!-!%7VJ&lt;C"S:7.P=G1A&lt;'6O:X2I!"6!#A!0&gt;G6S&gt;'FD97QA&lt;W:G=W6U!"&gt;!#A!2=(*P9G5A982U:7ZV982J&lt;WY!&amp;5!(!!^J&lt;H"V&gt;#"J&lt;8"F:'&amp;O9W5!(5!+!"&gt;N98BJ&lt;86N)'FO=(6U)':S:8&amp;V:7ZD?1""!0%!!!!!!!!!!2VO;6.D&lt;X"F)(:F=H2J9W&amp;M)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!-!%8:F=H2J9W&amp;M)'.P&gt;8"M;7ZH!""!)1J&amp;&lt;G&amp;C&lt;'5A6%F4!!!`1"9!"12&amp;:'&gt;F#EBZ=X2F=G6T;8-(2'FH;82B&lt;!:8;7ZE&lt;X=*37VN:72J982F!!!-&gt;(*J:W&gt;F=C"U?8"F!!!41!I!$82S;7&gt;H:8)A:'6M98E!'5!+!"*S:7:F=G6O9W5A='^T;82J&lt;WY!!".!#A!.&gt;(*J:W&gt;F=C"M:8:F&lt;!!21!I!#GBZ=X2F=G6T;8-!!!^!#A!*&lt;'^X)'RF&gt;G6M!"&amp;!#A!+;'FH;#"M:8:F&lt;!!!1!$R!!!!!!!!!!%=&lt;GF49W^Q:3"U=GFH:W6S)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!9!%(2S;7&gt;H:8)A9W^V='RJ&lt;G=!!%5!]1!!!!!!!!!"(WZJ5W.P='5A&gt;(*J:W&gt;F=C"X;7ZE&lt;X=A&lt;7^E:3ZD&gt;'Q!(5!$!".U=GFH:W6S)(&gt;J&lt;G2P&gt;S"N&lt;W2F!$E!]1!!!!!!!!!"'7ZJ5W.P='5A&gt;(*J:W&gt;F=C"T&lt;'^Q:3ZD&gt;'Q!&amp;U!$!!VU=GFH:W6S)(.M&lt;X"F!"B!-0````]/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!!Q!1&gt;(*J:W&gt;F=C"N&lt;W2J:GFF=A!!&amp;E!Q`````QRD;'&amp;O&lt;G6M)'ZB&lt;75!!%*!5!!8!!!!!1!#!!-!"!!&amp;!!9!"Q!)!!E!#A!,!!Q!$1!/!!]!%!!2!")!%Q!5!"5!&amp;AR49W^Q:3"4;7ZH&lt;'5!!!%!&amp;Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope15" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\NDCXII-PXI3\AcqLib\PXI3ScopeData15</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;03Q%!!"!!A!!!!!!0!"F!#A!397.U&gt;7&amp;M)(.B&lt;8"M:3"S982F!!!51#%/986U&lt;S"U=GFH:W6S:71!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!":!5!!$!!)!!Q!%#76S=G^S)'^V&gt;!!81!I!%(*F&lt;'&amp;U;8:F37ZJ&gt;'FB&lt;&amp;A!!"&amp;!#A!+?%FO9X*F&lt;76O&gt;!!!"1!+!!!11%!!!@````]!#!.X:GU!$!"1!!-!"A!(!!E!(%"!!!(`````!!I/6W&amp;W:7:P=GUA2X*B='A!!"N!!Q!597.U&gt;7&amp;M)(*F9W^S:#"M:7ZH&gt;'A!!!V!#!!'&gt;'FN:49U!!!C1&amp;!!"A!!!!%!"1!,!!Q!$1Z49W^Q:3"3:7&amp;E9G&amp;D;Q!!!1!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope15Settings" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\NDCXII-PXI3\AcqLib\PXI3ScopeControl15</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">4</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">niScope trigger coupling.ctl</Property>
		<Property Name="typedefName2" Type="Str">niScope trigger slope.ctl</Property>
		<Property Name="typedefName3" Type="Str">niScope trigger window mode.ctl</Property>
		<Property Name="typedefName4" Type="Str">niScope vertical coupling.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"!!A!!!!!!9!":!)2"F&lt;G:P=G.F)(*F97RU;7VF!!!61!I!$H:F=H2J9W&amp;M)(*B&lt;G&gt;F!!!61!I!$WVJ&lt;C"T97VQ&lt;'5A=G&amp;U:1!81!-!%7VJ&lt;C"S:7.P=G1A&lt;'6O:X2I!"6!#A!0&gt;G6S&gt;'FD97QA&lt;W:G=W6U!"&gt;!#A!2=(*P9G5A982U:7ZV982J&lt;WY!&amp;5!(!!^J&lt;H"V&gt;#"J&lt;8"F:'&amp;O9W5!(5!+!"&gt;N98BJ&lt;86N)'FO=(6U)':S:8&amp;V:7ZD?1""!0%!!!!!!!!!!2VO;6.D&lt;X"F)(:F=H2J9W&amp;M)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!-!%8:F=H2J9W&amp;M)'.P&gt;8"M;7ZH!""!)1J&amp;&lt;G&amp;C&lt;'5A6%F4!!!`1"9!"12&amp;:'&gt;F#EBZ=X2F=G6T;8-(2'FH;82B&lt;!:8;7ZE&lt;X=*37VN:72J982F!!!-&gt;(*J:W&gt;F=C"U?8"F!!!41!I!$82S;7&gt;H:8)A:'6M98E!'5!+!"*S:7:F=G6O9W5A='^T;82J&lt;WY!!".!#A!.&gt;(*J:W&gt;F=C"M:8:F&lt;!!21!I!#GBZ=X2F=G6T;8-!!!^!#A!*&lt;'^X)'RF&gt;G6M!"&amp;!#A!+;'FH;#"M:8:F&lt;!!!1!$R!!!!!!!!!!%=&lt;GF49W^Q:3"U=GFH:W6S)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!9!%(2S;7&gt;H:8)A9W^V='RJ&lt;G=!!%5!]1!!!!!!!!!"(WZJ5W.P='5A&gt;(*J:W&gt;F=C"X;7ZE&lt;X=A&lt;7^E:3ZD&gt;'Q!(5!$!".U=GFH:W6S)(&gt;J&lt;G2P&gt;S"N&lt;W2F!$E!]1!!!!!!!!!"'7ZJ5W.P='5A&gt;(*J:W&gt;F=C"T&lt;'^Q:3ZD&gt;'Q!&amp;U!$!!VU=GFH:W6S)(.M&lt;X"F!"B!-0````]/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!!Q!1&gt;(*J:W&gt;F=C"N&lt;W2J:GFF=A!!&amp;E!Q`````QRD;'&amp;O&lt;G6M)'ZB&lt;75!!%*!5!!8!!!!!1!#!!-!"!!&amp;!!9!"Q!)!!E!#A!,!!Q!$1!/!!]!%!!2!")!%Q!5!"5!&amp;AR49W^Q:3"4;7ZH&lt;'5!!!%!&amp;Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope16" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\NDCXII-PXI3\AcqLib\PXI3ScopeData16</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;03Q%!!"!!A!!!!!!0!"F!#A!397.U&gt;7&amp;M)(.B&lt;8"M:3"S982F!!!51#%/986U&lt;S"U=GFH:W6S:71!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!":!5!!$!!)!!Q!%#76S=G^S)'^V&gt;!!81!I!%(*F&lt;'&amp;U;8:F37ZJ&gt;'FB&lt;&amp;A!!"&amp;!#A!+?%FO9X*F&lt;76O&gt;!!!"1!+!!!11%!!!@````]!#!.X:GU!$!"1!!-!"A!(!!E!(%"!!!(`````!!I/6W&amp;W:7:P=GUA2X*B='A!!"N!!Q!597.U&gt;7&amp;M)(*F9W^S:#"M:7ZH&gt;'A!!!V!#!!'&gt;'FN:49U!!!C1&amp;!!"A!!!!%!"1!,!!Q!$1Z49W^Q:3"3:7&amp;E9G&amp;D;Q!!!1!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope16Settings" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\NDCXII-PXI3\AcqLib\PXI3ScopeControl16</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">4</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">niScope trigger coupling.ctl</Property>
		<Property Name="typedefName2" Type="Str">niScope trigger slope.ctl</Property>
		<Property Name="typedefName3" Type="Str">niScope trigger window mode.ctl</Property>
		<Property Name="typedefName4" Type="Str">niScope vertical coupling.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"!!A!!!!!!9!":!)2"F&lt;G:P=G.F)(*F97RU;7VF!!!61!I!$H:F=H2J9W&amp;M)(*B&lt;G&gt;F!!!61!I!$WVJ&lt;C"T97VQ&lt;'5A=G&amp;U:1!81!-!%7VJ&lt;C"S:7.P=G1A&lt;'6O:X2I!"6!#A!0&gt;G6S&gt;'FD97QA&lt;W:G=W6U!"&gt;!#A!2=(*P9G5A982U:7ZV982J&lt;WY!&amp;5!(!!^J&lt;H"V&gt;#"J&lt;8"F:'&amp;O9W5!(5!+!"&gt;N98BJ&lt;86N)'FO=(6U)':S:8&amp;V:7ZD?1""!0%!!!!!!!!!!2VO;6.D&lt;X"F)(:F=H2J9W&amp;M)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!-!%8:F=H2J9W&amp;M)'.P&gt;8"M;7ZH!""!)1J&amp;&lt;G&amp;C&lt;'5A6%F4!!!`1"9!"12&amp;:'&gt;F#EBZ=X2F=G6T;8-(2'FH;82B&lt;!:8;7ZE&lt;X=*37VN:72J982F!!!-&gt;(*J:W&gt;F=C"U?8"F!!!41!I!$82S;7&gt;H:8)A:'6M98E!'5!+!"*S:7:F=G6O9W5A='^T;82J&lt;WY!!".!#A!.&gt;(*J:W&gt;F=C"M:8:F&lt;!!21!I!#GBZ=X2F=G6T;8-!!!^!#A!*&lt;'^X)'RF&gt;G6M!"&amp;!#A!+;'FH;#"M:8:F&lt;!!!1!$R!!!!!!!!!!%=&lt;GF49W^Q:3"U=GFH:W6S)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!9!%(2S;7&gt;H:8)A9W^V='RJ&lt;G=!!%5!]1!!!!!!!!!"(WZJ5W.P='5A&gt;(*J:W&gt;F=C"X;7ZE&lt;X=A&lt;7^E:3ZD&gt;'Q!(5!$!".U=GFH:W6S)(&gt;J&lt;G2P&gt;S"N&lt;W2F!$E!]1!!!!!!!!!"'7ZJ5W.P='5A&gt;(*J:W&gt;F=C"T&lt;'^Q:3ZD&gt;'Q!&amp;U!$!!VU=GFH:W6S)(.M&lt;X"F!"B!-0````]/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!!Q!1&gt;(*J:W&gt;F=C"N&lt;W2J:GFF=A!!&amp;E!Q`````QRD;'&amp;O&lt;G6M)'ZB&lt;75!!%*!5!!8!!!!!1!#!!-!"!!&amp;!!9!"Q!)!!E!#A!,!!Q!$1!/!!]!%!!2!")!%Q!5!"5!&amp;AR49W^Q:3"4;7ZH&lt;'5!!!%!&amp;Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope17" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\NDCXII-PXI3\AcqLib\PXI3ScopeData17</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;03Q%!!"!!A!!!!!!0!"F!#A!397.U&gt;7&amp;M)(.B&lt;8"M:3"S982F!!!51#%/986U&lt;S"U=GFH:W6S:71!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!":!5!!$!!)!!Q!%#76S=G^S)'^V&gt;!!81!I!%(*F&lt;'&amp;U;8:F37ZJ&gt;'FB&lt;&amp;A!!"&amp;!#A!+?%FO9X*F&lt;76O&gt;!!!"1!+!!!11%!!!@````]!#!.X:GU!$!"1!!-!"A!(!!E!(%"!!!(`````!!I/6W&amp;W:7:P=GUA2X*B='A!!"N!!Q!597.U&gt;7&amp;M)(*F9W^S:#"M:7ZH&gt;'A!!!V!#!!'&gt;'FN:49U!!!C1&amp;!!"A!!!!%!"1!,!!Q!$1Z49W^Q:3"3:7&amp;E9G&amp;D;Q!!!1!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope17Settings" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\NDCXII-PXI3\AcqLib\PXI3ScopeControl17</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">4</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">niScope trigger coupling.ctl</Property>
		<Property Name="typedefName2" Type="Str">niScope trigger slope.ctl</Property>
		<Property Name="typedefName3" Type="Str">niScope trigger window mode.ctl</Property>
		<Property Name="typedefName4" Type="Str">niScope vertical coupling.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"!!A!!!!!!9!":!)2"F&lt;G:P=G.F)(*F97RU;7VF!!!61!I!$H:F=H2J9W&amp;M)(*B&lt;G&gt;F!!!61!I!$WVJ&lt;C"T97VQ&lt;'5A=G&amp;U:1!81!-!%7VJ&lt;C"S:7.P=G1A&lt;'6O:X2I!"6!#A!0&gt;G6S&gt;'FD97QA&lt;W:G=W6U!"&gt;!#A!2=(*P9G5A982U:7ZV982J&lt;WY!&amp;5!(!!^J&lt;H"V&gt;#"J&lt;8"F:'&amp;O9W5!(5!+!"&gt;N98BJ&lt;86N)'FO=(6U)':S:8&amp;V:7ZD?1""!0%!!!!!!!!!!2VO;6.D&lt;X"F)(:F=H2J9W&amp;M)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!-!%8:F=H2J9W&amp;M)'.P&gt;8"M;7ZH!""!)1J&amp;&lt;G&amp;C&lt;'5A6%F4!!!`1"9!"12&amp;:'&gt;F#EBZ=X2F=G6T;8-(2'FH;82B&lt;!:8;7ZE&lt;X=*37VN:72J982F!!!-&gt;(*J:W&gt;F=C"U?8"F!!!41!I!$82S;7&gt;H:8)A:'6M98E!'5!+!"*S:7:F=G6O9W5A='^T;82J&lt;WY!!".!#A!.&gt;(*J:W&gt;F=C"M:8:F&lt;!!21!I!#GBZ=X2F=G6T;8-!!!^!#A!*&lt;'^X)'RF&gt;G6M!"&amp;!#A!+;'FH;#"M:8:F&lt;!!!1!$R!!!!!!!!!!%=&lt;GF49W^Q:3"U=GFH:W6S)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!9!%(2S;7&gt;H:8)A9W^V='RJ&lt;G=!!%5!]1!!!!!!!!!"(WZJ5W.P='5A&gt;(*J:W&gt;F=C"X;7ZE&lt;X=A&lt;7^E:3ZD&gt;'Q!(5!$!".U=GFH:W6S)(&gt;J&lt;G2P&gt;S"N&lt;W2F!$E!]1!!!!!!!!!"'7ZJ5W.P='5A&gt;(*J:W&gt;F=C"T&lt;'^Q:3ZD&gt;'Q!&amp;U!$!!VU=GFH:W6S)(.M&lt;X"F!"B!-0````]/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!!Q!1&gt;(*J:W&gt;F=C"N&lt;W2J:GFF=A!!&amp;E!Q`````QRD;'&amp;O&lt;G6M)'ZB&lt;75!!%*!5!!8!!!!!1!#!!-!"!!&amp;!!9!"Q!)!!E!#A!,!!Q!$1!/!!]!%!!2!")!%Q!5!"5!&amp;AR49W^Q:3"4;7ZH&lt;'5!!!%!&amp;Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope18" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\NDCXII-PXI3\AcqLib\PXI3ScopeData18</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;03Q%!!"!!A!!!!!!0!"F!#A!397.U&gt;7&amp;M)(.B&lt;8"M:3"S982F!!!51#%/986U&lt;S"U=GFH:W6S:71!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!":!5!!$!!)!!Q!%#76S=G^S)'^V&gt;!!81!I!%(*F&lt;'&amp;U;8:F37ZJ&gt;'FB&lt;&amp;A!!"&amp;!#A!+?%FO9X*F&lt;76O&gt;!!!"1!+!!!11%!!!@````]!#!.X:GU!$!"1!!-!"A!(!!E!(%"!!!(`````!!I/6W&amp;W:7:P=GUA2X*B='A!!"N!!Q!597.U&gt;7&amp;M)(*F9W^S:#"M:7ZH&gt;'A!!!V!#!!'&gt;'FN:49U!!!C1&amp;!!"A!!!!%!"1!,!!Q!$1Z49W^Q:3"3:7&amp;E9G&amp;D;Q!!!1!/!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI3Scope18Settings" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read only</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\NDCXII-PXI3\AcqLib\PXI3ScopeControl18</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">4</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">niScope trigger coupling.ctl</Property>
		<Property Name="typedefName2" Type="Str">niScope trigger slope.ctl</Property>
		<Property Name="typedefName3" Type="Str">niScope trigger window mode.ctl</Property>
		<Property Name="typedefName4" Type="Str">niScope vertical coupling.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger coupling.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger slope.ctl</Property>
		<Property Name="typedefPath3" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope trigger window mode.ctl</Property>
		<Property Name="typedefPath4" Type="PathRel">/&lt;instrlib&gt;/niScope/Controls/niScope vertical coupling.ctl</Property>
		<Property Name="typeDesc" Type="Bin">%!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"!!A!!!!!!9!":!)2"F&lt;G:P=G.F)(*F97RU;7VF!!!61!I!$H:F=H2J9W&amp;M)(*B&lt;G&gt;F!!!61!I!$WVJ&lt;C"T97VQ&lt;'5A=G&amp;U:1!81!-!%7VJ&lt;C"S:7.P=G1A&lt;'6O:X2I!"6!#A!0&gt;G6S&gt;'FD97QA&lt;W:G=W6U!"&gt;!#A!2=(*P9G5A982U:7ZV982J&lt;WY!&amp;5!(!!^J&lt;H"V&gt;#"J&lt;8"F:'&amp;O9W5!(5!+!"&gt;N98BJ&lt;86N)'FO=(6U)':S:8&amp;V:7ZD?1""!0%!!!!!!!!!!2VO;6.D&lt;X"F)(:F=H2J9W&amp;M)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!-!%8:F=H2J9W&amp;M)'.P&gt;8"M;7ZH!""!)1J&amp;&lt;G&amp;C&lt;'5A6%F4!!!`1"9!"12&amp;:'&gt;F#EBZ=X2F=G6T;8-(2'FH;82B&lt;!:8;7ZE&lt;X=*37VN:72J982F!!!-&gt;(*J:W&gt;F=C"U?8"F!!!41!I!$82S;7&gt;H:8)A:'6M98E!'5!+!"*S:7:F=G6O9W5A='^T;82J&lt;WY!!".!#A!.&gt;(*J:W&gt;F=C"M:8:F&lt;!!21!I!#GBZ=X2F=G6T;8-!!!^!#A!*&lt;'^X)'RF&gt;G6M!"&amp;!#A!+;'FH;#"M:8:F&lt;!!!1!$R!!!!!!!!!!%=&lt;GF49W^Q:3"U=GFH:W6S)'.P&gt;8"M;7ZH,G.U&lt;!!&lt;1!9!%(2S;7&gt;H:8)A9W^V='RJ&lt;G=!!%5!]1!!!!!!!!!"(WZJ5W.P='5A&gt;(*J:W&gt;F=C"X;7ZE&lt;X=A&lt;7^E:3ZD&gt;'Q!(5!$!".U=GFH:W6S)(&gt;J&lt;G2P&gt;S"N&lt;W2F!$E!]1!!!!!!!!!"'7ZJ5W.P='5A&gt;(*J:W&gt;F=C"T&lt;'^Q:3ZD&gt;'Q!&amp;U!$!!VU=GFH:W6S)(.M&lt;X"F!"B!-0````]/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!!Q!1&gt;(*J:W&gt;F=C"N&lt;W2J:GFF=A!!&amp;E!Q`````QRD;'&amp;O&lt;G6M)'ZB&lt;75!!%*!5!!8!!!!!1!#!!-!"!!&amp;!!9!"Q!)!!E!#A!,!!Q!$1!/!!]!%!!2!")!%Q!5!"5!&amp;AR49W^Q:3"4;7ZH&lt;'5!!!%!&amp;Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
</Library>
