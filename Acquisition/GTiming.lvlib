﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="15008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"=&gt;MQ%!8143;(8.6"2CVM#WJ",7Q,SN&amp;(N&lt;!NK!7VM#WI"&lt;8A0$%94UZ2$P%E"Y.?G@I%A7=11U&gt;M\7P%FXB^VL\`NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAG_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y!#/7SO!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">352354304</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="BackAnnotate" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="GTime" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"5!A!!!!!!"!!V!#!!'65FO&gt;$9U!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Timing" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\NDCXII-PXI0\TimingLib\PXI0Timing</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">2</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">GFTy 9404.lvlib:GFT9404_Auxiliary Delay Channels.ctl</Property>
		<Property Name="typedefName2" Type="Str">GFTy 9404.lvlib:GFT9404_Precision Delay Channels.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/GreenfieldTechnology GFT9404/_Typedefs/GFT9404_Auxiliary Delay Channels.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/GreenfieldTechnology GFT9404/_Typedefs/GFT9404_Precision Delay Channels.ctl</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"5!A!!!!!!@!"B!-0````]0=W^V=G.F)(2F=GVJ&lt;G&amp;M!"2!1!!"`````Q!!"F.P&gt;8*D:1!!(E!Q`````R2E:8.U;7ZB&gt;'FP&lt;C"U:8*N;7ZB&lt;!!!'%"!!!(`````!!),2'6T&gt;'FO982J&lt;WY!&amp;E"1!!)!!1!$#U.P&lt;GZF9X2J&lt;WZT!!1!)1!-1#%'68"E982F!!!/1#%)47&amp;O)&amp;2S;7=!!#F!&amp;A!##%^O:3"4;'^U#F*F='6U;82J&gt;G5!$(2S;7&gt;H:8)A&lt;7^E:1!!-5!7!!-)37ZU:8*O97Q(6&amp;**2S"*4AB17%EA5V2"5A!/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!#A!2&gt;(*J:W&gt;F=C"M:8:F&lt;#!I6CE!*U!7!!))5'^T;82J&gt;G5)4G6H982J&gt;G5!$82S;7&gt;H:8)A=WRP='5!(5!+!":U=GFH:W6S)':S:8&amp;V:7ZD?3!I3(IJ!!!91&amp;!!"1!)!!E!#A!,!!Q(6(*J:W&gt;F=A"5!0%!!!!!!!!!!A^(2F2Z)$EU-$1O&lt;(:M;7)E2U:5/41Q.&amp;^1=G6D;8.J&lt;WYA2'6M98EA1WBB&lt;GZF&lt;(-O9X2M!"&gt;!!Q!.:'6M98EA9WBB&lt;GZF&lt;!!01!I!#72F&lt;'&amp;Z)#BT+1!41!I!$7&amp;N='RJ&gt;(6E:3!I6CE!%5!+!!JX;72U;#!I&lt;H-J!!!31#%.&lt;X6U=(6U)'6O97*M:1!?1&amp;!!"1!/!!]!%!!2!").2'6M98EA1WBB&lt;GZF&lt;!!71%!!!@````]!%QB$;'&amp;O&lt;G6M=Q!!6!$R!!!!!!!!!!)02U:5?3!Z.$!U,GRW&lt;'FC*%&gt;'6$EU-$2@186Y;7RJ98*Z)%2F&lt;'&amp;Z)%.I97ZO:7RT,G.U&lt;!!81!-!$72F&lt;'&amp;Z)'.I97ZO:7Q!&amp;%!B$G.P&lt;GZF9X1A&gt;']A5&amp;B*!!!71&amp;!!"!!6!!]!%A!7"U.M&gt;8.U:8)!'E"!!!(`````!"=-186Y)%.I97ZO:7RT!!!F1"9!!AB3:7RB&gt;'FW:1B"9H.P&lt;(6U:1!+:'6M98EA&gt;(FQ:1!!&amp;%!B$EVB&lt;H6B&lt;#"5=GFH:W6S!!!?1&amp;!!"1!.!"1!'!!:!"I-1G^B=G1A1W^O:GFH!!!11&amp;!!!1!&lt;"U&gt;'6$EU-$1!(%"!!!(`````!"Q06(*J:W&gt;F=C".&lt;W2V&lt;'6T!"Z!5!!&amp;!!1!"1!'!!=!(1R$=G&amp;U:3"5;7VJ&lt;G=!!!%!(A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI0Tstop" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Timing" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:AccessType" Type="Str">read/write</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:ProjectBinding" Type="Str">False</Property>
		<Property Name="Network:URL" Type="Str">\\NDCXII-PXI1\TimingLib\PXI1Timing</Property>
		<Property Name="Network:UseBinding" Type="Str">True</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">2</Property>
		<Property Name="Path" Type="Str">/Timing.lvproj/My Computer/GTiming.lvlib/</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typedefName1" Type="Str">GFTy 9404.lvlib:GFT9404_Auxiliary Delay Channels.ctl</Property>
		<Property Name="typedefName2" Type="Str">GFTy 9404.lvlib:GFT9404_Precision Delay Channels.ctl</Property>
		<Property Name="typedefPath1" Type="PathRel">/&lt;instrlib&gt;/GreenfieldTechnology GFT9404/_Typedefs/GFT9404_Auxiliary Delay Channels.ctl</Property>
		<Property Name="typedefPath2" Type="PathRel">/&lt;instrlib&gt;/GreenfieldTechnology GFT9404/_Typedefs/GFT9404_Precision Delay Channels.ctl</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!/-C!-!!"5!A!!!!!!@!"B!-0````]0=W^V=G.F)(2F=GVJ&lt;G&amp;M!"2!1!!"`````Q!!"F.P&gt;8*D:1!!(E!Q`````R2E:8.U;7ZB&gt;'FP&lt;C"U:8*N;7ZB&lt;!!!'%"!!!(`````!!),2'6T&gt;'FO982J&lt;WY!&amp;E"1!!)!!1!$#U.P&lt;GZF9X2J&lt;WZT!!1!)1!-1#%'68"E982F!!!/1#%)47&amp;O)&amp;2S;7=!!#F!&amp;A!##%^O:3"4;'^U#F*F='6U;82J&gt;G5!$(2S;7&gt;H:8)A&lt;7^E:1!!-5!7!!-)37ZU:8*O97Q(6&amp;**2S"*4AB17%EA5V2"5A!/&gt;(*J:W&gt;F=C"T&lt;X6S9W5!!"&gt;!#A!2&gt;(*J:W&gt;F=C"M:8:F&lt;#!I6CE!*U!7!!))5'^T;82J&gt;G5)4G6H982J&gt;G5!$82S;7&gt;H:8)A=WRP='5!(5!+!":U=GFH:W6S)':S:8&amp;V:7ZD?3!I3(IJ!!!91&amp;!!"1!)!!E!#A!,!!Q(6(*J:W&gt;F=A"5!0%!!!!!!!!!!A^(2F2Z)$EU-$1O&lt;(:M;7)E2U:5/41Q.&amp;^1=G6D;8.J&lt;WYA2'6M98EA1WBB&lt;GZF&lt;(-O9X2M!"&gt;!!Q!.:'6M98EA9WBB&lt;GZF&lt;!!01!I!#72F&lt;'&amp;Z)#BT+1!41!I!$7&amp;N='RJ&gt;(6E:3!I6CE!%5!+!!JX;72U;#!I&lt;H-J!!!31#%.&lt;X6U=(6U)'6O97*M:1!?1&amp;!!"1!/!!]!%!!2!").2'6M98EA1WBB&lt;GZF&lt;!!71%!!!@````]!%QB$;'&amp;O&lt;G6M=Q!!6!$R!!!!!!!!!!)02U:5?3!Z.$!U,GRW&lt;'FC*%&gt;'6$EU-$2@186Y;7RJ98*Z)%2F&lt;'&amp;Z)%.I97ZO:7RT,G.U&lt;!!81!-!$72F&lt;'&amp;Z)'.I97ZO:7Q!&amp;%!B$G.P&lt;GZF9X1A&gt;']A5&amp;B*!!!71&amp;!!"!!6!!]!%A!7"U.M&gt;8.U:8)!'E"!!!(`````!"=-186Y)%.I97ZO:7RT!!!F1"9!!AB3:7RB&gt;'FW:1B"9H.P&lt;(6U:1!+:'6M98EA&gt;(FQ:1!!&amp;%!B$EVB&lt;H6B&lt;#"5=GFH:W6S!!!?1&amp;!!"1!.!"1!'!!:!"I-1G^B=G1A1W^O:GFH!!!11&amp;!!!1!&lt;"U&gt;'6$EU-$1!(%"!!!(`````!"Q06(*J:W&gt;F=C".&lt;W2V&lt;'6T!"Z!5!!&amp;!!1!"1!'!!=!(1R$=G&amp;U:3"5;7VJ&lt;G=!!!%!(A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="PXI1Tstop" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"5!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="TestStringArray" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"%1!!!!"5!A!!!!!!#!""!-0````]'5X2S;7ZH!!!=1%!!!@````]!!!^"=H*B?3"P:C"4&gt;(*J&lt;G=!!1!"!!!!!!!!!!!!!!!!</Property>
	</Item>
</Library>
